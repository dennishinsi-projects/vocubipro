/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "table_language")
data class Language(
    @SerializedName("id")
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: String = UUID.randomUUID().toString(),

    @SerializedName(value = "title")
    @ColumnInfo(name = "title")
    var title: String,

    @SerializedName(value = "title_translated")
    @ColumnInfo(name = "title_translated")
    var titleTranslated: String,

    @SerializedName(value = "emoji")
    @ColumnInfo(name = "emoji")
    var emoji: String,

    @SerializedName(value = "accent_color")
    @ColumnInfo(name = "accent_color")
    var accentColor: String
)