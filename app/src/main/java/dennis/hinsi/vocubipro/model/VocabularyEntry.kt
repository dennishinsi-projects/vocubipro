/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "table_vocabulary_vocabulary_entry")
data class VocabularyEntry(
    @SerializedName("id")
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: String = UUID.randomUUID().toString(),

    @SerializedName("entry_id")
    @ColumnInfo(name = "entry_id")
    val entryId: String = UUID.randomUUID().toString(),

    @SerializedName(value = "language_id")
    val languageId: String,

    @SerializedName(value = "phonetic_text")
    val phoneticText: String,

    @SerializedName(value = "transcription_text")
    val transcriptionText: String,

    @SerializedName(value = "text")
    val text: String
)
