/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.dagger

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import dennis.hinsi.vocubipro.database.*
import dennis.hinsi.vocubipro.repository.*
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RoomModule {

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext context: Context): AppDataBase {
        return Room
            .databaseBuilder(context, AppDataBase::class.java, "app_database")
            .build()
    }

    @Provides
    @Singleton
    fun provideVocabularyGroupRepository(dataBase: AppDataBase): VocabularyGroupRepository =
        VocabularyGroupRepositoryImpl(dataBase.provideVocabularyGroupDao())

    @Provides
    @Singleton
    fun provideVocabularyChapterRepository(dataBase: AppDataBase): VocabularyChapterRepository =
        VocabularyChapterRepositoryImpl(dataBase.provideVocabularyChapterDao())

    @Provides
    @Singleton
    fun provideVocabularyRepository(dataBase: AppDataBase): VocabularyRepository =
        VocabularyRepositoryImpl(dataBase.provideVocabularyDao())

    @Provides
    @Singleton
    fun provideVocabularyEntryRepository(dataBase: AppDataBase): VocabularyEntryRepository =
        VocabularyEntryRepositoryImpl(dataBase.provideVocabularyEntryDao())


    @Provides
    @Singleton
    fun provideLanguageRepository(dataBase: AppDataBase): LanguageRepository =
        LanguageRepositoryImpl(dataBase.provideLanguageDao())

}