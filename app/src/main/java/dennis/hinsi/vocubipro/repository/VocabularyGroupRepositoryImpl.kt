/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.repository

import dennis.hinsi.vocubipro.database.VocabularyGroupDao
import dennis.hinsi.vocubipro.model.VocabularyGroup
import kotlinx.coroutines.flow.Flow

class VocabularyGroupRepositoryImpl(
    private val dao: VocabularyGroupDao
) : VocabularyGroupRepository {

    override suspend fun insert(data: VocabularyGroup) {
        dao.insert(data)
    }

    override suspend fun update(data: VocabularyGroup) {
        dao.update(data)
    }

    override suspend fun delete(groupId: String) {
        dao.delete(groupId)
    }

    override suspend fun load(groupId: String): VocabularyGroup {
        return dao.load(groupId)
    }

    override fun loadAll(): Flow<List<VocabularyGroup>> {
        return dao.loadAll()
    }
}