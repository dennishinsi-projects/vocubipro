/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.repository

import dennis.hinsi.vocubipro.database.VocabularyDao
import dennis.hinsi.vocubipro.model.Vocabulary
import kotlinx.coroutines.flow.Flow

class VocabularyRepositoryImpl(
    private val dao: VocabularyDao
) : VocabularyRepository {

    override suspend fun insert(data: Vocabulary) {
        dao.insert(data)
    }

    override suspend fun update(data: Vocabulary) {
        dao.update(data)
    }

    override suspend fun delete(vocabularyId: String) {
        dao.delete(vocabularyId)
    }

    override suspend fun load(vocabularyId: String): Vocabulary {
        return dao.load(vocabularyId)
    }

    override suspend fun loadAll(chapterId: String): List<Vocabulary> {
        return dao.loadAll(chapterId)
    }

    override suspend fun numberOfVocabulariesOfChapter(chapterId: String): Int {
        return dao.numberOfVocabulariesOfChapter(chapterId)
    }

    override fun loadAll(): Flow<List<Vocabulary>> {
        return dao.loadAll()
    }
}