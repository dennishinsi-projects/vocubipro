/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.repository

import dennis.hinsi.vocubipro.model.VocabularyChapter
import kotlinx.coroutines.flow.Flow

interface VocabularyChapterRepository {
    suspend fun insert(data: VocabularyChapter)
    suspend fun update(data: VocabularyChapter)
    suspend fun delete(chapterId: String)
    suspend fun load(chapterId: String): VocabularyChapter
    suspend fun loadAll(groupId: String): List<VocabularyChapter>
    suspend fun numberOfChaptersOfGroup(groupId: String): Int
    fun loadAll(): Flow<List<VocabularyChapter>>
}

