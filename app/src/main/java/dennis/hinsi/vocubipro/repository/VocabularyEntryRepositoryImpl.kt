/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.repository

import dennis.hinsi.vocubipro.database.VocabularyEntryDao
import dennis.hinsi.vocubipro.model.VocabularyEntry
import kotlinx.coroutines.flow.Flow

class VocabularyEntryRepositoryImpl(
    private val dao: VocabularyEntryDao
) : VocabularyEntryRepository {

    override suspend fun insert(data: VocabularyEntry) {
        dao.insert(data)
    }

    override suspend fun update(data: VocabularyEntry) {
        dao.update(data)
    }

    override suspend fun delete(id: String) {
        dao.delete(id)
    }

    override suspend fun load(id: String): VocabularyEntry {
        return dao.load(id)
    }

    override suspend fun loadAll(id: String): List<VocabularyEntry> {
        return dao.loadAll(id)
    }

    override suspend fun deleteWithEntryId(entryId: String) {
        dao.deleteAllWithEntryId(entryId)
    }

    override suspend fun loadWithEntryId(entryId: String): VocabularyEntry {
        return dao.loadWithEntryId(entryId)
    }

    override suspend fun loadAllWithEntryId(entryId: String): List<VocabularyEntry> {
        return dao.loadAllWithEntryId(entryId)
    }

    override fun loadAll(): Flow<List<VocabularyEntry>> {
        return dao.loadAll()
    }
}