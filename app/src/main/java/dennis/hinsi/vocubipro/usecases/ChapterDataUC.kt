/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.usecases

import dennis.hinsi.vocubipro.model.VocabularyChapter
import dennis.hinsi.vocubipro.repository.VocabularyChapterRepository
import dennis.hinsi.vocubipro.repository.VocabularyEntryRepository
import dennis.hinsi.vocubipro.repository.VocabularyGroupRepository
import dennis.hinsi.vocubipro.repository.VocabularyRepository
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

class ChapterDataUC @Inject constructor(
    private val chapterRepository: VocabularyChapterRepository,
    private val vocabularyRepository: VocabularyRepository,
) {

   suspend fun loadChaptersOfGroup(groupId: String) =
       chapterRepository.loadAll(groupId)
           .onEach { vocabularyChapter ->
               vocabularyChapter.vocabularyCount =
                   vocabularyRepository.numberOfVocabulariesOfChapter(vocabularyChapter.id)
           }
           .sortedBy { it.position }

    suspend fun loadChapter(chapterId: String) = chapterRepository.load(chapterId)

    suspend fun insert(vocabularyChapter: VocabularyChapter) {
        chapterRepository.insert(vocabularyChapter)
    }

    suspend fun update(vocabularyChapter: VocabularyChapter) {
        chapterRepository.update(vocabularyChapter)
    }

}