/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.usecases

import dennis.hinsi.vocubipro.model.VocabularyGroup
import dennis.hinsi.vocubipro.repository.VocabularyChapterRepository
import dennis.hinsi.vocubipro.repository.VocabularyGroupRepository
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

class GroupDataUC @Inject constructor(
    private val groupRepository: VocabularyGroupRepository,
    private val chapterRepository: VocabularyChapterRepository,
) {

    fun loadAllGroups() = groupRepository.loadAll().onEach { list ->
        list.onEach { it.chapterCount = chapterRepository.numberOfChaptersOfGroup(it.id) }
    }

    suspend fun loadGroup(groupId: String) = groupRepository.load(groupId)

    suspend fun insert(data: VocabularyGroup) {
        groupRepository.insert(data)
    }

    suspend fun update(data: VocabularyGroup) {
        groupRepository.update(data)
    }

    suspend fun delete(id: String) {
        groupRepository.delete(id)
    }

}