/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.usecases

import dennis.hinsi.vocubipro.repository.*
import javax.inject.Inject

/**
 * UseCase to clean data the right way without forgetting parts. Since we have many different
 * database tables connected by different ids and relations we always have to make sure to delete
 * everything connected to the target we want to delete.
 */
class DataDeletionUC @Inject constructor(
    private val languageRepository: LanguageRepository,
    private val groupRepository: VocabularyGroupRepository,
    private val chapterRepository: VocabularyChapterRepository,
    private val vocabularyRepository: VocabularyRepository,
    private val vocabularyEntryRepository: VocabularyEntryRepository
) {

    suspend fun deleteLanguage(languageId: String) {
        languageRepository.delete(languageId)
    }

    suspend fun deleteGroup(groupId: String) {
        // delete group
        groupRepository.delete(groupId)
        // find and delete all chapters connected to group
        chapterRepository.loadAll(groupId).forEach { chapter ->
            deleteChapter(chapter.id)
        }
    }

    suspend fun deleteChapter(chapterId: String) {
        // delete chapter
        chapterRepository.delete(chapterId)
        // find and delete all vocabularies connected to chapter
        vocabularyRepository.loadAll(chapterId).forEach { vocabulary ->
           deleteVocabulary(vocabulary.id, vocabulary.entryId)
        }
    }

    suspend fun deleteVocabulary(vocabularyId: String, entryId: String) {
        // delete vocabulary
        vocabularyRepository.delete(vocabularyId)
        // delete all entries of vocabulary by using entry id
        vocabularyEntryRepository.deleteWithEntryId(entryId)
    }

    suspend fun deleteEntry(id: String) {
        vocabularyEntryRepository.delete(id)
    }

}