/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.usecases

import dennis.hinsi.vocubipro.model.Vocabulary
import dennis.hinsi.vocubipro.model.container.VocabularyContainer
import dennis.hinsi.vocubipro.model.VocabularyEntry
import dennis.hinsi.vocubipro.model.container.VocabularyEntryContainer
import dennis.hinsi.vocubipro.repository.LanguageRepository
import dennis.hinsi.vocubipro.repository.VocabularyChapterRepository
import dennis.hinsi.vocubipro.repository.VocabularyEntryRepository
import dennis.hinsi.vocubipro.repository.VocabularyRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class VocabularyDataUC @Inject constructor(
    private val languageRepository: LanguageRepository,
    private val chapterRepository: VocabularyChapterRepository,
    private val vocabularyRepository: VocabularyRepository,
    private val vocabularyEntryRepository: VocabularyEntryRepository
) {

    val allEntries: Flow<List<VocabularyEntry>> get()= vocabularyEntryRepository.loadAll()

    suspend fun loadChapter(chapterId: String) = chapterRepository.load(chapterId)

    suspend fun loadVocabulariesFromChapter(chapterId: String): List<VocabularyContainer> {
        return vocabularyRepository.loadAll(chapterId)
            .map { vocabulary ->
                VocabularyContainer(vocabulary,
                    vocabularyEntryRepository.loadAllWithEntryId(vocabulary.entryId).map { entry ->
                        VocabularyEntryContainer(entry, languageRepository.load(entry.languageId))
                    }
                )
            }
    }

    suspend fun loadVocabulary(vocabularyId: String) = vocabularyRepository.load(vocabularyId)

    suspend fun insert(data: Vocabulary) {
        vocabularyRepository.insert(data)
    }

    suspend fun update(data: Vocabulary) {
        vocabularyRepository.update(data)
    }

    suspend fun insertEntry(data: VocabularyEntry) {
        vocabularyEntryRepository.insert(data)
    }

    suspend fun updateEntry(data: VocabularyEntry) {
        vocabularyEntryRepository.update(data)
    }

    suspend fun deleteEntry(id: String) {
        vocabularyEntryRepository.delete(id)
    }

}