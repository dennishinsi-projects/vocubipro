/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.util

import androidx.compose.ui.graphics.Color


inline fun <T> T?.onNull(block: () -> Unit) {
    if (this == null)
        block()
}

fun String?.notNullOrEmpty(): Boolean {
    return !isNullOrEmpty()
}

fun String.colorFromHexOrDefault(defaultHex: String): Color {
    return if (this.isNotEmpty() && this.startsWith("#")) {
        Color(android.graphics.Color.parseColor(this))
    } else {
        Color(android.graphics.Color.parseColor(defaultHex))
    }
}

fun String.colorFromHexOrDefault(alpha: Int, defaultHex: String): Color {
    return if (this.isNotEmpty() && this.startsWith("#")) {
        Color(android.graphics.Color.parseColor(replace("#", "#$alpha")))
    } else {
        Color(android.graphics.Color.parseColor(defaultHex))
    }
}

fun String?.orEmpty(): String {
    return if (isNullOrEmpty()) "" else this
}

fun Boolean?.inverseOrDefault(default: Boolean): Boolean  {
    return if (this != null) !this else default
}