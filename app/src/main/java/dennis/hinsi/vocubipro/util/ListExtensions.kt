/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.util


inline fun <T> List<T>?.notNullOrEmpty(block: (List<T>) -> Unit) {
    if (!isNullOrEmpty())
        block(this)
}

fun <T> List<T>.groupByIndexed(targetIndex: Int): List<List<T>> {
    if (targetIndex !in this.indices) {
        return emptyList()
    }
    val list = mutableListOf<List<T>>()

    var tempCount = 0
    var tempList = mutableListOf<T>()
    forEach { each: T ->
        if (tempCount < targetIndex) {
            tempList.add(each)
            tempCount++
        }
        if (tempCount + 1 == targetIndex) {
            tempCount = 0
            list.add(tempList)
            tempList = mutableListOf()
        }
    }

    return list
}

fun <T> List<T>?.sizeOrZero() = this?.size ?: 0