/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.dev.mock

import dennis.hinsi.vocubipro.Constants
import dennis.hinsi.vocubipro.model.*
import java.util.*

object MockDataSet {
    val languages = listOf(
        Language(
            id = "language_id_0",
            title = "Sprache 0",
            titleTranslated = "Language 0",
            emoji = MockDataProvider.flagsGrouped().get(0).get(0),
            accentColor = Constants.accentColors().get(0).get(0)
        ),
        Language(
            id = "language_id_1",
            title = "Sprache 1",
            titleTranslated = "Language 1",
            emoji = MockDataProvider.flagsGrouped().get(0).get(1),
            accentColor = Constants.accentColors().get(0).get(1)
        ),
        Language(
            id = "language_id_2",
            title = "Sprache 2",
            titleTranslated = "Language 2",
            emoji = MockDataProvider.flagsGrouped().get(1).get(0),
            accentColor = Constants.accentColors().get(1).get(0)
        ),
        Language(
            id = "language_id_3",
            title = "Sprache 3",
            titleTranslated = "Language 3",
            emoji = MockDataProvider.flagsGrouped().get(1).get(1),
            accentColor = Constants.accentColors().get(0).get(2)
        )
    )

    fun provideGroups(size: Int = 3) = List(size) {
        VocabularyGroup(
            id = UUID.randomUUID().toString(),
            title = "Vocabulary group $it",
            createdAt = System.currentTimeMillis() - (it * 43),
            modifiedAt = System.currentTimeMillis() - (it * 23)
        )
    }

    fun provideChapters(groupId: String, size: Int = 4) = List(size) {
        VocabularyChapter(
            id = UUID.randomUUID().toString(),
            title = "Vocabulary chapter $it",
            groupId = groupId,
            position = it
        )
    }

    fun provideVocabularies(chapterId: String, size: Int = 4) = List(size) {
        Vocabulary(
            id = UUID.randomUUID().toString(),
            chapterId = chapterId,
            entryId =  UUID.randomUUID().toString(),
            text = "lang_0_text_$it"
        )
    }

    fun provideVocabularyEntry(languageId: String, entryId: String, index: Int) =
        VocabularyEntry(
            id = UUID.randomUUID().toString(),
            entryId = entryId,
            text = "lang_1_text_$index",
            phoneticText = "lang_1_ph_text_$index",
            languageId = languageId,
            transcriptionText = "tr_$index"
        )
}