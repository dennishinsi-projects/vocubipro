/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.dev.helper


class DevOptionsRepo(private val reader: PreferenceReader) {

    var showAdditionalInformationEnabled: Boolean = false
        set(value) {
            field = value
            reader.saveBoolean(ADD_INFO_ENABLED, value)
        }
        get() = reader.readBoolean(ADD_INFO_ENABLED)

    companion object {
        private const val ADD_INFO_ENABLED = "add_info_enabled"
    }
}