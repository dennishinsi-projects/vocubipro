/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.dev.mock

import dennis.hinsi.vocubipro.model.*
import java.util.*

object MockDataProvider {


    fun flags() = listOf(
        "\uD83C\uDDE6\uD83C\uDDE9",
        "\uD83C\uDDE6\uD83C\uDDFA",
        "\uD83C\uDDE8\uD83C\uDDE6",
        "\uD83C\uDDE8\uD83C\uDDFA",
        "\uD83C\uDDE9\uD83C\uDDF0",
        "\uD83C\uDDEB\uD83C\uDDEE",
        "\uD83C\uDDEC\uD83C\uDDF7"
    )

    fun flagsGrouped() = listOf(
        listOf(
            "\uD83C\uDDE9\uD83C\uDDEA",
            "\uD83C\uDDEC\uD83C\uDDE7",
            "\uD83C\uDDEE\uD83C\uDDF9",
            "\uD83C\uDDEA\uD83C\uDDF8"
        ),
        listOf(
            "\uD83C\uDDEF\uD83C\uDDF5",
            "\uD83C\uDDF0\uD83C\uDDF7",
            "\uD83C\uDDF9\uD83C\uDDF7",
            "\uD83C\uDDF8\uD83C\uDDEA"
        )
    )

    fun accentColors() = listOf(
        "#FFBB86FC",
        "#FF6200EE",
        "#F44336",
        "#CDDC39"
    )

    fun provideLanguageList() = listOf(
        Language(
            id = "l_0",
            title = "Language 0",
            titleTranslated = "Sprache 0",
            emoji = "\uD83C\uDDE9\uD83C\uDDEA",
            accentColor = "#FF0099CC"
        ),
        Language(
            id = "l_1",
            title = "Language 1",
            titleTranslated = "Sprache 1",
            emoji = "\uD83C\uDDEF\uD83C\uDDF5",
            accentColor = "#FF0088CC"
        ),
        Language(
            id = "l_2",
            title = "Language 2",
            titleTranslated = "Sprache 2",
            emoji = "\uD83C\uDDEA\uD83C\uDDF8",
            accentColor = "#FF0066CC"
        )
    )

    fun provideVocabularyGroupList(size: Int = 7) = List(size) {
        VocabularyGroup(
            id = "g_$it",
            title = "Vocabulary group $it",
            createdAt = System.currentTimeMillis() - (it * 43),
            modifiedAt = System.currentTimeMillis() - (it * 23)
        )
    }

    fun provideVocabularyChapterList(size: Int = 12) = List(size) {
        VocabularyChapter(
            id = "c_$it",
            title = "Vocabulary chapter $it",
            groupId = "g_$it",
            position = it
        )
    }

    fun provideVocabularyList(size: Int = 30) = List(size) {
        Vocabulary(
            id = "c_$it",
            chapterId = "c_$it",
            entryId = "e_$it",
            ""
        )
    }

    fun provideVocabularyEntryList(size: Int = 3, entryId: String) = List(size) {
        VocabularyEntry(
            id = UUID.randomUUID().toString(),
            entryId = entryId,
            languageId = "l_$it",
            text = "text_$it",
            phoneticText = "phoneticText_$it",
            transcriptionText = "transcriptionText_$it"
        )
    }

}