/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import dennis.hinsi.vocubipro.model.Language
import kotlinx.coroutines.flow.Flow

@Dao
interface LanguageDao {
    @Insert
    suspend fun insert(data: Language)

    @Update
    suspend fun update(data: Language)

    @Query("DELETE FROM table_language WHERE id = :id")
    suspend fun delete(id: String)

    @Query("SELECT * FROM table_language WHERE id = :id")
    suspend fun load(id: String): Language

    @Query("SELECT * FROM table_language")
    fun loadAll(): Flow<List<Language>>
}