/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import dennis.hinsi.vocubipro.model.VocabularyChapter
import kotlinx.coroutines.flow.Flow

@Dao
interface VocabularyChapterDao {
    @Insert
    suspend fun insert(data: VocabularyChapter)

    @Update
    suspend fun update(data: VocabularyChapter)

    @Query("DELETE FROM table_vocabulary_chapter WHERE id = :chapterId")
    suspend fun delete(chapterId: String)

    @Query("SELECT * FROM table_vocabulary_chapter WHERE id = :chapterId")
    suspend fun load(chapterId: String): VocabularyChapter

    @Query("SELECT * FROM table_vocabulary_chapter WHERE group_id = :groupId")
    suspend fun loadAll(groupId: String): List<VocabularyChapter>

    @Query("SELECT COUNT(*) FROM table_vocabulary_chapter WHERE group_id = :groupId")
    suspend fun numberOfChaptersOfGroup(groupId: String): Int

    @Query("SELECT * FROM table_vocabulary_chapter")
    fun loadAll(): Flow<List<VocabularyChapter>>
}