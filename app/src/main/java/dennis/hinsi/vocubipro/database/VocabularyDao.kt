/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import dennis.hinsi.vocubipro.model.Vocabulary
import kotlinx.coroutines.flow.Flow

@Dao
interface VocabularyDao {
    @Insert
    suspend fun insert(data: Vocabulary)

    @Update
    suspend fun update(data: Vocabulary)

    @Query("DELETE FROM table_vocabulary WHERE id = :vocabularyId")
    suspend fun delete(vocabularyId: String)

    @Query("SELECT * FROM table_vocabulary WHERE id = :vocabularyId")
    suspend fun load(vocabularyId: String): Vocabulary

    @Query("SELECT COUNT(*) FROM table_vocabulary WHERE chapter_id = :chapterId")
    suspend fun numberOfVocabulariesOfChapter(chapterId: String): Int

    @Query("SELECT * FROM table_vocabulary WHERE chapter_id = :chapterId")
    suspend fun loadAll(chapterId: String): List<Vocabulary>

    @Query("SELECT * FROM table_vocabulary")
    fun loadAll(): Flow<List<Vocabulary>>
}