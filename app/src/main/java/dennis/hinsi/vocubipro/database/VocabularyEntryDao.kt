/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import dennis.hinsi.vocubipro.model.VocabularyEntry
import kotlinx.coroutines.flow.Flow

@Dao
interface VocabularyEntryDao {
    @Insert
    suspend fun insert(data: VocabularyEntry)

    @Update
    suspend fun update(data: VocabularyEntry)

    @Query("DELETE FROM table_vocabulary_vocabulary_entry WHERE id = :id")
    suspend fun delete(id: String)

    @Query("SELECT * FROM table_vocabulary_vocabulary_entry WHERE id = :id")
    suspend fun load(id: String): VocabularyEntry

    @Query("SELECT * FROM table_vocabulary_vocabulary_entry WHERE id = :id")
    suspend fun loadAll(id: String): List<VocabularyEntry>

    @Query("DELETE FROM table_vocabulary_vocabulary_entry WHERE entry_id = :entryId")
    suspend fun deleteAllWithEntryId(entryId: String)

    @Query("SELECT * FROM table_vocabulary_vocabulary_entry WHERE entry_id = :entryId")
    suspend fun loadWithEntryId(entryId: String): VocabularyEntry

    @Query("SELECT * FROM table_vocabulary_vocabulary_entry WHERE entry_id = :entryId")
    suspend fun loadAllWithEntryId(entryId: String): List<VocabularyEntry>

    @Query("SELECT * FROM table_vocabulary_vocabulary_entry")
    fun loadAll(): Flow<List<VocabularyEntry>>
}