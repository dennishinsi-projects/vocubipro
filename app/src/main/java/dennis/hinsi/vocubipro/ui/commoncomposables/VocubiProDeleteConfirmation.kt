/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.commoncomposables

import androidx.compose.foundation.layout.*
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import dennis.hinsi.vocubipro.R

@Composable
fun VocubiProDeleteConfirmation(
    modifier: Modifier = Modifier,
    title: String,
    canDelete: (Boolean) -> Unit
) {
    VocubiProBottomSheetContainer(
        modifier = modifier,
        title = {
            Text(
                text = stringResource(id = R.string.common_text_delete_title),
                fontWeight = FontWeight.Bold,
                fontSize = 18.sp
            )
        },
        content = {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
            ) {
                Text(
                    text = stringResource(id = R.string.common_text_delete_confirmation, title),
                    fontSize = 16.sp,
                    modifier = Modifier.align(Alignment.CenterHorizontally)
                )
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 16.dp)
                ) {
                    Button(
                        modifier = Modifier
                            .weight(1f)
                            .padding(start = 16.dp, top = 10.dp, bottom = 10.dp, end = 8.dp),
                        onClick = { canDelete(false) },
                        colors = ButtonDefaults.buttonColors(
                            containerColor = colorResource(id = android.R.color.system_accent2_400)
                        ),
                        content = {
                            Text(
                                text = stringResource(id = R.string.common_text_cancel),
                            )
                        }
                    )
                    Button(
                        modifier = Modifier
                            .weight(1f)
                            .padding(end = 16.dp, top = 10.dp, bottom = 10.dp, start = 8.dp),
                        onClick = { canDelete(true) },
                        colors = ButtonDefaults.buttonColors(
                            containerColor = colorResource(id = R.color.delete_red)
                        ),
                        content = {
                            Text(
                                text = stringResource(id = R.string.common_text_confirm),
                            )
                        }
                    )
                }
            }
        }
    )

}