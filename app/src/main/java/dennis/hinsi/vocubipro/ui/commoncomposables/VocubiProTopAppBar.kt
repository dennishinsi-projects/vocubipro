/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.commoncomposables

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import dennis.hinsi.vocubipro.R
import dennis.hinsi.vocubipro.ui.theme.AppStyle

@Composable
fun VocubiTopAppBar(
    title: @Composable () -> Unit,
    navBackClickEnabled: Boolean = true,
    onNavBackClick: () -> Unit
) {
    TopAppBar(
        title = title,
        elevation = 15.dp,
        backgroundColor = colorResource(id = AppStyle.STATUS_BAR_COLOR_REF),
        navigationIcon = {
            Image(
                painter = painterResource(id = R.drawable.ic_arrow_back),
                contentDescription = null,
                colorFilter = ColorFilter.tint(Color.Black),
                modifier = Modifier
                    .size(50.dp)
                    .scale(0.6F)
                    .clip(CircleShape)
                    .clickable(navBackClickEnabled) { onNavBackClick() }
            )
        }
    )
}