/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.chapter.overview

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CardDefaults
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import dennis.hinsi.vocubipro.R
import dennis.hinsi.vocubipro.dev.helper.DevOptions
import dennis.hinsi.vocubipro.util.ListIndexInt
import dennis.hinsi.vocubipro.model.VocabularyChapter
import dennis.hinsi.vocubipro.ui.commoncomposables.*
import dennis.hinsi.vocubipro.util.notNullOrEmpty
import dennis.hinsi.vocubipro.util.orEmpty
import dennis.hinsi.vocubipro.util.sizeOrZero
import dennis.hinsi.vocubipro.util.waitForStateResumed
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ChapterMainComposable(
    viewModel: ChapterViewModel = hiltViewModel(),
    navController: NavController,
    groupId: String
) {
    val storedGroup by viewModel.loadGroup(groupId).waitForStateResumed(null)
    val storedChapterList by viewModel.loadChapters(groupId).waitForStateResumed(null)

    val modalBottomSheetState = rememberModalBottomSheetState(ModalBottomSheetValue.Hidden)
    val modalBottomSheetType = remember { mutableStateOf(ChapterBottomSheetType.NONE) }
    val coroutineScope = rememberCoroutineScope()
    val remChapterIdForDeletion = remember { mutableStateOf<String?>(null) }

    ModalBottomSheetLayout(
        sheetShape = RoundedCornerShape(topStart = 20.dp, topEnd = 20.dp),
        sheetState = modalBottomSheetState,
        sheetContent = {
            Box(modifier = Modifier.defaultMinSize(minHeight = 1.dp)) {
                when (modalBottomSheetType.value) {
                    ChapterBottomSheetType.GROUP_DELETE_CONFIRMATION -> {
                        VocubiProDeleteConfirmation(
                            title = storedGroup?.title.orEmpty(),
                            canDelete = { yes ->
                                if (yes) {
                                    viewModel.deleteGroup(groupId)
                                    navController.popBackStack()
                                } else {
                                    coroutineScope.launch {
                                        modalBottomSheetType.value = ChapterBottomSheetType.NONE
                                        modalBottomSheetState.hide()
                                    }
                                }
                            }
                        )
                    }
                    ChapterBottomSheetType.CHAPTER_DELETE_CONFIRMATION -> {
                        VocubiProDeleteConfirmation(
                            title = "sheetTitle",
                            canDelete = { yes ->
                                if (yes) {
                                    remChapterIdForDeletion.value?.let(viewModel::deleteChapter)
                                    navController.popBackStack()
                                } else {
                                    coroutineScope.launch {
                                        modalBottomSheetType.value = ChapterBottomSheetType.NONE
                                        modalBottomSheetState.hide()
                                    }
                                }
                            }
                        )
                    }
                    else -> Unit
                }
            }
        },
        content = {
            Scaffold(modifier = Modifier.fillMaxSize(),
                topBar = {
                    ChapterTopBar(
                        title = storedGroup?.title.orEmpty(),
                        navController = navController
                    )
                },
                bottomBar = {
                    ChapterBottomBar(
                        onNavigationClick = {
                            when (it.id) {
                                ChapterOverViewBottomBarIdDelete -> {
                                    modalBottomSheetType.value =
                                        ChapterBottomSheetType.GROUP_DELETE_CONFIRMATION
                                    coroutineScope.launch {
                                        modalBottomSheetState.show()
                                    }
                                }
                                ChapterOverViewBottomBarIdAdd -> {
                                    navController.navigate("chapter_detail?group_id=${groupId}&chapter_position=${storedChapterList.sizeOrZero()}")
                                }
                            }
                        }
                    )
                },
                content = {
                    storedChapterList.notNullOrEmpty { list ->
                        ChapterContentFilled(list,
                            onClick = {
                                navController.navigate("vocabulary?&chapter_id=${it.id}")
                            },
                            onEditClick = { index: ListIndexInt, vc: VocabularyChapter ->
                                navController.navigate("chapter_detail?group_id=${groupId}&chapter_id=${vc.id}&chapter_position=${index}")
                            },
                            onDeleteClick = {
                                remChapterIdForDeletion.value = it.id
                                coroutineScope.launch {
                                    modalBottomSheetType.value = ChapterBottomSheetType.CHAPTER_DELETE_CONFIRMATION
                                    modalBottomSheetState.show()
                                }
                            }
                        )
                    }
                }
            )
        }
    )
}

@Composable
private fun ChapterContentFilled(
    list: List<VocabularyChapter>,
    onClick: (VocabularyChapter) -> Unit,
    onEditClick: (ListIndexInt, VocabularyChapter) -> Unit,
    onDeleteClick: (VocabularyChapter) -> Unit
) {
    val current = DevOptions.optionsRepo
    LazyColumn {
        list.forEachIndexed { index, item ->
            item {
                ChapterItem(
                    onClick = { onClick(item) },
                    onEditClick = { onEditClick(index, item) },
                    onDeleteClick = { onDeleteClick(item) },
                    title = item.title,
                    count = item.vocabularyCount
                )
            }
            if (current?.showAdditionalInformationEnabled == true) {
                item {
                    Column {
                        Text(
                            modifier = Modifier.padding(horizontal = 16.dp),
                            text = "Index: ${index}; $item}"
                        )
                        Divider(modifier = Modifier.padding(horizontal = 16.dp))
                    }
                }
            }
        }
        item {
            Spacer(modifier = Modifier.height(80.dp))
        }
    }
}

@Composable
private fun ChapterTopBar(title: String, navController: NavController) {
    VocubiTopAppBar(
        title = { Text(text = title, color = Color.Black) },
        onNavBackClick = { navController.popBackStack() }
    )
}

@Composable
private fun ChapterBottomBar(onNavigationClick: (NavigationBarEntry) -> Unit) {
    VocubiProBottomNavigationBar(
        entries = listOf(
            NavigationBarEntry(
                id = ChapterOverViewBottomBarIdDelete,
                title = R.string.common_text_delete,
                icon = R.drawable.ic_delete
            ),
            NavigationBarEntry(
                id = ChapterOverViewBottomBarIdAdd,
                title = R.string.common_text_add,
                icon = R.drawable.ic_add
            )
        ),
        onClick = { onNavigationClick(it) }
    )
}

@Composable
private fun ChapterItem(
    title: String,
    count: Int,
    onClick: () -> Unit,
    onEditClick: () -> Unit,
    onDeleteClick: () -> Unit,
) {
    androidx.compose.material3.Card(
        elevation = CardDefaults.cardElevation(defaultElevation = 5.dp),
        colors = CardDefaults.cardColors(containerColor = Color.White),
        modifier = Modifier.padding(start = 10.dp, end = 10.dp, top = 10.dp)
    ) {
        Column {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .defaultMinSize(minHeight = 80.dp)
                    .clickable { onClick() }
            ) {
                Image(
                    painter = painterResource(id = R.drawable.ic_chapter),
                    contentDescription = title,
                    modifier = Modifier
                        .align(Alignment.CenterVertically)
                        .width(60.dp)
                )
                Text(
                    text = title,
                    modifier = Modifier
                        .fillMaxWidth()
                        .align(Alignment.CenterVertically),
                    fontSize = 18.sp,
                )
            }

            Divider(modifier = Modifier.padding(horizontal = 16.dp))

            Box(modifier = Modifier.fillMaxWidth()) {
                Text(
                    modifier = Modifier
                        .align(Alignment.CenterStart)
                        .padding(start = 16.dp),
                    text = if (count == 1) {
                        stringResource(id = R.string.chapter_overview_item_text_single, count)
                    } else {
                        stringResource(id = R.string.chapter_overview_item_text_multiple, count)
                    }
                )
                Row(
                    modifier = Modifier
                        .align(Alignment.CenterEnd)
                        .padding(10.dp)
                ) {
                    androidx.compose.material3.Button(
                        modifier = Modifier.padding(horizontal = 5.dp),
                        onClick = { onDeleteClick() },
                        colors = ButtonDefaults.buttonColors(
                            containerColor = colorResource(id = R.color.delete_red)
                        ),
                        content = {
                            Image(
                                modifier = Modifier.align(Alignment.CenterVertically),
                                painter = painterResource(id = R.drawable.ic_delete),
                                contentDescription = "",
                                colorFilter = ColorFilter.tint(Color.White)
                            )
                        }
                    )
                    androidx.compose.material3.Button(
                        modifier = Modifier.padding(horizontal = 5.dp),
                        onClick = { onEditClick() },
                        colors = ButtonDefaults.buttonColors(
                            containerColor = colorResource(id = android.R.color.system_accent2_400)
                        ),
                        content = {
                            Image(
                                modifier = Modifier.align(Alignment.CenterVertically),
                                painter = painterResource(id = R.drawable.ic_edit),
                                contentDescription = "",
                                colorFilter = ColorFilter.tint(Color.White)
                            )
                        }
                    )
                }
            }
        }
    }
}