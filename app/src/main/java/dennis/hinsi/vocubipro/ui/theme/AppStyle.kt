/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.theme

import androidx.compose.ui.unit.dp


object AppStyle {
    const val STATUS_BAR_COLOR_REF = android.R.color.system_accent1_100
    const val NAVIGATION_BAR_COLOR_REF = android.R.color.system_neutral1_50
    const val BACKGROUND_COLOR = android.R.color.system_neutral1_10
    const val FOREGROUND_COLOR = android.R.color.system_neutral1_50
    const val DIVIDER_COLOR = android.R.color.system_neutral1_50
    const val DIVIDER_COLOR_DARK = android.R.color.system_neutral1_200
    val BOTTOM_NAVIGATION_ELEVATION = 20.dp
}