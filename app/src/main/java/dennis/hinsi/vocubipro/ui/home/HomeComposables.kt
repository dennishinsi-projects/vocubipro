/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.home

import android.annotation.SuppressLint
import androidx.annotation.StringRes
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import dennis.hinsi.vocubipro.R
import dennis.hinsi.vocubipro.ui.commoncomposables.NavigationBarEntry
import dennis.hinsi.vocubipro.ui.commoncomposables.VocubiProBottomNavigationBar
import dennis.hinsi.vocubipro.util.waitForStateResumed
import dennis.hinsi.vocubipro.ui.theme.AppStyle

private const val BOTTOM_NAV_ID_SETTINGS = 0
private const val BOTTOM_NAV_ID_LANGUAGE = 1
private const val BOTTOM_NAV_ID_ADD = 2

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HomeView(navController: NavController, homeViewModel: HomeViewModel = hiltViewModel()) {
    val languageList = homeViewModel.languageList.waitForStateResumed(initialValue = emptyList())
    val groupList = homeViewModel.groupList.waitForStateResumed(initialValue = emptyList())

    Scaffold(
        topBar = {
            Surface(
                shadowElevation = 10.dp
            ) {
                Box(
                    modifier = Modifier
                        .height(50.dp)
                        .fillMaxWidth()
                        .background(colorResource(id = AppStyle.STATUS_BAR_COLOR_REF))
                ) {
                    Text(
                        modifier = Modifier.align(Alignment.Center),
                        text = stringResource(id = R.string.app_name),
                        fontSize = 20.sp
                    )
                }
            }
        },
        bottomBar = {
            VocubiProBottomNavigationBar(
                entries = listOf(
                    NavigationBarEntry(
                        id = BOTTOM_NAV_ID_SETTINGS,
                        title = R.string.home_bottom_nav_entry_settings,
                        icon = R.drawable.ic_settings
                    ),
                    NavigationBarEntry(
                        id = BOTTOM_NAV_ID_LANGUAGE,
                        title = R.string.home_bottom_nav_entry_language,
                        icon = R.drawable.ic_language
                    ),
                    NavigationBarEntry(
                        id = BOTTOM_NAV_ID_ADD,
                        title = R.string.home_bottom_nav_entry_add_group,
                        icon = R.drawable.ic_add
                    )
                ),
                onClick = {
                    when (it.id) {
                        BOTTOM_NAV_ID_SETTINGS -> navController.navigate("settings")
                        BOTTOM_NAV_ID_LANGUAGE -> navController.navigate("language_overview")
                        BOTTOM_NAV_ID_ADD -> navController.navigate("group_detail")
                    }
                }
            )
        },
        content = {
            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
                    .background(colorResource(id = AppStyle.BACKGROUND_COLOR))
            ) {
                item {
                    Spacer(modifier = Modifier.height(55.dp))
                }
                item {
                    LanguageHomeCard(
                        list = languageList.value,
                        onClick = {
                            navController.navigate("language_overview")
                        }
                    )
                }
                groupList.value.forEach {
                    item {
                        VocabularyGroupHomeCard(
                            onClick = { navController.navigate("chapter?group_id=${it.id}") },
                            title = it.title,
                            chapterCount = it.chapterCount
                        )
                    }
                }
                item {
                    Spacer(
                        modifier = Modifier
                            .height(80.dp)
                    )
                }
            }
        }
    )
}

@Composable
fun HomeCardBase(
    modifier: Modifier = Modifier,
    @StringRes title: Int,
    content: @Composable ColumnScope.() -> Unit,
    onClick: () -> Unit
) {
    Card(
        shape = RoundedCornerShape(20.dp),
        modifier = modifier
            .padding(top = 16.dp)
            .padding(horizontal = 16.dp),
        elevation = CardDefaults.cardElevation(defaultElevation = 10.dp),
        colors = CardDefaults.cardColors(containerColor = Color.White),
        content = {
            Box(modifier = modifier.clickable { onClick() }) {
                Column(
                    modifier = Modifier
                        .padding(10.dp)
                        .align(Alignment.Center)
                ) {
                    Text(
                        modifier = Modifier
                            .padding(5.dp),
                        text = stringResource(id = title)
                    )
                    Divider(
                        modifier = Modifier
                            .padding(horizontal = 5.dp),
                        color = colorResource(id = AppStyle.DIVIDER_COLOR)
                    )
                    content()
                }
            }
        }
    )
}
