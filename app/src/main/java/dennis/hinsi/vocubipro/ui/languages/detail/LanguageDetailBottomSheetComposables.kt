/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.languages.detail

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import dennis.hinsi.vocubipro.Constants
import dennis.hinsi.vocubipro.dev.mock.MockDataProvider
import dennis.hinsi.vocubipro.R
import dennis.hinsi.vocubipro.ui.commoncomposables.VocubiProBottomSheetContainer
import dennis.hinsi.vocubipro.util.colorFromHexOrDefault

@Composable
fun LanguageDetailFlagSelectorBottomSheet(
    selectedFlag: String,
    onFlagChange: (String) -> Unit
) {
    val flags = MockDataProvider.flagsGrouped()

    VocubiProBottomSheetContainer(
        title = { Text(text = "Select Flag", fontWeight = FontWeight.Bold, fontSize = 18.sp) },
        content = {
            Card(
                shape = RoundedCornerShape(20.dp),
                modifier = Modifier.padding(10.dp)
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(Color.White)
                ) {
                    flags.forEach { rowList ->
                        Row {
                            rowList.forEach {
                                Box(modifier = Modifier
                                    .weight(1f)
                                    .align(Alignment.CenterVertically)
                                    .height(80.dp)
                                    .clickable { onFlagChange(it) }
                                ) {
                                    if (it == selectedFlag) {
                                        Image(
                                            modifier = Modifier
                                                .align(Alignment.TopEnd)
                                                .padding(10.dp),
                                            painter = painterResource(id = R.drawable.ic_check_dot),
                                            contentDescription = null
                                        )
                                    }
                                    Text(
                                        text = it,
                                        modifier = Modifier.align(Alignment.Center),
                                        fontSize = 28.sp
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }
    )
}

@Composable
fun LanguageDetailColorSelectorBottomSheet(
    selectedColor: String,
    onColorChange: (String) -> Unit
) {
    val colors = Constants.accentColors()

    VocubiProBottomSheetContainer(
        title = { Text(text = "Select Color", fontWeight = FontWeight.Bold, fontSize = 18.sp) },
        content = {
            Card(
                shape = RoundedCornerShape(20.dp),
                modifier = Modifier.padding(10.dp)
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(Color.White)
                ) {
                    colors.forEach { rowList ->
                        Row {
                            rowList.forEach {
                                Box(modifier = Modifier
                                    .weight(1f)
                                    .align(Alignment.CenterVertically)
                                    .height(80.dp)
                                    .clickable { onColorChange(it) }
                                ) {
                                    if (it == selectedColor) {
                                        Image(
                                            modifier = Modifier
                                                .align(Alignment.TopEnd)
                                                .padding(10.dp),
                                            painter = painterResource(id = R.drawable.ic_check_dot),
                                            contentDescription = null
                                        )
                                    }
                                    Spacer(
                                        modifier = Modifier
                                            .align(Alignment.Center)
                                            .size(40.dp)
                                            .border(
                                                border = BorderStroke(1.dp, Color.Gray),
                                                shape = CircleShape
                                            )
                                            .padding(10.dp)
                                            .background(
                                                it.colorFromHexOrDefault(Constants.DEFAULT_UNSELECTED_COLOR)

                                            )
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }
    )
}