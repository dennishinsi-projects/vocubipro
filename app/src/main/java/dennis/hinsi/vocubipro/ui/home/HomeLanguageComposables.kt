/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.home

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import dennis.hinsi.vocubipro.dev.mock.MockDataProvider
import dennis.hinsi.vocubipro.R
import dennis.hinsi.vocubipro.model.Language

@Composable
fun LanguageHomeCard(list: List<Language>, onClick: () -> Unit) {
    HomeCardBase(
        title = R.string.home_card_header_languages,
        content = {
            list.forEach {
                LanguageHomeCardEntry(it)
            }
        },
        onClick = onClick
    )
}

@Composable
fun LanguageHomeCardEntry(language: Language) {
    Row(
        modifier = Modifier
            .height(40.dp)
            .fillMaxWidth(),
    ) {
        Text(
            modifier = Modifier
                .align(Alignment.CenterVertically)
                .padding(horizontal = 5.dp),
            text = language.emoji
        )
        Text(
            modifier = Modifier
                .align(Alignment.CenterVertically)
                .padding(horizontal = 5.dp)
                .fillMaxWidth(),
            text = "${language.title} - ${language.titleTranslated}"
        )
    }
}

@Composable
@Preview(showBackground = true)
fun LanguageHomeCardEntryPreview() {
    LanguageHomeCardEntry(language = MockDataProvider.provideLanguageList().first())
}
