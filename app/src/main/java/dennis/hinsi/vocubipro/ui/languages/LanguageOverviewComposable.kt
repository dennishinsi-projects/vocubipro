/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.languages

import android.annotation.SuppressLint
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import dennis.hinsi.vocubipro.Constants
import dennis.hinsi.vocubipro.R
import dennis.hinsi.vocubipro.model.Language
import dennis.hinsi.vocubipro.util.colorFromHexOrDefault
import dennis.hinsi.vocubipro.ui.commoncomposables.NavigationBarEntry
import dennis.hinsi.vocubipro.ui.commoncomposables.VocubiProBottomNavigationBar
import dennis.hinsi.vocubipro.ui.commoncomposables.VocubiTopAppBar
import dennis.hinsi.vocubipro.util.waitForStateResumed

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LanguageOverviewView(
    navController: NavController,
    languageDetailViewModel: LanguageDetailViewModel = hiltViewModel()
) {
    val list = languageDetailViewModel.languageList.waitForStateResumed(emptyList())

    Scaffold(
        topBar = {
            VocubiTopAppBar(
                title = {
                    androidx.compose.material.Text(
                        text = stringResource(id = R.string.language_overview_action_bar_title),
                        color = Color.Black
                    )
                },
                onNavBackClick = { navController.popBackStack() }
            )
        },
        bottomBar = {
            VocubiProBottomNavigationBar(
                entries = listOf(
                    NavigationBarEntry(
                        id = 0,
                        title = R.string.home_bottom_nav_entry_add_group,
                        icon = R.drawable.ic_add
                    )
                ),
                onClick = { navController.navigate("language_detail") }
            )
        },
        content = {
            LazyColumn(
                content = {
                    item {
                        Spacer(modifier = Modifier.height(55.dp))
                    }
                    list.value.forEach {
                        item {
                            LanguageEntryView(
                                modifier = Modifier.clickable {
                                    navController.navigate(
                                        "language_detail?id=${it.id}"
                                    )
                                },
                                language = it
                            )
                        }
                    }
                    item {
                        Spacer(modifier = Modifier.height(80.dp))
                    }
                }
            )
        }
    )
}

@Composable
fun LanguageEntryView(modifier: Modifier, language: Language) {
    Card(
        elevation = CardDefaults.cardElevation(defaultElevation = 5.dp),
        colors = CardDefaults.cardColors(containerColor = Color.White),
        modifier = Modifier
            .padding(start = 10.dp, end = 10.dp, top = 10.dp)
    ) {
        Row(
            modifier = modifier
                .height(50.dp)
                .background(
                    language.accentColor.colorFromHexOrDefault(
                        alpha = Constants.DEFAULT_ACCENT_COLOR_ALPHA,
                        defaultHex = Constants.DEFAULT_UNSELECTED_COLOR
                    )
                )
                .fillMaxWidth(),
        ) {
            Text(
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .padding(start = 16.dp),
                text = language.emoji,
                fontSize = 20.sp
            )
            Text(
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .padding(horizontal = 15.dp)
                    .fillMaxWidth(),
                text = "${language.title} - \"${language.titleTranslated}\""
            )
        }
    }
}