/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.languages.detail

import android.annotation.SuppressLint
import androidx.annotation.StringRes
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material3.*
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import dennis.hinsi.vocubipro.Constants
import dennis.hinsi.vocubipro.dev.mock.MockDataProvider
import dennis.hinsi.vocubipro.R
import dennis.hinsi.vocubipro.ui.commoncomposables.*
import dennis.hinsi.vocubipro.util.colorFromHexOrDefault
import dennis.hinsi.vocubipro.util.onNull
import dennis.hinsi.vocubipro.util.waitForStateResumed
import dennis.hinsi.vocubipro.ui.languages.LanguageDetailViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class, ExperimentalMaterialApi::class)
@Composable
fun LanguageDetailView(
    navController: NavController,
    languageId: String? = null,
    viewModel: LanguageDetailViewModel = hiltViewModel()
) {
    val storedLanguage by viewModel.loadLanguage(languageId).waitForStateResumed(null)

    val rememberInputTitle = remember { mutableStateOf("") }
    val rememberInputTitleTranslated = remember { mutableStateOf("") }
    val rememberSelectedFlag = remember { mutableStateOf("") }
    val rememberSelectedColor = remember { mutableStateOf("") }

    storedLanguage?.let { language ->
        rememberInputTitle.value = language.title
        rememberInputTitleTranslated.value = language.titleTranslated
        rememberSelectedFlag.value = language.emoji
        rememberSelectedColor.value = language.accentColor
    }
    languageId.onNull {
        rememberInputTitle.value = ""
        rememberInputTitleTranslated.value = ""
        rememberSelectedFlag.value = Constants.DEFAULT_UNSELECTED_FLAG
        rememberSelectedColor.value = Constants.DEFAULT_UNSELECTED_COLOR
    }

    val modalBottomSheetState = rememberModalBottomSheetState(ModalBottomSheetValue.Hidden)
    val modalBottomSheetType = remember { mutableStateOf(LanguageDetailBottomSheetType.NONE) }
    val coroutineScope = rememberCoroutineScope()

    ModalBottomSheetLayout(
        sheetShape = RoundedCornerShape(topStart = 20.dp, topEnd = 20.dp),
        sheetState = modalBottomSheetState,
        sheetContent = {
            Box(modifier = Modifier.defaultMinSize(minHeight = 1.dp)) {
                when (modalBottomSheetType.value) {
                    LanguageDetailBottomSheetType.FLAG_SELECTION -> {
                        LanguageDetailFlagSelectorBottomSheet(
                            selectedFlag = rememberSelectedFlag.value,
                            onFlagChange = { rememberSelectedFlag.value = it }
                        )
                    }
                    LanguageDetailBottomSheetType.COLOR_SELECTION -> {
                        LanguageDetailColorSelectorBottomSheet(
                            selectedColor = rememberSelectedColor.value,
                            onColorChange = { rememberSelectedColor.value = it }
                        )
                    }
                    LanguageDetailBottomSheetType.CONFIRM_DELETION -> {
                        if (languageId == null) {
                            VocubiProDeleteNothingConfirmation(
                                canDelete = {
                                    coroutineScope.launch {
                                        modalBottomSheetState.hide()
                                    }
                                }
                            )
                        } else {
                            VocubiProDeleteConfirmation(
                                title = rememberInputTitle.value,
                                canDelete = {
                                    if (it) {
                                        viewModel.delete(languageId)
                                        navController.popBackStack()
                                    } else {
                                        coroutineScope.launch {
                                            modalBottomSheetState.hide()
                                        }
                                    }
                                }
                            )
                        }
                    }
                    else -> Unit
                }
            }
        },
        content = {
            Scaffold(
                topBar = {
                        VocubiTopAppBar(
                            title = {
                                androidx.compose.material.Text(
                                    text = storedLanguage?.title ?: "",
                                    color = Color.Black
                                )
                            },
                            onNavBackClick = { navController.popBackStack() }
                        )
                },
                bottomBar = {
                    LanguageDetailBottomBar(
                        onNavigationClick = {
                            when(it.id) {
                                LanguageDetailBottomBarIdDelete -> {
                                    coroutineScope.launch {
                                        modalBottomSheetType.value = LanguageDetailBottomSheetType.CONFIRM_DELETION
                                        modalBottomSheetState.show()
                                    }
                                }
                                LanguageDetailBottomBarIdSave -> {
                                    viewModel.submitData(
                                        id = languageId,
                                        title = rememberInputTitle.value,
                                        titleTranslated = rememberInputTitleTranslated.value,
                                        flag = rememberSelectedFlag.value,
                                        accentColor = rememberSelectedColor.value
                                    )
                                    navController.popBackStack()
                                }
                            }
                        }
                    )
                },
                content = {
                    LanguageDetailContent(
                        rememberSelectedFlag,
                        coroutineScope,
                        modalBottomSheetType,
                        modalBottomSheetState,
                        rememberSelectedColor,
                        rememberInputTitle,
                        rememberInputTitleTranslated
                    )
                }
            )
        }
    )
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
private fun LanguageDetailContent(
    rememberSelectedFlag: MutableState<String>,
    coroutineScope: CoroutineScope,
    modalBottomSheetType: MutableState<LanguageDetailBottomSheetType>,
    modalBottomSheetState: ModalBottomSheetState,
    rememberSelectedColor: MutableState<String>,
    rememberInputTitle: MutableState<String>,
    rememberInputTitleTranslated: MutableState<String>
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(top = 55.dp)
    ) {
        LanguageFlagSelectorCard(
            selectedFlag = rememberSelectedFlag.value,
            onChange = {
                coroutineScope.launch {
                    modalBottomSheetType.value =
                        LanguageDetailBottomSheetType.FLAG_SELECTION
                    modalBottomSheetState.show()
                }
            })
        LanguageAccentColorSelectorCard(
            selectedColor = rememberSelectedColor.value,
            onChange = {
                coroutineScope.launch {
                    modalBottomSheetType.value = LanguageDetailBottomSheetType.COLOR_SELECTION
                    modalBottomSheetState.show()
                }
            })
        LanguageNameSelectorCard(
            storedText = rememberInputTitle.value,
            storedTextTranslated = rememberInputTitleTranslated.value,
            onChangeText = { rememberInputTitle.value = it },
            onChangeTranslatedText = { rememberInputTitleTranslated.value = it }
        )
    }
}

@Composable
private fun LanguageDetailBottomBar(onNavigationClick: (NavigationBarEntry) -> Unit) {
    VocubiProBottomNavigationBar(
        entries = listOf(
            NavigationBarEntry(
                id = LanguageDetailBottomBarIdDelete,
                title = R.string.language_detail_nav_entry_delete,
                icon = R.drawable.ic_delete
            ),
            NavigationBarEntry(
                id = LanguageDetailBottomBarIdSave,
                title = R.string.language_detail_nav_entry_save,
                icon = R.drawable.ic_save
            )
        ),
        onClick = { onNavigationClick(it) }
    )
}

@Composable
fun LanguageDetailCard(@StringRes title: Int, content: @Composable () -> Unit) {
    Card(
        elevation = CardDefaults.cardElevation(defaultElevation = 5.dp),
        colors = CardDefaults.cardColors(containerColor = Color.White),
        modifier = Modifier
            .padding(start = 10.dp, end = 10.dp, top = 10.dp)
    ) {
        Column {
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 10.dp, horizontal = 16.dp),
                text = stringResource(id = title),
                fontWeight = FontWeight.Bold
            )
            Box(
                modifier = Modifier
                    .padding(horizontal = 16.dp)
                    .padding(bottom = 10.dp)
            ) {
                content()
            }
        }
    }
}

@Composable
fun LanguageFlagSelectorCard(selectedFlag: String, onChange: (String) -> Unit) {
    LanguageDetailCard(
        title = R.string.language_detail_header_flag,
        content = {
            Box(modifier = Modifier.fillMaxWidth()) {
                Text(
                    text = selectedFlag,
                    modifier = Modifier
                        .border(
                            border = BorderStroke(1.dp, Color.Gray),
                            shape = CircleShape
                        )
                        .padding(10.dp),
                    fontSize = 18.sp
                )
                Button(
                    colors = ButtonDefaults.buttonColors(
                        containerColor = colorResource(id = android.R.color.system_accent2_400)
                    ),
                    modifier = Modifier.align(Alignment.CenterEnd),
                    onClick = {
                        onChange(MockDataProvider.flags()[1])
                    }
                ) {
                    Text(text = stringResource(id = R.string.common_text_select))
                }
            }
        }
    )
}

@Composable
fun LanguageAccentColorSelectorCard(selectedColor: String, onChange: (String) -> Unit) {
    LanguageDetailCard(
        title = R.string.language_detail_header_accent_color,
        content = {
            Box(modifier = Modifier.fillMaxWidth()) {
                Spacer(
                    modifier = Modifier
                        .size(40.dp)
                        .border(
                            border = BorderStroke(1.dp, Color.Gray),
                            shape = CircleShape
                        )
                        .padding(10.dp)
                        .background(selectedColor.colorFromHexOrDefault(Constants.DEFAULT_UNSELECTED_COLOR))
                )
                Button(
                    colors = ButtonDefaults.buttonColors(
                        containerColor = colorResource(id = android.R.color.system_accent2_400)
                    ),
                    modifier = Modifier.align(Alignment.CenterEnd),
                    onClick = {
                        onChange(selectedColor)
                    }
                ) {
                    Text(text = stringResource(id = R.string.common_text_select))
                }
            }
        }
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LanguageNameSelectorCard(
    storedText: String,
    storedTextTranslated: String,
    onChangeText: (String) -> Unit,
    onChangeTranslatedText: (String) -> Unit
) {
    LanguageDetailCard(
        title = R.string.language_detail_header_text_selector,
        content = {
            Column {
                OutlinedTextField(
                    modifier = Modifier.fillMaxWidth(),
                    value = storedText,
                    onValueChange = {
                        onChangeText(it)
                    },
                    label = {
                        Text(stringResource(id = R.string.language_detail_name))
                    }
                )
                OutlinedTextField(
                    modifier = Modifier.fillMaxWidth(),
                    value = storedTextTranslated,
                    onValueChange = {
                        onChangeTranslatedText(it)
                    },
                    label = {
                        Text(stringResource(id = R.string.language_detail_translated_name))
                    }
                )
            }
        }
    )
}
