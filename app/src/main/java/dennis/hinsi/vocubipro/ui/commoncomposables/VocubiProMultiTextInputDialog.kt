/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.commoncomposables

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import dennis.hinsi.vocubipro.R

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun VocubiProMultiTextInputDialogComposable(
    title: String,
    hint: List<String>,
    text: List<String>,
    onCancelClick: () -> Unit,
    onSubmitClick: (List<String>) -> Unit
) {
    val remTextList = remember { mutableStateListOf<String>().apply { addAll(text) } }

    VocubiProBottomSheetContainer(
        title = { Text(text = title, fontWeight = FontWeight.Bold, fontSize = 18.sp) },
        content = {
            Column {
                remTextList.forEachIndexed { index, eachText ->
                    TextInputContainer(
                        text = text[index],
                        hint = hint[index],
                        onChange = { text ->
                            remTextList.removeAt(index)
                            remTextList.add(index, text)
                        }
                    )
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 16.dp)
                ) {
                    Button(
                        modifier = Modifier
                            .weight(1f)
                            .padding(start = 16.dp, top = 10.dp, bottom = 10.dp, end = 8.dp),
                        onClick = { onCancelClick() },
                        colors = ButtonDefaults.buttonColors(
                            containerColor = colorResource(id = android.R.color.system_accent2_400)
                        ),
                        content = {
                            Text(
                                text = stringResource(id = R.string.common_text_cancel),
                            )
                        }
                    )
                    Button(
                        modifier = Modifier
                            .weight(1f)
                            .padding(end = 16.dp, top = 10.dp, bottom = 10.dp, start = 8.dp),
                        onClick = { onSubmitClick(remTextList) },
                        colors = ButtonDefaults.buttonColors(
                            containerColor = colorResource(id = R.color.delete_red)
                        ),
                        content = {
                            Text(
                                text = stringResource(id = R.string.common_text_confirm),
                            )
                        }
                    )
                }
            }
        }
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun TextInputContainer(
    text: String,
    hint: String,
    onChange: (String) -> Unit
) {
    val remText = remember { mutableStateOf(text)}

    OutlinedTextField(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp)
            .padding(top = 10.dp),
        value = remText.value,
        onValueChange = {
            remText.value = it
            onChange(it)
        },
        label = { Text(text = hint) }
    )
}
