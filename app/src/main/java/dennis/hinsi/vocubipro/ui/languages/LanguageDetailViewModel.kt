/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.languages

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dennis.hinsi.vocubipro.model.Language
import dennis.hinsi.vocubipro.util.onNull
import dennis.hinsi.vocubipro.usecases.DataDeletionUC
import dennis.hinsi.vocubipro.usecases.LanguageDataUC
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class LanguageDetailViewModel @Inject constructor(
    private val languageDataUC: LanguageDataUC,
    private val dataDeletionUC: DataDeletionUC
) : ViewModel() {

    val languageList get() = languageDataUC.loadAll()

    fun loadLanguage(languageId: String?) = if (!languageId.isNullOrEmpty()) {
        flow { emit(languageDataUC.loadLanguage(languageId)) }
    } else {
        emptyFlow()
    }

    fun delete(id: String) = viewModelScope.launch {
        dataDeletionUC.deleteLanguage(id)
    }

    fun submitData(
        id: String?,
        title: String,
        titleTranslated: String,
        flag: String,
        accentColor: String
    ) = viewModelScope.launch {
        id?.let { existingId ->
            languageDataUC.update(
                Language(
                    id = existingId,
                    title = title,
                    titleTranslated = titleTranslated,
                    emoji = flag,
                    accentColor = accentColor
                )
            )
        }.onNull {
            languageDataUC.insert(
                Language(
                    id = UUID.randomUUID().toString(),
                    title = title,
                    titleTranslated = titleTranslated,
                    emoji = flag,
                    accentColor = accentColor
                )
            )
        }
    }
}