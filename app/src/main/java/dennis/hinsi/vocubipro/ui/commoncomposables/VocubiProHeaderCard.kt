/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.commoncomposables

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.CardDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

@Composable
fun VocubiProHeaderCard(
    title: @Composable () -> Unit,
    content: @Composable () -> Unit,
    backgroundColor: Color = Color.White,
    onClick: (() -> Unit)? = null
) {
    androidx.compose.material3.Card(
        elevation = CardDefaults.cardElevation(defaultElevation = 5.dp),
        colors = CardDefaults.cardColors(containerColor = Color.White),
        modifier = Modifier
            .padding(start = 10.dp, end = 10.dp, top = 10.dp)
    ) {
        // if onClick is null the all descendants are also not clickable anymore!
        // clickable(isClickable) doesn't work here
        val mod = if (onClick != null) {
            Modifier
                .background(backgroundColor)
                .clickable { onClick() }
        } else {
            Modifier
                .background(backgroundColor)
        }
        Column(
            modifier = mod) {
            title()
            Box(
                modifier = Modifier
                    .padding(horizontal = 16.dp)
                    .padding(bottom = 16.dp)
            ) {
                content()
            }
        }
    }
}