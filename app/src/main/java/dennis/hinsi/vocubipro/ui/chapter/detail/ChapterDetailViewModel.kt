/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.chapter.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dennis.hinsi.vocubipro.model.VocabularyChapter
import dennis.hinsi.vocubipro.usecases.ChapterDataUC
import dennis.hinsi.vocubipro.usecases.DataDeletionUC
import dennis.hinsi.vocubipro.util.ListIndexInt
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class ChapterDetailViewModel @Inject constructor(
    private val chapterDataUC: ChapterDataUC,
    private val deletionUseCase: DataDeletionUC
): ViewModel() {

    fun loadChapter(chapterId: String?) = if (!chapterId.isNullOrEmpty()) {
        flow { emit(chapterDataUC.loadChapter(chapterId)) }
    } else {
        emptyFlow()
    }

    fun submit(
        groupId: String,
        chapterId: String?,
        title: String,
        index: ListIndexInt?
    ) = viewModelScope.launch {
        if (chapterId.isNullOrEmpty()) {
            chapterDataUC.insert(
                VocabularyChapter(
                    id = UUID.randomUUID().toString(),
                    groupId = groupId,
                    title = title,
                    position = index ?: 0
                )
            )
        } else {
            chapterDataUC.update(
                VocabularyChapter(
                    id = chapterId,
                    groupId = groupId,
                    title = title,
                    position = index ?: 0
                )
            )
        }
    }

    fun delete(chapterId: String) = viewModelScope.launch {
        deletionUseCase.deleteChapter(chapterId)
    }

}