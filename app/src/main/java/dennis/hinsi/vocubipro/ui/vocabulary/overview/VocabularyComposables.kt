/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.vocabulary.overview

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material3.CardDefaults
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import dennis.hinsi.vocubipro.Constants
import dennis.hinsi.vocubipro.R
import dennis.hinsi.vocubipro.model.container.VocabularyContainer
import dennis.hinsi.vocubipro.ui.commoncomposables.NavigationBarEntry
import dennis.hinsi.vocubipro.ui.commoncomposables.VocubiProBottomNavigationBar
import dennis.hinsi.vocubipro.ui.commoncomposables.VocubiTopAppBar
import dennis.hinsi.vocubipro.util.colorFromHexOrDefault
import dennis.hinsi.vocubipro.util.orEmpty
import dennis.hinsi.vocubipro.util.waitForStateResumed

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun VocabularyMainComposable(
    viewModel: VocabularyViewModel = hiltViewModel(),
    navController: NavController,
    chapterId: String
) {
    val storedChapter by viewModel.loadChapter(chapterId).waitForStateResumed(null)
    val storedVocabularyContainerList by viewModel.load(chapterId).waitForStateResumed(null)

    Scaffold(modifier = Modifier.fillMaxSize(),
        topBar = {
            VocubiTopAppBar(
                title = {
                    Text(
                        text = storedChapter?.title.orEmpty(),
                        color = Color.Black
                    )
                },
                onNavBackClick = { navController.popBackStack() }
            )
        },
        bottomBar = {
            VocabularyBottomBar(
                onNavigationClick = {
                    when (it.id) {
                        ChapterDetailBottomBarIdAdd -> {
                            navController.navigate("vocabulary_detail?chapter_id=${chapterId}")
                        }
                    }
                }
            )
        },
        content = {
            LazyColumn(content = {
                storedVocabularyContainerList?.forEach {
                    item {
                        VocabularyEntryOverviewItem(
                            container = it,
                            onClick = {
                                navController.navigate("vocabulary_detail?chapter_id=${chapterId}&vocabulary_id=${it.vocabulary.id}")
                            }
                        )
                    }
                }
                item {
                    Spacer(modifier = Modifier.height(80.dp))
                }

            })
        }
    )
}

@Composable
private fun VocabularyBottomBar(onNavigationClick: (NavigationBarEntry) -> Unit) {
    VocubiProBottomNavigationBar(
        entries = listOf(
            NavigationBarEntry(
                id = ChapterDetailBottomBarIdAdd,
                title = R.string.common_text_add,
                icon = R.drawable.ic_add
            )
        ),
        onClick = { onNavigationClick(it) }
    )
}

@Composable
private fun VocabularyEntryOverviewItem(
    container: VocabularyContainer,
    onClick: (VocabularyContainer) -> Unit
) {
    androidx.compose.material3.Card(
        shape = RoundedCornerShape(10.dp),
        elevation = CardDefaults.cardElevation(defaultElevation = 5.dp),
        colors = CardDefaults.cardColors(containerColor = Color.White),
        modifier = Modifier
            .padding(horizontal = 16.dp)
            .padding(top = 15.dp),
        content = {
            Column(modifier = Modifier
                .clickable { onClick(container) }
                .fillMaxWidth()) {
                Text(
                    text = container.vocabulary.text,
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier.padding(horizontal = 16.dp, vertical = 10.dp)
                )
                Column(
                    modifier = Modifier
                        .padding(bottom = 5.dp)
                        .padding(horizontal = 16.dp)
                ) {
                    container.entries.forEach {
                        Row(
                            modifier = Modifier
                                .clip(RoundedCornerShape(20.dp))
                                .padding(vertical = 5.dp)
                                .background(
                                    it.language.accentColor.colorFromHexOrDefault(
                                        alpha = Constants.DEFAULT_ACCENT_COLOR_ALPHA,
                                        defaultHex = Constants.DEFAULT_UNSELECTED_COLOR
                                    )
                                )
                        ) {
                            Text(
                                modifier = Modifier
                                    .padding(start = 10.dp)
                                    .padding(vertical = 10.dp)
                                    .align(Alignment.CenterVertically),
                                text = it.language.title
                            )
                            Box(
                                modifier = Modifier
                                    .padding(start = 10.dp)
                                    .background(Color.White)
                            ) {
                                Text(
                                    modifier = Modifier
                                        .align(Alignment.CenterStart)
                                        .padding(10.dp),
                                    text = it.entry.text
                                )
                            }
                        }
                    }
                }
            }
        }
    )
}
