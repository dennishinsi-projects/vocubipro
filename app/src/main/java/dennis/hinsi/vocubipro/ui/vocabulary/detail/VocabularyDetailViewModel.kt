/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.vocabulary.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dennis.hinsi.vocubipro.model.*
import dennis.hinsi.vocubipro.model.container.VocabularyEntryContainer
import dennis.hinsi.vocubipro.usecases.DataDeletionUC
import dennis.hinsi.vocubipro.usecases.LanguageDataUC
import dennis.hinsi.vocubipro.usecases.VocabularyDataUC
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class VocabularyDetailViewModel @Inject constructor(
    private val languageDataUC: LanguageDataUC,
    private val vocabularyDataUC: VocabularyDataUC,
    private val deletionUC: DataDeletionUC
) : ViewModel() {

    val languages: Flow<List<Language>> get() = languageDataUC.loadAll()
    val infinityEntryFlow: Flow<List<VocabularyEntry>> get() = vocabularyDataUC.allEntries

    fun loadVocabulary(vocabularyId: String?) = if(!vocabularyId.isNullOrEmpty()) {
        flow { emit(vocabularyDataUC.loadVocabulary(vocabularyId)) }
    } else {
        emptyFlow()
    }

    fun submit(alreadyStored: Boolean, vocabulary: Vocabulary) = viewModelScope.launch {
        if (alreadyStored) {
            vocabularyDataUC.update(vocabulary)
        } else {
            vocabularyDataUC.insert(vocabulary)
        }
    }

    fun submitEntry(
        entryId: String,
        vocabularyEntryContainer: VocabularyEntryContainer?,
        language: Language?,
        newText: String,
        newPhoneticText: String,
        newTranscriptionText: String
    ) = viewModelScope.launch {
        if (language == null) {
            return@launch
        }
        if (vocabularyEntryContainer == null) {
            val entry = VocabularyEntry(
                id = UUID.randomUUID().toString(),
                entryId = entryId,
                languageId = language.id,
                phoneticText = newPhoneticText,
                text = newText,
                transcriptionText = newTranscriptionText
            )
            vocabularyDataUC.insertEntry(entry)
        } else {
            vocabularyDataUC.updateEntry(
                vocabularyEntryContainer.entry.copy(
                    text = newText,
                    phoneticText = newPhoneticText,
                    transcriptionText = newTranscriptionText
                )
            )
        }
    }

    fun deleteEntry(id: String) = viewModelScope.launch {
        deletionUC.deleteEntry(id)
    }

    fun deleteVocabulary(id: String, entryId: String) = viewModelScope.launch {
        deletionUC.deleteVocabulary(id,entryId)
    }

}