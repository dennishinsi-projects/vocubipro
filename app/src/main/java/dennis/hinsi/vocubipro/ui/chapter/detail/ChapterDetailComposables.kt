/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.chapter.detail

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import dennis.hinsi.vocubipro.R
import dennis.hinsi.vocubipro.ui.commoncomposables.*
import dennis.hinsi.vocubipro.util.waitForStateResumed
import dennis.hinsi.vocubipro.util.ListIndexInt
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ChapterDetailComposable(
    viewModel: ChapterDetailViewModel = hiltViewModel(),
    navController: NavController,
    groupId: String,
    chapterId: String?,
    chapterPosition: ListIndexInt?
) {
    val storedChapter by viewModel.loadChapter(chapterId).waitForStateResumed(null)

    val modalBottomSheetState = rememberModalBottomSheetState(ModalBottomSheetValue.Hidden)
    val modalBottomSheetType = remember { mutableStateOf(ChapterDetailBottomSheetType.NONE) }
    val coroutineScope = rememberCoroutineScope()
    val rememberChapterTitle = remember { mutableStateOf("") }

    storedChapter?.let {
        rememberChapterTitle.value = it.title
    }

    ModalBottomSheetLayout(
        sheetShape = RoundedCornerShape(topStart = 20.dp, topEnd = 20.dp),
        sheetState = modalBottomSheetState,
        sheetContent = {
            Box(modifier = Modifier.defaultMinSize(minHeight = 1.dp)) {
                if (chapterId == null) {
                    VocubiProDeleteNothingConfirmation(
                        canDelete = {
                            coroutineScope.launch {
                                modalBottomSheetState.hide()
                            }
                        }
                    )
                } else {
                    VocubiProDeleteConfirmation(
                        title = rememberChapterTitle.value,
                        canDelete = {
                            if (it) {
                                viewModel.delete(chapterId)
                                navController.popBackStack()
                            } else {
                                coroutineScope.launch {
                                    modalBottomSheetState.hide()
                                }
                            }
                        }
                    )
                }
            }
        },
        content = {
            Scaffold(modifier = Modifier.fillMaxSize(),
                topBar = {
                    VocubiTopAppBar(
                        title = {
                            Text(
                                text = stringResource(id = R.string.chapter_detail_action_bar_title),
                                color = Color.Black
                            )
                        },
                        onNavBackClick = { navController.popBackStack() }
                    )
                },
                bottomBar = {
                    ChapterDetailBottomBar(
                        onNavigationClick = {
                            when (it.id) {
                                GroupDetailBottomBarIdDelete -> {
                                    coroutineScope.launch {
                                        modalBottomSheetType.value = ChapterDetailBottomSheetType.DELETE_CONFIRMATION
                                        modalBottomSheetState.show()
                                    }
                                }
                                GroupDetailBottomBarIdSave -> {
                                    viewModel.submit(
                                        groupId = groupId,
                                        chapterId = chapterId,
                                        title = rememberChapterTitle.value,
                                        index = chapterPosition
                                    )
                                    navController.popBackStack()
                                }
                            }
                        }
                    )
                },
                content = {
                    ChapterDetailTitleInputCard(
                        cardTitle = stringResource(id = R.string.chapter_detail_title_input_card_header),
                        title = rememberChapterTitle.value,
                        onTitleChange = { rememberChapterTitle.value = it }
                    )
                }
            )
        }
    )

}

@Composable
private fun ChapterDetailBottomBar(onNavigationClick: (NavigationBarEntry) -> Unit) {
    VocubiProBottomNavigationBar(
        entries = listOf(
            NavigationBarEntry(
                id = GroupDetailBottomBarIdDelete,
                title = R.string.common_text_delete,
                icon = R.drawable.ic_delete
            ),
            NavigationBarEntry(
                id = GroupDetailBottomBarIdSave,
                title = R.string.common_text_save,
                icon = R.drawable.ic_save
            )
        ),
        onClick = { onNavigationClick(it) }
    )
}

@Composable
fun ChapterDetailTitleInputCard(
    cardTitle: String,
    title: String,
    onTitleChange: (String) -> Unit
) {
    VocubiProHeaderCard(
        title = {
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 10.dp, horizontal = 16.dp),
                text = cardTitle,
                fontWeight = FontWeight.Bold
            )
        },
        content = {
            VocubiProTextInput(
                modifier = Modifier.fillMaxWidth(),
                value = title,
                onValueChange = { onTitleChange(it) },
                label = { Text(stringResource(id = R.string.chapter_detail_title_input_header)) },
                imeAction = ImeAction.Done
            )
        }
    )
}