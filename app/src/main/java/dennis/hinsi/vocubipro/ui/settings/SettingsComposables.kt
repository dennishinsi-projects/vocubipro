/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.settings

import android.annotation.SuppressLint
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Divider
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import dennis.hinsi.vocubipro.R
import dennis.hinsi.vocubipro.dev.helper.DevOptions
import dennis.hinsi.vocubipro.ui.commoncomposables.VocubiTopAppBar
import dennis.hinsi.vocubipro.util.inverseOrDefault

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun SettingsMainComposable(
    navController: NavController,
    viewModel: SettingsViewModel = hiltViewModel(),
    onLicencesClick: () -> Unit
) {
    val current = DevOptions.optionsRepo
    Scaffold(modifier = Modifier.fillMaxSize(),
        topBar = {
            VocubiTopAppBar(
                title = {
                    Text(
                        text = stringResource(id = R.string.settings_action_bar_title),
                        color = Color.Black
                    )
                },
                onNavBackClick = { navController.popBackStack() }
            )
        },
        content = {
            LazyColumn {
                item {
                    SettingsListHeader(title = stringResource(id = R.string.settings_list_entry_sub_header_about))
                }
                item {
                    SettingsListEntry(
                        title = stringResource(id = R.string.settings_list_entry_open_source_licences),
                        description = null,
                        onClick = onLicencesClick
                    )
                    Divider(modifier = Modifier.padding(horizontal = 16.dp))
                }
                item {
                    SettingsListEntry(
                        title = stringResource(id = R.string.settings_list_entry_app_information),
                        description = "TODO"
                    )
                }

                item {
                    SettingsListHeader(
                        title = stringResource(id = R.string.settings_list_entry_sub_header_developer_options)
                    )
                }
                item {
                    SettingsListEntry(
                        title = stringResource(id = R.string.settings_list_entry_mock_data_title),
                        description = stringResource(id = R.string.settings_list_entry_mock_data_description),
                        onClick = {
                            viewModel.saveMockedData()
                            navController.popBackStack()
                        }
                    )
                }
                item {
                    SettingsListEntry(
                        title = stringResource(id = R.string.settings_list_entry_show_additional_information_title),
                        description = "State: ${if (current?.showAdditionalInformationEnabled == true) "active" else "inactive"}",
                        onClick = {
                            current?.showAdditionalInformationEnabled =
                                current?.showAdditionalInformationEnabled.inverseOrDefault(false)
                            navController.popBackStack()
                        }
                    )
                }
            }
        }
    )
}

@Composable
private fun SettingsListHeader(title: String) {
    Text(
        text = title,
        modifier = Modifier.padding(16.dp),
        fontSize = 18.sp,
        fontWeight = FontWeight.Bold,
        color = colorResource(id = android.R.color.system_accent1_500)
    )
}

@Composable
private fun SettingsListEntry(title: String, description: String?, onClick: (() -> Unit)? = null) {
    Column(
        modifier = Modifier
            .clickable(onClick != null) { onClick?.invoke() }
            .fillMaxWidth()
            .defaultMinSize(minHeight = 50.dp)
            .padding(bottom = 10.dp)
    ) {
        Text(
            text = title,
            modifier = Modifier
                .padding(horizontal = 16.dp)
                .padding(top = 10.dp),
            fontSize = 16.sp,
            fontWeight = FontWeight.Bold
        )
        if (!description.isNullOrEmpty()) {
            Text(
                text = description,
                modifier = Modifier
                    .padding(horizontal = 16.dp)
                    .padding(top = 10.dp),
                fontSize = 14.sp,
                fontWeight = FontWeight.Normal
            )
        }
    }
}