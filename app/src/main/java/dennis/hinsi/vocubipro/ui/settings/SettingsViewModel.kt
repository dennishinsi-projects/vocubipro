/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.settings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dennis.hinsi.vocubipro.dev.mock.MockDataSet
import dennis.hinsi.vocubipro.repository.*
import dennis.hinsi.vocubipro.usecases.ChapterDataUC
import dennis.hinsi.vocubipro.usecases.GroupDataUC
import dennis.hinsi.vocubipro.usecases.LanguageDataUC
import dennis.hinsi.vocubipro.usecases.VocabularyDataUC
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SettingsViewModel @Inject constructor(
    private val languageDataUC: LanguageDataUC,
    private val groupDataUC: GroupDataUC,
    private val chapterDataUC: ChapterDataUC,
    private val vocabularyDataUC: VocabularyDataUC
) : ViewModel() {

    fun saveMockedData() = viewModelScope.launch {
        val languageList = MockDataSet.languages
        languageList.forEach { lang ->
            languageDataUC.insert(lang)
        }

        MockDataSet.provideGroups(size = 3).forEach { group ->
            groupDataUC.insert(group)

            MockDataSet.provideChapters(groupId = group.id, size = 4).forEach { chapter ->
                chapterDataUC.insert(chapter)

                MockDataSet.provideVocabularies(chapterId = chapter.id, size = 4).forEach { voc ->
                    vocabularyDataUC.insert(voc)

                    languageList.forEachIndexed { index, lang ->
                        val entry = MockDataSet.provideVocabularyEntry(
                            languageId = lang.id,
                            entryId = voc.entryId,
                            index = index
                        )
                        vocabularyDataUC.insertEntry(entry)
                    }
                }
            }
        }
    }

}