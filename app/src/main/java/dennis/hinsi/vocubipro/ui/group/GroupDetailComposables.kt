/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.group

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import dennis.hinsi.vocubipro.R
import dennis.hinsi.vocubipro.ui.commoncomposables.*
import dennis.hinsi.vocubipro.util.waitForStateResumed
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun GroupMainComposable(
    viewModel: GroupDetailViewModel = hiltViewModel(),
    navController: NavController,
    groupId: String?
) {
    val storedGroup by viewModel.loadGroup(groupId).waitForStateResumed(null)

    val modalBottomSheetState = rememberModalBottomSheetState(ModalBottomSheetValue.Hidden)
    val modalBottomSheetType = remember { mutableStateOf(GroupDetailBottomSheetType.NONE) }
    val rememberTitle = remember { mutableStateOf("") }
    val coroutineScope = rememberCoroutineScope()

    storedGroup?.let {
        viewModel.existingVocabularyGroup = it
        rememberTitle.value = it.title
    }

    ModalBottomSheetLayout(
        sheetShape = RoundedCornerShape(topStart = 20.dp, topEnd = 20.dp),
        sheetState = modalBottomSheetState,
        sheetContent = {
            Box(modifier = Modifier.defaultMinSize(minHeight = 1.dp)) {
                when (modalBottomSheetType.value) {
                    GroupDetailBottomSheetType.DELETE_CONFIRMATION -> {
                        if (groupId == null) {
                            VocubiProDeleteNothingConfirmation(
                                canDelete = {
                                    coroutineScope.launch {
                                        modalBottomSheetState.hide()
                                    }
                                }
                            )
                        } else {
                            VocubiProDeleteConfirmation(
                                title = rememberTitle.value,
                                canDelete = {
                                    if (it) {
                                        viewModel.delete()
                                        navController.popBackStack()
                                    } else {
                                        coroutineScope.launch {
                                            modalBottomSheetState.hide()
                                        }
                                    }
                                }
                            )
                        }
                    }
                    else -> Unit
                }
            }
        },
        content = {
            Scaffold(modifier = Modifier.fillMaxSize(),
                topBar = {
                    VocubiTopAppBar(
                        title = {
                            Text(
                                text = stringResource(id = R.string.group_detail_action_bar_title),
                                color = Color.Black
                            )
                        },
                        onNavBackClick = { navController.popBackStack() }
                    )
                },
                bottomBar = {
                    GroupDetailBottomBar(
                        onNavigationClick = {
                            when (it.id) {
                                GroupDetailBottomBarIdDelete -> {
                                    coroutineScope.launch {
                                        modalBottomSheetType.value =
                                            GroupDetailBottomSheetType.DELETE_CONFIRMATION
                                        modalBottomSheetState.show()
                                    }
                                }
                                GroupDetailBottomBarIdSave -> {
                                    viewModel.submit(rememberTitle.value)
                                    navController.popBackStack()
                                }
                            }
                        }
                    )
                },
                content = {
                    GroupDetailTitleInputCard(
                        cardTitle = stringResource(id = R.string.group_detail_title_input_card_header),
                        title = rememberTitle.value,
                        onTitleChange = { rememberTitle.value = it }
                    )
                }
            )
        }
    )

}

@Composable
fun GroupDetailTitleInputCard(
    cardTitle: String,
    title: String,
    onTitleChange: (String) -> Unit
) {
    VocubiProHeaderCard(
        title = {
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 10.dp, horizontal = 16.dp),
                text = cardTitle,
                fontWeight = FontWeight.Bold
            )
        },
        content = {
            VocubiProTextInput(
                value = title,
                onValueChange = { onTitleChange(it) },
                label = { Text(stringResource(id = R.string.group_detail_title_input_header)) },
                imeAction = ImeAction.Done
            )
        }
    )
}

@Composable
private fun GroupDetailBottomBar(onNavigationClick: (NavigationBarEntry) -> Unit) {
    VocubiProBottomNavigationBar(
        entries = listOf(
            NavigationBarEntry(
                id = GroupDetailBottomBarIdDelete,
                title = R.string.common_text_delete,
                icon = R.drawable.ic_delete
            ),
            NavigationBarEntry(
                id = GroupDetailBottomBarIdSave,
                title = R.string.common_text_save,
                icon = R.drawable.ic_save
            )
        ),
        onClick = { onNavigationClick(it) }
    )
}
