/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.vocabulary.detail

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CardDefaults
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import dennis.hinsi.vocubipro.Constants
import dennis.hinsi.vocubipro.R
import dennis.hinsi.vocubipro.model.*
import dennis.hinsi.vocubipro.model.container.VocabularyEntryContainer
import dennis.hinsi.vocubipro.ui.commoncomposables.*
import dennis.hinsi.vocubipro.util.colorFromHexOrDefault
import dennis.hinsi.vocubipro.util.waitForStateResumed
import kotlinx.coroutines.launch
import java.util.*


@OptIn(ExperimentalMaterialApi::class)
@Composable
fun VocabularyDetailComposable(
    viewModel: VocabularyDetailViewModel = hiltViewModel(),
    navController: NavController,
    chapterId: String,
    vocabularyId: String?
) {
    val storedVocabulary by viewModel.loadVocabulary(vocabularyId).waitForStateResumed(null)

    val storedLanguages by viewModel.languages.waitForStateResumed(null)
    val allEntriesStored by viewModel.infinityEntryFlow.waitForStateResumed(null)

    val modalBottomSheetState = rememberModalBottomSheetState(ModalBottomSheetValue.Hidden)
    val modalBottomSheetType = remember { mutableStateOf(VocabularyDetailBottomSheetType.NONE) }

    val coroutineScope = rememberCoroutineScope()
    val remVocabulary = remember { mutableStateOf<Vocabulary?>(null) }
    val remEntries = remember { mutableStateOf<List<VocabularyEntryContainer>?>(null) }

    val remLanguage = remember { mutableStateOf<Language?>(null) }
    val remEntry = remember { mutableStateOf<VocabularyEntryContainer?>(null) }

    remVocabulary.value = storedVocabulary
    storedVocabulary?.let { voc ->
        remEntries.value = allEntriesStored?.filter { it.entryId == voc.entryId }
            ?.filter { entry -> storedLanguages?.find { it.id == entry.languageId } != null }
            ?.map { entry ->
                VocabularyEntryContainer(
                    entry,
                    storedLanguages?.find { it.id == entry.languageId }!!
                )
            }
    }

    ModalBottomSheetLayout(
        sheetShape = RoundedCornerShape(topStart = 20.dp, topEnd = 20.dp),
        sheetState = modalBottomSheetState,
        sheetContent = {
            Box(modifier = Modifier.defaultMinSize(minHeight = 1.dp)) {
                when (modalBottomSheetType.value) {
                    VocabularyDetailBottomSheetType.VOCABULARY_TEXT -> {
                        VocubiProSingleTextInputDialogComposable(
                            title = stringResource(id = R.string.vocabulary_bottom_sheet_change_vocabulary_title),
                            hint = stringResource(id = R.string.vocabulary_bottom_sheet_change_vocabulary_input_hint),
                            text = remVocabulary.value?.text ?: "",
                            onCancelClick = {
                                coroutineScope.launch {
                                    modalBottomSheetType.value =
                                        VocabularyDetailBottomSheetType.NONE
                                    modalBottomSheetState.hide()
                                }
                            },
                            onSubmitClick = {
                                if (vocabularyId.isNullOrEmpty()) {
                                    remVocabulary.value = Vocabulary(
                                        id = UUID.randomUUID().toString(),
                                        chapterId = chapterId,
                                        entryId = UUID.randomUUID().toString(),
                                        text = it
                                    )
                                } else {
                                    remVocabulary.value = remVocabulary.value?.copy(text = it)
                                }
                                coroutineScope.launch {
                                    modalBottomSheetType.value =
                                        VocabularyDetailBottomSheetType.NONE
                                    modalBottomSheetState.hide()
                                }
                            }
                        )
                    }
                    VocabularyDetailBottomSheetType.ENTRY -> {
                        VocubiProMultiTextInputDialogComposable(
                            title = stringResource(
                                id = R.string.vocabulary_bottom_sheet_change_vocabulary_entry_title,
                                remLanguage.value?.title ?: ""
                            ),
                            text = listOf(
                                remEntry.value?.entry?.text ?: "",
                                remEntry.value?.entry?.phoneticText ?: "",
                                remEntry.value?.entry?.transcriptionText ?: ""
                            ),
                            hint = listOf(
                                stringResource(id = R.string.vocabulary_bottom_sheet_change_vocabulary_entry_input_hint_0),
                                stringResource(id = R.string.vocabulary_bottom_sheet_change_vocabulary_entry_input_hint_1),
                                stringResource(id = R.string.vocabulary_bottom_sheet_change_vocabulary_entry_input_hint_2)
                            ),
                            onCancelClick = {
                                coroutineScope.launch {
                                    modalBottomSheetType.value =
                                        VocabularyDetailBottomSheetType.NONE
                                    modalBottomSheetState.hide()
                                }
                            },
                            onSubmitClick = {
                                viewModel.submitEntry(
                                    remVocabulary.value!!.entryId,
                                    remEntry.value,
                                    remLanguage.value,
                                    it[0],
                                    it[1],
                                    it[2]
                                )
                                coroutineScope.launch {
                                    modalBottomSheetType.value =
                                        VocabularyDetailBottomSheetType.NONE
                                    modalBottomSheetState.hide()
                                }
                            }
                        )
                    }
                    VocabularyDetailBottomSheetType.DELETE_ENTRY -> {
                        VocubiProDeleteConfirmation(
                            title = remEntry.value?.entry?.text ?: "",
                            canDelete = { confirmed ->
                                if (confirmed) {
                                    remEntry.value?.entry?.id?.let { viewModel.deleteEntry(it) }
                                }
                                coroutineScope.launch {
                                    modalBottomSheetType.value =
                                        VocabularyDetailBottomSheetType.NONE
                                    modalBottomSheetState.hide()
                                }
                            }
                        )
                    }
                    VocabularyDetailBottomSheetType.DELETE_VOCABULARY -> {
                        VocubiProDeleteConfirmation(
                            title = remVocabulary.value?.text ?: "",
                            canDelete = { confirmed ->
                                if (confirmed) {
                                    remVocabulary.value?.let {
                                        viewModel.deleteVocabulary(it.id, it.entryId)
                                    }
                                }
                                coroutineScope.launch {
                                    modalBottomSheetType.value =
                                        VocabularyDetailBottomSheetType.NONE
                                    modalBottomSheetState.hide()
                                }
                            }
                        )
                    }

                    else -> Unit
                }
            }
        },
        content = {
            Scaffold(modifier = Modifier.fillMaxSize(),
                topBar = {
                    VocubiTopAppBar(
                        title = {
                            Text(
                                text = stringResource(id = R.string.vocabulary_detail_action_bar_title),
                                color = Color.Black
                            )
                        },
                        onNavBackClick = { navController.popBackStack() }
                    )
                },
                bottomBar = {
                    VocabularyDetailBottomBar(
                        onNavigationClick = {
                            when (it.id) {
                                VocabularyDetailBottomBarIdDelete -> {
                                    coroutineScope.launch {
                                        modalBottomSheetType.value =
                                            VocabularyDetailBottomSheetType.DELETE_VOCABULARY
                                        modalBottomSheetState.show()
                                    }
                                }
                                VocabularyDetailBottomBarIdSave -> {
                                    viewModel.submit(
                                        !vocabularyId.isNullOrEmpty(),
                                        remVocabulary.value!!
                                    )
                                    navController.popBackStack()
                                }
                            }
                        }
                    )
                },
                content = {
                    LazyColumn {
                        item {
                            VocabularyDetailCard(
                                vocabulary = remVocabulary.value,
                                onClick = {
                                    coroutineScope.launch {
                                        modalBottomSheetType.value =
                                            VocabularyDetailBottomSheetType.VOCABULARY_TEXT
                                        modalBottomSheetState.show()
                                    }
                                }
                            )
                        }
                        item {
                            Text(
                                text = stringResource(id = R.string.vocabulary_detail_sub_header_translations),
                                modifier = Modifier
                                    .padding(horizontal = 16.dp)
                                    .padding(top = 10.dp),
                                fontSize = 18.sp,
                                fontWeight = FontWeight.Bold
                            )
                        }
                        remEntries.value?.forEach {
                            item {
                                VocabularyDetailEntryItemCard(it,
                                    onClick = {
                                        remEntry.value = it
                                        remLanguage.value = it.language
                                        coroutineScope.launch {
                                            modalBottomSheetType.value =
                                                VocabularyDetailBottomSheetType.ENTRY
                                            modalBottomSheetState.show()
                                        }
                                    },
                                    onDeleteClick = {
                                        remEntry.value = it
                                        remLanguage.value = it.language
                                        coroutineScope.launch {
                                            modalBottomSheetType.value =
                                                VocabularyDetailBottomSheetType.DELETE_ENTRY
                                            modalBottomSheetState.show()
                                        }
                                    }
                                )
                            }
                        }
                        val missingLanguages = storedLanguages
                            ?.filter { t -> remEntries.value?.find { it.language.id == t.id } == null }
                        if (!missingLanguages.isNullOrEmpty()) {
                            item {
                                Text(
                                    text = stringResource(id = R.string.vocabulary_detail_sub_header_missing_translations),
                                    modifier = Modifier
                                        .padding(horizontal = 16.dp)
                                        .padding(top = 10.dp),
                                    fontSize = 18.sp,
                                    fontWeight = FontWeight.Bold
                                )
                            }
                            missingLanguages.forEach {
                                item {
                                    VocabularyDetailLanguageCard(
                                        language = it,
                                        onClick = {
                                            remEntry.value = null
                                            remLanguage.value = it
                                            coroutineScope.launch {
                                                modalBottomSheetType.value =
                                                    VocabularyDetailBottomSheetType.ENTRY
                                                modalBottomSheetState.show()
                                            }
                                        }
                                    )
                                }
                            }
                        }

                        item {
                            Spacer(modifier = Modifier.height(80.dp))
                        }
                    }
                }
            )
        }
    )
}

@Composable
fun VocabularyDetailCard(
    vocabulary: Vocabulary?,
    onClick: (Vocabulary?) -> Unit
) {
    androidx.compose.material3.Card(
        shape = RoundedCornerShape(10.dp),
        elevation = CardDefaults.cardElevation(defaultElevation = 5.dp),
        colors = CardDefaults.cardColors(containerColor = Color.White),
        modifier = Modifier
            .padding(16.dp)
            .clickable { onClick(vocabulary) },
        content = {
            if (vocabulary == null) {
                VocabularyDetailPlaceholder(onClick = { onClick(vocabulary) }, height = 200.dp)
            } else {
                Box(modifier = Modifier
                    .fillMaxWidth()
                    .height(200.dp)
                    .clickable { onClick(vocabulary) }) {
                    Text(
                        text = vocabulary.text,
                        fontSize = 20.sp,
                        modifier = Modifier.align(Alignment.Center)
                    )
                }
            }
        }
    )
}

@Composable
private fun VocabularyDetailEntryItemCard(
    entryContainer: VocabularyEntryContainer,
    onClick: (VocabularyEntryContainer) -> Unit,
    onDeleteClick: (VocabularyEntryContainer) -> Unit
) {
    androidx.compose.material3.Card(
        shape = RoundedCornerShape(10.dp),
        elevation = CardDefaults.cardElevation(defaultElevation = 5.dp),
        colors = CardDefaults.cardColors(containerColor = Color.White),
        modifier = Modifier
            .padding(horizontal = 16.dp)
            .padding(vertical = 10.dp),
        content = {
            Column(
                modifier = Modifier
                    .background(
                        entryContainer.language.accentColor.colorFromHexOrDefault(
                            alpha = Constants.DEFAULT_DECENT_BACKGROUND_ACCENT_COLOR_ALPHA,
                            defaultHex = Constants.DEFAULT_UNSELECTED_COLOR
                        )
                    )
                    .clickable { onClick(entryContainer) },
            ) {
                Text(
                    text = entryContainer.entry.text,
                    modifier = Modifier
                        .padding(horizontal = 16.dp)
                        .padding(top = 10.dp),
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Bold
                )
                if (entryContainer.entry.phoneticText.isNotEmpty()) {
                    Text(
                        text = stringResource(
                            id = R.string.vocabulary_detail_vocabulary_item_pronunciation_text,
                            entryContainer.entry.phoneticText
                        ),
                        modifier = Modifier
                            .padding(horizontal = 16.dp)
                            .padding(top = 10.dp),
                        fontSize = 16.sp
                    )
                }
                if (entryContainer.entry.transcriptionText.isNotEmpty()) {
                    Text(
                        text = stringResource(
                            id = R.string.vocabulary_detail_vocabulary_item_transcription_text,
                            entryContainer.entry.transcriptionText
                        ),
                        modifier = Modifier
                            .padding(horizontal = 16.dp)
                            .padding(top = 10.dp),
                        fontSize = 16.sp
                    )
                }

                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 10.dp)
                ) {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .align(Alignment.CenterStart)
                    ) {
                        androidx.compose.material3.Button(
                            modifier = Modifier
                                .padding(horizontal = 10.dp)
                                .align(Alignment.CenterVertically),
                            onClick = { onDeleteClick(entryContainer) },
                            colors = ButtonDefaults.buttonColors(
                                containerColor = Color.White
                            ),
                            content = {
                                Image(
                                    modifier = Modifier.align(Alignment.CenterVertically),
                                    painter = painterResource(id = R.drawable.ic_delete),
                                    contentDescription = "",
                                    colorFilter = ColorFilter.tint(colorResource(id = R.color.delete_red))
                                )
                            }
                        )

                        VocabularyDetailLanguageCard(
                            modifier = Modifier.align(Alignment.CenterVertically),
                            language = entryContainer.language
                        )
                    }
                }
            }
        }
    )
}

@Composable
private fun VocabularyDetailLanguageCard(
    language: Language?,
    onClick: (Language?) -> Unit
) {
    androidx.compose.material3.Card(
        shape = RoundedCornerShape(10.dp),
        elevation = CardDefaults.cardElevation(defaultElevation = 5.dp),
        colors = CardDefaults.cardColors(containerColor = Color.White),
        modifier = Modifier
            .padding(horizontal = 16.dp)
            .padding(vertical = 10.dp)
            .clickable { onClick(language) },
        content = {
            Box(modifier = Modifier
                .fillMaxWidth()
                .background(
                    language?.accentColor?.colorFromHexOrDefault(
                        alpha = Constants.DEFAULT_ACCENT_COLOR_ALPHA,
                        defaultHex = Constants.DEFAULT_UNSELECTED_COLOR
                    ) ?: Color.White
                )
                .clickable { onClick(language) }
            ) {
                Text(
                    text = "${language?.title} - \"${language?.titleTranslated}\"",
                    fontSize = 20.sp,
                    modifier = Modifier
                        .align(Alignment.CenterStart)
                        .padding(10.dp)
                )
            }
        }
    )
}

@Composable
private fun VocabularyDetailPlaceholder(onClick: () -> Unit, height: Dp) {
    Box(modifier = Modifier
        .fillMaxWidth()
        .height(height)
        .clickable { onClick() }) {
        Image(
            painter = painterResource(id = R.drawable.ic_add),
            contentDescription = null,
            colorFilter = ColorFilter.tint(Color.Black),
            modifier = Modifier
                .size(50.dp)
                .clip(CircleShape)
                .align(Alignment.Center)
        )
    }
}


@Composable
private fun VocabularyDetailBottomBar(onNavigationClick: (NavigationBarEntry) -> Unit) {
    VocubiProBottomNavigationBar(
        entries = listOf(
            NavigationBarEntry(
                id = VocabularyDetailBottomBarIdDelete,
                title = R.string.common_text_delete,
                icon = R.drawable.ic_delete
            ),
            NavigationBarEntry(
                id = VocabularyDetailBottomBarIdSave,
                title = R.string.common_text_save,
                icon = R.drawable.ic_save
            )
        ),
        onClick = { onNavigationClick(it) }
    )
}

@Composable
private fun VocabularyDetailLanguageCard(modifier: Modifier = Modifier, language: Language) {
    androidx.compose.material3.Card(
        elevation = CardDefaults.cardElevation(defaultElevation = 5.dp),
        colors = CardDefaults.cardColors(containerColor = Color.White),
        modifier = modifier.padding(horizontal = 10.dp)
    ) {
        Row(
            modifier = modifier
                .height(50.dp)
                .background(
                    language.accentColor.colorFromHexOrDefault(
                        alpha = Constants.DEFAULT_ACCENT_COLOR_ALPHA,
                        defaultHex = Constants.DEFAULT_UNSELECTED_COLOR
                    )
                )
                .fillMaxWidth(),
        ) {
            androidx.compose.material3.Text(
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .padding(start = 16.dp),
                text = language.emoji,
                fontSize = 20.sp
            )
            androidx.compose.material3.Text(
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .padding(horizontal = 15.dp)
                    .fillMaxWidth(),
                text = "${language.title} - \"${language.titleTranslated}\""
            )
        }
    }
}

