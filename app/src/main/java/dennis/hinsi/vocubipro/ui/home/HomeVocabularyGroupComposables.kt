/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.home

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import dennis.hinsi.vocubipro.R
import dennis.hinsi.vocubipro.model.VocabularyChapter
import dennis.hinsi.vocubipro.ui.theme.AppStyle


@Composable
fun VocabularyGroupHomeCard(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    title: String,
    chapterCount: Int
) {
    Card(
        shape = RoundedCornerShape(20.dp),
        elevation = CardDefaults.cardElevation(defaultElevation = 5.dp),
        colors = CardDefaults.cardColors(containerColor = Color.White),
        modifier = Modifier
            .padding(top = 16.dp)
            .padding(horizontal = 16.dp)
    ) {
        Box(modifier = Modifier.clickable { onClick() }) {
            Column(modifier = modifier
                .padding(10.dp)
                .padding(bottom = 15.dp)) {
                Text(
                    modifier = Modifier
                        .padding(5.dp),
                    text = title
                )
                Divider(
                    modifier = Modifier.padding(vertical = 5.dp, horizontal = 5.dp),
                    color = colorResource(id = AppStyle.DIVIDER_COLOR)
                )
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(IntrinsicSize.Min)
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_chapter),
                        contentDescription = "",
                        modifier = Modifier.align(Alignment.CenterVertically)
                    )
                    Divider(
                        color = colorResource(id = AppStyle.DIVIDER_COLOR),
                        modifier = Modifier
                            .fillMaxHeight()
                            .padding(top = 5.dp)
                            .padding(horizontal = 5.dp)
                            .width(1.dp)
                    )
                    Column {
                        Text(
                            text = if (chapterCount == 1) {
                                stringResource(
                                    id = R.string.home_group_item_chapter_text_single,
                                    chapterCount
                                )
                            } else {
                                stringResource(
                                    id = R.string.home_group_item_chapter_text_multiple,
                                    chapterCount
                                )
                            }
                        )
                    }
                }
            }
        }
    }
}
