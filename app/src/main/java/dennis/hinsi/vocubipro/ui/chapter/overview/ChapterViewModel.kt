/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.chapter.overview

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dennis.hinsi.vocubipro.usecases.ChapterDataUC
import dennis.hinsi.vocubipro.usecases.DataDeletionUC
import dennis.hinsi.vocubipro.usecases.GroupDataUC
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ChapterViewModel @Inject constructor(
    private val groupDataUC: GroupDataUC,
    private val chapterDataUC: ChapterDataUC,
    private val deletionUC: DataDeletionUC
) : ViewModel() {

    fun loadGroup(groupId: String) = flow {
        emit(groupDataUC.loadGroup(groupId))
    }

    fun loadChapters(groupId: String) = flow {
        emit(chapterDataUC.loadChaptersOfGroup(groupId))
    }

    fun deleteGroup(groupId: String) = viewModelScope.launch {
        deletionUC.deleteGroup(groupId)
    }

    fun deleteChapter(chapterId: String) = viewModelScope.launch {
        deletionUC.deleteChapter(chapterId)
    }

}