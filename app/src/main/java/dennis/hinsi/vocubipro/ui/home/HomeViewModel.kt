/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dennis.hinsi.vocubipro.model.Language
import dennis.hinsi.vocubipro.model.VocabularyGroup
import dennis.hinsi.vocubipro.repository.LanguageRepository
import dennis.hinsi.vocubipro.repository.VocabularyGroupRepository
import dennis.hinsi.vocubipro.usecases.GroupDataUC
import dennis.hinsi.vocubipro.usecases.LanguageDataUC
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val languageDataUC: LanguageDataUC,
    private val groupDataUC: GroupDataUC
): ViewModel() {
    val languageList: Flow<List<Language>> get() = languageDataUC.loadAll()
    val groupList: Flow<List<VocabularyGroup>> get() = groupDataUC.loadAllGroups()
}