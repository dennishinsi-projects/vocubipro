/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.group

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dennis.hinsi.vocubipro.model.VocabularyGroup
import dennis.hinsi.vocubipro.util.onNull
import dennis.hinsi.vocubipro.usecases.GroupDataUC
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class GroupDetailViewModel @Inject constructor(
    private val groupDataUC: GroupDataUC
): ViewModel() {

    fun loadGroup(groupId: String?) = if (!groupId.isNullOrEmpty()) {
        flow { emit(groupDataUC.loadGroup(groupId)) }
    } else {
        emptyFlow()
    }


    var existingVocabularyGroup: VocabularyGroup? = null

    fun submit(title: String) = viewModelScope.launch {
        existingVocabularyGroup?.let { existingData ->
            groupDataUC.update(
                existingData.copy(
                    id = existingData.id,
                    title = title,
                    createdAt = existingData.createdAt,
                    modifiedAt = System.currentTimeMillis()
                )
            )
        }.onNull {
            val time = System.currentTimeMillis()
            groupDataUC.insert(
                VocabularyGroup(
                    id = UUID.randomUUID().toString(),
                    title = title,
                    createdAt = time,
                    modifiedAt = time
                )
            )
        }
    }

    fun delete() = viewModelScope.launch {
        existingVocabularyGroup?.id?.let { groupDataUC.delete(it) }
    }

}