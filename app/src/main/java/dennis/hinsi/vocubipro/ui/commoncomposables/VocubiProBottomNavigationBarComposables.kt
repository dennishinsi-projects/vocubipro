/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro.ui.commoncomposables

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import dennis.hinsi.vocubipro.R
import dennis.hinsi.vocubipro.ui.theme.AppStyle

data class NavigationBarEntry(val id: Int, @StringRes val title: Int, @DrawableRes val icon: Int)

@Composable
fun VocubiProBottomNavigationBar(entries: List<NavigationBarEntry>, onClick: (NavigationBarEntry) -> Unit) {
    Surface(shadowElevation = AppStyle.BOTTOM_NAVIGATION_ELEVATION) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .background(colorResource(id = AppStyle.NAVIGATION_BAR_COLOR_REF))
        ) {
            entries.forEach {
                Column(modifier = Modifier
                    .weight(1f)
                    .clickable { onClick(it) }) {
                    Image(
                        modifier = Modifier
                            .align(Alignment.CenterHorizontally)
                            .padding(top = 10.dp),
                        painter = painterResource(id = it.icon),
                        contentDescription = stringResource(id = it.title)
                    )
                    Text(
                        modifier = Modifier
                            .align(Alignment.CenterHorizontally)
                            .padding(bottom = 10.dp),
                        text = stringResource(id = it.title)
                    )
                }
            }
        }
    }
}

@Composable
@Preview(showBackground = true)
fun BottomNavigationBarPreview() {
    VocubiProBottomNavigationBar(
        entries = listOf(
            NavigationBarEntry(
                id = 0,
                title = R.string.app_name,
                icon = R.drawable.ic_language
            ),
            NavigationBarEntry(
                id = 0,
                title = R.string.app_name,
                icon = R.drawable.ic_language
            )
        ),
        onClick = {

        }
    )
}
