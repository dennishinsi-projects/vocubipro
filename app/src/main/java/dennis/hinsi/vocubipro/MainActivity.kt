/*
 * Copyright 2022 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package dennis.hinsi.vocubipro

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.CompositionLocalProvider
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.google.android.gms.oss.licenses.OssLicensesMenuActivity
import dagger.hilt.android.AndroidEntryPoint
import dennis.hinsi.vocubipro.dev.helper.DevOptions
import dennis.hinsi.vocubipro.dev.helper.DevOptionsRepo
import dennis.hinsi.vocubipro.dev.helper.PreferenceReader
import dennis.hinsi.vocubipro.ui.chapter.overview.ChapterMainComposable
import dennis.hinsi.vocubipro.ui.chapter.detail.ChapterDetailComposable
import dennis.hinsi.vocubipro.ui.group.GroupMainComposable
import dennis.hinsi.vocubipro.ui.home.HomeView
import dennis.hinsi.vocubipro.ui.languages.detail.LanguageDetailView
import dennis.hinsi.vocubipro.ui.languages.LanguageOverviewView
import dennis.hinsi.vocubipro.ui.settings.SettingsMainComposable
import dennis.hinsi.vocubipro.ui.theme.VocubiProTheme
import dennis.hinsi.vocubipro.ui.vocabulary.detail.VocabularyDetailComposable
import dennis.hinsi.vocubipro.ui.vocabulary.overview.VocabularyMainComposable
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    @Inject
    lateinit var preferenceReader: PreferenceReader

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val defOptionsRepo = DevOptionsRepo(preferenceReader)
        setContent {
            VocubiProTheme {
                CompositionLocalProvider(
                    values = arrayOf(DevOptions.provides(defOptionsRepo))
                ) {
                    val navController = rememberNavController()
                    NavHost(
                        navController = navController,
                        startDestination = "home",
                        builder = { createNavGraph(navController) }
                    )
                }
            }
        }
    }

    private fun NavGraphBuilder.createNavGraph(navController: NavController) {
        composable(
            route = "home",
            content = { HomeView(navController) }
        )
        composable(
            route = "language_overview",
            content = { LanguageOverviewView(navController) },
        )
        composable(
            route = "language_detail?id={id}",
            arguments = listOf(navArgument("id") { nullable = true }),
            content = { backStackEntry ->
                LanguageDetailView(
                    navController,
                    backStackEntry.arguments?.getString("id")
                )
            }
        )
        composable(
            "group_detail?id={id}",
            arguments = listOf(navArgument("id") { nullable = true }),
            content = { backStackEntry ->
                GroupMainComposable(
                    navController = navController,
                    groupId = backStackEntry.arguments?.getString("id"),
                )
            }
        )
        composable(
            "chapter?group_id={group_id}",
            arguments = listOf(
                navArgument("group_id") { nullable = false },
            ),
            content = { backStackEntry ->
                ChapterMainComposable(
                    navController = navController,
                    groupId = backStackEntry.arguments?.getString("group_id")!!
                )
            }
        )
        composable(
            "chapter_detail?group_id={group_id}&chapter_id={chapter_id}&chapter_position={chapter_position}",
            arguments = listOf(
                navArgument("group_id") { nullable = false },
                navArgument("chapter_id") { nullable = true },
                navArgument("chapter_position") { nullable = true },
            ),
            content = { backStackEntry ->
                ChapterDetailComposable(
                    navController = navController,
                    groupId = backStackEntry.arguments?.getString("group_id")!!,
                    chapterId = backStackEntry.arguments?.getString("chapter_id"),
                    chapterPosition = backStackEntry.arguments?.getInt("chapter_position")
                )
            }
        )
        composable(
            "vocabulary?chapter_id={chapter_id}",
            arguments = listOf(
                navArgument("chapter_id") { nullable = false },
            ),
            content = { backStackEntry ->
                VocabularyMainComposable(
                    navController = navController,
                    chapterId = backStackEntry.arguments?.getString("chapter_id")!!
                )
            }
        )
        composable(
            "vocabulary_detail?chapter_id={chapter_id}&vocabulary_id={vocabulary_id}",
            arguments = listOf(
                navArgument("chapter_id") { nullable = false },
                navArgument("vocabulary_id") { nullable = true },
            ),
            content = { backStackEntry ->
                VocabularyDetailComposable(
                    navController = navController,
                    chapterId = backStackEntry.arguments?.getString("chapter_id")!!,
                    vocabularyId = backStackEntry.arguments?.getString("vocabulary_id")
                )
            }
        )
        composable(
            route = "settings",
            content = {
                SettingsMainComposable(
                    navController = navController,
                    onLicencesClick = {
                        startActivity(
                            Intent(
                                this@MainActivity,
                                OssLicensesMenuActivity::class.java
                            )
                        )
                    }
                )
            }
        )
    }

}
