package dagger.hilt.internal.processedrootsentinel.codegen;

import dagger.hilt.internal.processedrootsentinel.ProcessedRootSentinel;

@ProcessedRootSentinel(
    roots = "dennis.hinsi.vocubipro.VocubiProApplication"
)
public final class _dennis_hinsi_vocubipro_VocubiProApplication {
}
