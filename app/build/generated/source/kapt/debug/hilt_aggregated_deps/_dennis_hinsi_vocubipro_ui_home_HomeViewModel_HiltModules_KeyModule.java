package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code! This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ActivityRetainedComponent",
    modules = "dennis.hinsi.vocubipro.ui.home.HomeViewModel_HiltModules.KeyModule"
)
public class _dennis_hinsi_vocubipro_ui_home_HomeViewModel_HiltModules_KeyModule {
}
