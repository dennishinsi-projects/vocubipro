package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code! This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ActivityComponent",
    entryPoints = "dennis.hinsi.vocubipro.MainActivity_GeneratedInjector"
)
public class _dennis_hinsi_vocubipro_MainActivity_GeneratedInjector {
}
