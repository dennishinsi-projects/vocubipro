package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code! This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ViewModelComponent",
    modules = "dennis.hinsi.vocubipro.ui.vocabulary.overview.VocabularyViewModel_HiltModules.BindsModule"
)
public class _dennis_hinsi_vocubipro_ui_vocabulary_overview_VocabularyViewModel_HiltModules_BindsModule {
}
