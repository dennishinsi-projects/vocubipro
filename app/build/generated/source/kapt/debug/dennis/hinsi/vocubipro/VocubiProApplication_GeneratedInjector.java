package dennis.hinsi.vocubipro;

import dagger.hilt.InstallIn;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.components.SingletonComponent;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = VocubiProApplication.class
)
@GeneratedEntryPoint
@InstallIn(SingletonComponent.class)
public interface VocubiProApplication_GeneratedInjector {
  void injectVocubiProApplication(VocubiProApplication vocubiProApplication);
}
