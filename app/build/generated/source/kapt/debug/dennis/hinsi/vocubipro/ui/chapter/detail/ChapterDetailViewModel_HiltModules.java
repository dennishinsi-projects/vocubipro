package dennis.hinsi.vocubipro.ui.chapter.detail;

import androidx.lifecycle.ViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityRetainedComponent;
import dagger.hilt.android.components.ViewModelComponent;
import dagger.hilt.android.internal.lifecycle.HiltViewModelMap;
import dagger.hilt.codegen.OriginatingElement;
import dagger.multibindings.IntoMap;
import dagger.multibindings.IntoSet;
import dagger.multibindings.StringKey;
import java.lang.String;

@OriginatingElement(
    topLevelClass = ChapterDetailViewModel.class
)
public final class ChapterDetailViewModel_HiltModules {
  private ChapterDetailViewModel_HiltModules() {
  }

  @Module
  @InstallIn(ViewModelComponent.class)
  public abstract static class BindsModule {
    private BindsModule() {
    }

    @Binds
    @IntoMap
    @StringKey("dennis.hinsi.vocubipro.ui.chapter.detail.ChapterDetailViewModel")
    @HiltViewModelMap
    public abstract ViewModel binds(ChapterDetailViewModel vm);
  }

  @Module
  @InstallIn(ActivityRetainedComponent.class)
  public static final class KeyModule {
    private KeyModule() {
    }

    @Provides
    @IntoSet
    @HiltViewModelMap.KeySet
    public static String provide() {
      return "dennis.hinsi.vocubipro.ui.chapter.detail.ChapterDetailViewModel";
    }
  }
}
