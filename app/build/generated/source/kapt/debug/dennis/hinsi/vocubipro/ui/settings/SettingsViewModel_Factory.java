// Generated by Dagger (https://dagger.dev).
package dennis.hinsi.vocubipro.ui.settings;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import dennis.hinsi.vocubipro.usecases.ChapterDataUC;
import dennis.hinsi.vocubipro.usecases.GroupDataUC;
import dennis.hinsi.vocubipro.usecases.LanguageDataUC;
import dennis.hinsi.vocubipro.usecases.VocabularyDataUC;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class SettingsViewModel_Factory implements Factory<SettingsViewModel> {
  private final Provider<LanguageDataUC> languageDataUCProvider;

  private final Provider<GroupDataUC> groupDataUCProvider;

  private final Provider<ChapterDataUC> chapterDataUCProvider;

  private final Provider<VocabularyDataUC> vocabularyDataUCProvider;

  public SettingsViewModel_Factory(Provider<LanguageDataUC> languageDataUCProvider,
      Provider<GroupDataUC> groupDataUCProvider, Provider<ChapterDataUC> chapterDataUCProvider,
      Provider<VocabularyDataUC> vocabularyDataUCProvider) {
    this.languageDataUCProvider = languageDataUCProvider;
    this.groupDataUCProvider = groupDataUCProvider;
    this.chapterDataUCProvider = chapterDataUCProvider;
    this.vocabularyDataUCProvider = vocabularyDataUCProvider;
  }

  @Override
  public SettingsViewModel get() {
    return newInstance(languageDataUCProvider.get(), groupDataUCProvider.get(), chapterDataUCProvider.get(), vocabularyDataUCProvider.get());
  }

  public static SettingsViewModel_Factory create(Provider<LanguageDataUC> languageDataUCProvider,
      Provider<GroupDataUC> groupDataUCProvider, Provider<ChapterDataUC> chapterDataUCProvider,
      Provider<VocabularyDataUC> vocabularyDataUCProvider) {
    return new SettingsViewModel_Factory(languageDataUCProvider, groupDataUCProvider, chapterDataUCProvider, vocabularyDataUCProvider);
  }

  public static SettingsViewModel newInstance(LanguageDataUC languageDataUC,
      GroupDataUC groupDataUC, ChapterDataUC chapterDataUC, VocabularyDataUC vocabularyDataUC) {
    return new SettingsViewModel(languageDataUC, groupDataUC, chapterDataUC, vocabularyDataUC);
  }
}
