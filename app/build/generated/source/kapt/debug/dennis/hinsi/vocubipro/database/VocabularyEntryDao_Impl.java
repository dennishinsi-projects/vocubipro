package dennis.hinsi.vocubipro.database;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.room.CoroutinesRoom;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import dennis.hinsi.vocubipro.model.VocabularyEntry;
import java.lang.Class;
import java.lang.Exception;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.flow.Flow;

@SuppressWarnings({"unchecked", "deprecation"})
public final class VocabularyEntryDao_Impl implements VocabularyEntryDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<VocabularyEntry> __insertionAdapterOfVocabularyEntry;

  private final EntityDeletionOrUpdateAdapter<VocabularyEntry> __updateAdapterOfVocabularyEntry;

  private final SharedSQLiteStatement __preparedStmtOfDelete;

  private final SharedSQLiteStatement __preparedStmtOfDeleteAllWithEntryId;

  public VocabularyEntryDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfVocabularyEntry = new EntityInsertionAdapter<VocabularyEntry>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `table_vocabulary_vocabulary_entry` (`id`,`entry_id`,`languageId`,`phoneticText`,`transcriptionText`,`text`) VALUES (?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, VocabularyEntry value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getId());
        }
        if (value.getEntryId() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getEntryId());
        }
        if (value.getLanguageId() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getLanguageId());
        }
        if (value.getPhoneticText() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getPhoneticText());
        }
        if (value.getTranscriptionText() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getTranscriptionText());
        }
        if (value.getText() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getText());
        }
      }
    };
    this.__updateAdapterOfVocabularyEntry = new EntityDeletionOrUpdateAdapter<VocabularyEntry>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `table_vocabulary_vocabulary_entry` SET `id` = ?,`entry_id` = ?,`languageId` = ?,`phoneticText` = ?,`transcriptionText` = ?,`text` = ? WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, VocabularyEntry value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getId());
        }
        if (value.getEntryId() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getEntryId());
        }
        if (value.getLanguageId() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getLanguageId());
        }
        if (value.getPhoneticText() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getPhoneticText());
        }
        if (value.getTranscriptionText() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getTranscriptionText());
        }
        if (value.getText() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getText());
        }
        if (value.getId() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.getId());
        }
      }
    };
    this.__preparedStmtOfDelete = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM table_vocabulary_vocabulary_entry WHERE id = ?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteAllWithEntryId = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM table_vocabulary_vocabulary_entry WHERE entry_id = ?";
        return _query;
      }
    };
  }

  @Override
  public Object insert(final VocabularyEntry data, final Continuation<? super Unit> continuation) {
    return CoroutinesRoom.execute(__db, true, new Callable<Unit>() {
      @Override
      public Unit call() throws Exception {
        __db.beginTransaction();
        try {
          __insertionAdapterOfVocabularyEntry.insert(data);
          __db.setTransactionSuccessful();
          return Unit.INSTANCE;
        } finally {
          __db.endTransaction();
        }
      }
    }, continuation);
  }

  @Override
  public Object update(final VocabularyEntry data, final Continuation<? super Unit> continuation) {
    return CoroutinesRoom.execute(__db, true, new Callable<Unit>() {
      @Override
      public Unit call() throws Exception {
        __db.beginTransaction();
        try {
          __updateAdapterOfVocabularyEntry.handle(data);
          __db.setTransactionSuccessful();
          return Unit.INSTANCE;
        } finally {
          __db.endTransaction();
        }
      }
    }, continuation);
  }

  @Override
  public Object delete(final String id, final Continuation<? super Unit> continuation) {
    return CoroutinesRoom.execute(__db, true, new Callable<Unit>() {
      @Override
      public Unit call() throws Exception {
        final SupportSQLiteStatement _stmt = __preparedStmtOfDelete.acquire();
        int _argIndex = 1;
        if (id == null) {
          _stmt.bindNull(_argIndex);
        } else {
          _stmt.bindString(_argIndex, id);
        }
        __db.beginTransaction();
        try {
          _stmt.executeUpdateDelete();
          __db.setTransactionSuccessful();
          return Unit.INSTANCE;
        } finally {
          __db.endTransaction();
          __preparedStmtOfDelete.release(_stmt);
        }
      }
    }, continuation);
  }

  @Override
  public Object deleteAllWithEntryId(final String entryId,
      final Continuation<? super Unit> continuation) {
    return CoroutinesRoom.execute(__db, true, new Callable<Unit>() {
      @Override
      public Unit call() throws Exception {
        final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteAllWithEntryId.acquire();
        int _argIndex = 1;
        if (entryId == null) {
          _stmt.bindNull(_argIndex);
        } else {
          _stmt.bindString(_argIndex, entryId);
        }
        __db.beginTransaction();
        try {
          _stmt.executeUpdateDelete();
          __db.setTransactionSuccessful();
          return Unit.INSTANCE;
        } finally {
          __db.endTransaction();
          __preparedStmtOfDeleteAllWithEntryId.release(_stmt);
        }
      }
    }, continuation);
  }

  @Override
  public Object load(final String id, final Continuation<? super VocabularyEntry> continuation) {
    final String _sql = "SELECT * FROM table_vocabulary_vocabulary_entry WHERE id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (id == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, id);
    }
    final CancellationSignal _cancellationSignal = DBUtil.createCancellationSignal();
    return CoroutinesRoom.execute(__db, false, _cancellationSignal, new Callable<VocabularyEntry>() {
      @Override
      public VocabularyEntry call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
        try {
          final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
          final int _cursorIndexOfEntryId = CursorUtil.getColumnIndexOrThrow(_cursor, "entry_id");
          final int _cursorIndexOfLanguageId = CursorUtil.getColumnIndexOrThrow(_cursor, "languageId");
          final int _cursorIndexOfPhoneticText = CursorUtil.getColumnIndexOrThrow(_cursor, "phoneticText");
          final int _cursorIndexOfTranscriptionText = CursorUtil.getColumnIndexOrThrow(_cursor, "transcriptionText");
          final int _cursorIndexOfText = CursorUtil.getColumnIndexOrThrow(_cursor, "text");
          final VocabularyEntry _result;
          if(_cursor.moveToFirst()) {
            final String _tmpId;
            if (_cursor.isNull(_cursorIndexOfId)) {
              _tmpId = null;
            } else {
              _tmpId = _cursor.getString(_cursorIndexOfId);
            }
            final String _tmpEntryId;
            if (_cursor.isNull(_cursorIndexOfEntryId)) {
              _tmpEntryId = null;
            } else {
              _tmpEntryId = _cursor.getString(_cursorIndexOfEntryId);
            }
            final String _tmpLanguageId;
            if (_cursor.isNull(_cursorIndexOfLanguageId)) {
              _tmpLanguageId = null;
            } else {
              _tmpLanguageId = _cursor.getString(_cursorIndexOfLanguageId);
            }
            final String _tmpPhoneticText;
            if (_cursor.isNull(_cursorIndexOfPhoneticText)) {
              _tmpPhoneticText = null;
            } else {
              _tmpPhoneticText = _cursor.getString(_cursorIndexOfPhoneticText);
            }
            final String _tmpTranscriptionText;
            if (_cursor.isNull(_cursorIndexOfTranscriptionText)) {
              _tmpTranscriptionText = null;
            } else {
              _tmpTranscriptionText = _cursor.getString(_cursorIndexOfTranscriptionText);
            }
            final String _tmpText;
            if (_cursor.isNull(_cursorIndexOfText)) {
              _tmpText = null;
            } else {
              _tmpText = _cursor.getString(_cursorIndexOfText);
            }
            _result = new VocabularyEntry(_tmpId,_tmpEntryId,_tmpLanguageId,_tmpPhoneticText,_tmpTranscriptionText,_tmpText);
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
          _statement.release();
        }
      }
    }, continuation);
  }

  @Override
  public Object loadAll(final String id,
      final Continuation<? super List<VocabularyEntry>> continuation) {
    final String _sql = "SELECT * FROM table_vocabulary_vocabulary_entry WHERE id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (id == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, id);
    }
    final CancellationSignal _cancellationSignal = DBUtil.createCancellationSignal();
    return CoroutinesRoom.execute(__db, false, _cancellationSignal, new Callable<List<VocabularyEntry>>() {
      @Override
      public List<VocabularyEntry> call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
        try {
          final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
          final int _cursorIndexOfEntryId = CursorUtil.getColumnIndexOrThrow(_cursor, "entry_id");
          final int _cursorIndexOfLanguageId = CursorUtil.getColumnIndexOrThrow(_cursor, "languageId");
          final int _cursorIndexOfPhoneticText = CursorUtil.getColumnIndexOrThrow(_cursor, "phoneticText");
          final int _cursorIndexOfTranscriptionText = CursorUtil.getColumnIndexOrThrow(_cursor, "transcriptionText");
          final int _cursorIndexOfText = CursorUtil.getColumnIndexOrThrow(_cursor, "text");
          final List<VocabularyEntry> _result = new ArrayList<VocabularyEntry>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final VocabularyEntry _item;
            final String _tmpId;
            if (_cursor.isNull(_cursorIndexOfId)) {
              _tmpId = null;
            } else {
              _tmpId = _cursor.getString(_cursorIndexOfId);
            }
            final String _tmpEntryId;
            if (_cursor.isNull(_cursorIndexOfEntryId)) {
              _tmpEntryId = null;
            } else {
              _tmpEntryId = _cursor.getString(_cursorIndexOfEntryId);
            }
            final String _tmpLanguageId;
            if (_cursor.isNull(_cursorIndexOfLanguageId)) {
              _tmpLanguageId = null;
            } else {
              _tmpLanguageId = _cursor.getString(_cursorIndexOfLanguageId);
            }
            final String _tmpPhoneticText;
            if (_cursor.isNull(_cursorIndexOfPhoneticText)) {
              _tmpPhoneticText = null;
            } else {
              _tmpPhoneticText = _cursor.getString(_cursorIndexOfPhoneticText);
            }
            final String _tmpTranscriptionText;
            if (_cursor.isNull(_cursorIndexOfTranscriptionText)) {
              _tmpTranscriptionText = null;
            } else {
              _tmpTranscriptionText = _cursor.getString(_cursorIndexOfTranscriptionText);
            }
            final String _tmpText;
            if (_cursor.isNull(_cursorIndexOfText)) {
              _tmpText = null;
            } else {
              _tmpText = _cursor.getString(_cursorIndexOfText);
            }
            _item = new VocabularyEntry(_tmpId,_tmpEntryId,_tmpLanguageId,_tmpPhoneticText,_tmpTranscriptionText,_tmpText);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
          _statement.release();
        }
      }
    }, continuation);
  }

  @Override
  public Flow<List<VocabularyEntry>> loadAll() {
    final String _sql = "SELECT * FROM table_vocabulary_vocabulary_entry";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return CoroutinesRoom.createFlow(__db, false, new String[]{"table_vocabulary_vocabulary_entry"}, new Callable<List<VocabularyEntry>>() {
      @Override
      public List<VocabularyEntry> call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
        try {
          final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
          final int _cursorIndexOfEntryId = CursorUtil.getColumnIndexOrThrow(_cursor, "entry_id");
          final int _cursorIndexOfLanguageId = CursorUtil.getColumnIndexOrThrow(_cursor, "languageId");
          final int _cursorIndexOfPhoneticText = CursorUtil.getColumnIndexOrThrow(_cursor, "phoneticText");
          final int _cursorIndexOfTranscriptionText = CursorUtil.getColumnIndexOrThrow(_cursor, "transcriptionText");
          final int _cursorIndexOfText = CursorUtil.getColumnIndexOrThrow(_cursor, "text");
          final List<VocabularyEntry> _result = new ArrayList<VocabularyEntry>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final VocabularyEntry _item;
            final String _tmpId;
            if (_cursor.isNull(_cursorIndexOfId)) {
              _tmpId = null;
            } else {
              _tmpId = _cursor.getString(_cursorIndexOfId);
            }
            final String _tmpEntryId;
            if (_cursor.isNull(_cursorIndexOfEntryId)) {
              _tmpEntryId = null;
            } else {
              _tmpEntryId = _cursor.getString(_cursorIndexOfEntryId);
            }
            final String _tmpLanguageId;
            if (_cursor.isNull(_cursorIndexOfLanguageId)) {
              _tmpLanguageId = null;
            } else {
              _tmpLanguageId = _cursor.getString(_cursorIndexOfLanguageId);
            }
            final String _tmpPhoneticText;
            if (_cursor.isNull(_cursorIndexOfPhoneticText)) {
              _tmpPhoneticText = null;
            } else {
              _tmpPhoneticText = _cursor.getString(_cursorIndexOfPhoneticText);
            }
            final String _tmpTranscriptionText;
            if (_cursor.isNull(_cursorIndexOfTranscriptionText)) {
              _tmpTranscriptionText = null;
            } else {
              _tmpTranscriptionText = _cursor.getString(_cursorIndexOfTranscriptionText);
            }
            final String _tmpText;
            if (_cursor.isNull(_cursorIndexOfText)) {
              _tmpText = null;
            } else {
              _tmpText = _cursor.getString(_cursorIndexOfText);
            }
            _item = new VocabularyEntry(_tmpId,_tmpEntryId,_tmpLanguageId,_tmpPhoneticText,_tmpTranscriptionText,_tmpText);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public Object loadWithEntryId(final String entryId,
      final Continuation<? super VocabularyEntry> continuation) {
    final String _sql = "SELECT * FROM table_vocabulary_vocabulary_entry WHERE entry_id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (entryId == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, entryId);
    }
    final CancellationSignal _cancellationSignal = DBUtil.createCancellationSignal();
    return CoroutinesRoom.execute(__db, false, _cancellationSignal, new Callable<VocabularyEntry>() {
      @Override
      public VocabularyEntry call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
        try {
          final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
          final int _cursorIndexOfEntryId = CursorUtil.getColumnIndexOrThrow(_cursor, "entry_id");
          final int _cursorIndexOfLanguageId = CursorUtil.getColumnIndexOrThrow(_cursor, "languageId");
          final int _cursorIndexOfPhoneticText = CursorUtil.getColumnIndexOrThrow(_cursor, "phoneticText");
          final int _cursorIndexOfTranscriptionText = CursorUtil.getColumnIndexOrThrow(_cursor, "transcriptionText");
          final int _cursorIndexOfText = CursorUtil.getColumnIndexOrThrow(_cursor, "text");
          final VocabularyEntry _result;
          if(_cursor.moveToFirst()) {
            final String _tmpId;
            if (_cursor.isNull(_cursorIndexOfId)) {
              _tmpId = null;
            } else {
              _tmpId = _cursor.getString(_cursorIndexOfId);
            }
            final String _tmpEntryId;
            if (_cursor.isNull(_cursorIndexOfEntryId)) {
              _tmpEntryId = null;
            } else {
              _tmpEntryId = _cursor.getString(_cursorIndexOfEntryId);
            }
            final String _tmpLanguageId;
            if (_cursor.isNull(_cursorIndexOfLanguageId)) {
              _tmpLanguageId = null;
            } else {
              _tmpLanguageId = _cursor.getString(_cursorIndexOfLanguageId);
            }
            final String _tmpPhoneticText;
            if (_cursor.isNull(_cursorIndexOfPhoneticText)) {
              _tmpPhoneticText = null;
            } else {
              _tmpPhoneticText = _cursor.getString(_cursorIndexOfPhoneticText);
            }
            final String _tmpTranscriptionText;
            if (_cursor.isNull(_cursorIndexOfTranscriptionText)) {
              _tmpTranscriptionText = null;
            } else {
              _tmpTranscriptionText = _cursor.getString(_cursorIndexOfTranscriptionText);
            }
            final String _tmpText;
            if (_cursor.isNull(_cursorIndexOfText)) {
              _tmpText = null;
            } else {
              _tmpText = _cursor.getString(_cursorIndexOfText);
            }
            _result = new VocabularyEntry(_tmpId,_tmpEntryId,_tmpLanguageId,_tmpPhoneticText,_tmpTranscriptionText,_tmpText);
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
          _statement.release();
        }
      }
    }, continuation);
  }

  @Override
  public Object loadAllWithEntryId(final String entryId,
      final Continuation<? super List<VocabularyEntry>> continuation) {
    final String _sql = "SELECT * FROM table_vocabulary_vocabulary_entry WHERE entry_id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (entryId == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, entryId);
    }
    final CancellationSignal _cancellationSignal = DBUtil.createCancellationSignal();
    return CoroutinesRoom.execute(__db, false, _cancellationSignal, new Callable<List<VocabularyEntry>>() {
      @Override
      public List<VocabularyEntry> call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
        try {
          final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
          final int _cursorIndexOfEntryId = CursorUtil.getColumnIndexOrThrow(_cursor, "entry_id");
          final int _cursorIndexOfLanguageId = CursorUtil.getColumnIndexOrThrow(_cursor, "languageId");
          final int _cursorIndexOfPhoneticText = CursorUtil.getColumnIndexOrThrow(_cursor, "phoneticText");
          final int _cursorIndexOfTranscriptionText = CursorUtil.getColumnIndexOrThrow(_cursor, "transcriptionText");
          final int _cursorIndexOfText = CursorUtil.getColumnIndexOrThrow(_cursor, "text");
          final List<VocabularyEntry> _result = new ArrayList<VocabularyEntry>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final VocabularyEntry _item;
            final String _tmpId;
            if (_cursor.isNull(_cursorIndexOfId)) {
              _tmpId = null;
            } else {
              _tmpId = _cursor.getString(_cursorIndexOfId);
            }
            final String _tmpEntryId;
            if (_cursor.isNull(_cursorIndexOfEntryId)) {
              _tmpEntryId = null;
            } else {
              _tmpEntryId = _cursor.getString(_cursorIndexOfEntryId);
            }
            final String _tmpLanguageId;
            if (_cursor.isNull(_cursorIndexOfLanguageId)) {
              _tmpLanguageId = null;
            } else {
              _tmpLanguageId = _cursor.getString(_cursorIndexOfLanguageId);
            }
            final String _tmpPhoneticText;
            if (_cursor.isNull(_cursorIndexOfPhoneticText)) {
              _tmpPhoneticText = null;
            } else {
              _tmpPhoneticText = _cursor.getString(_cursorIndexOfPhoneticText);
            }
            final String _tmpTranscriptionText;
            if (_cursor.isNull(_cursorIndexOfTranscriptionText)) {
              _tmpTranscriptionText = null;
            } else {
              _tmpTranscriptionText = _cursor.getString(_cursorIndexOfTranscriptionText);
            }
            final String _tmpText;
            if (_cursor.isNull(_cursorIndexOfText)) {
              _tmpText = null;
            } else {
              _tmpText = _cursor.getString(_cursorIndexOfText);
            }
            _item = new VocabularyEntry(_tmpId,_tmpEntryId,_tmpLanguageId,_tmpPhoneticText,_tmpTranscriptionText,_tmpText);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
          _statement.release();
        }
      }
    }, continuation);
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
