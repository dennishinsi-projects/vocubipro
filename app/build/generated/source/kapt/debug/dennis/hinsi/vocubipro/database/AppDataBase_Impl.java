package dennis.hinsi.vocubipro.database;

import androidx.annotation.NonNull;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.RoomOpenHelper.ValidationResult;
import androidx.room.migration.AutoMigrationSpec;
import androidx.room.migration.Migration;
import androidx.room.util.DBUtil;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings({"unchecked", "deprecation"})
public final class AppDataBase_Impl extends AppDataBase {
  private volatile VocabularyGroupDao _vocabularyGroupDao;

  private volatile VocabularyChapterDao _vocabularyChapterDao;

  private volatile VocabularyDao _vocabularyDao;

  private volatile VocabularyEntryDao _vocabularyEntryDao;

  private volatile LanguageDao _languageDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `table_vocabulary_group` (`id` TEXT NOT NULL, `title` TEXT NOT NULL, `created_at` INTEGER NOT NULL, `modified_at` INTEGER NOT NULL, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `table_vocabulary_chapter` (`id` TEXT NOT NULL, `group_id` TEXT NOT NULL, `title` TEXT NOT NULL, `position` INTEGER NOT NULL, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `table_vocabulary` (`id` TEXT NOT NULL, `chapter_id` TEXT NOT NULL, `entry_id` TEXT NOT NULL, `text` TEXT NOT NULL, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `table_vocabulary_vocabulary_entry` (`id` TEXT NOT NULL, `entry_id` TEXT NOT NULL, `languageId` TEXT NOT NULL, `phoneticText` TEXT NOT NULL, `transcriptionText` TEXT NOT NULL, `text` TEXT NOT NULL, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `table_language` (`id` TEXT NOT NULL, `title` TEXT NOT NULL, `title_translated` TEXT NOT NULL, `emoji` TEXT NOT NULL, `accent_color` TEXT NOT NULL, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '96e1736afbb35b177e6e0ef2fd3c8f8a')");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `table_vocabulary_group`");
        _db.execSQL("DROP TABLE IF EXISTS `table_vocabulary_chapter`");
        _db.execSQL("DROP TABLE IF EXISTS `table_vocabulary`");
        _db.execSQL("DROP TABLE IF EXISTS `table_vocabulary_vocabulary_entry`");
        _db.execSQL("DROP TABLE IF EXISTS `table_language`");
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onDestructiveMigration(_db);
          }
        }
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      public void onPreMigrate(SupportSQLiteDatabase _db) {
        DBUtil.dropFtsSyncTriggers(_db);
      }

      @Override
      public void onPostMigrate(SupportSQLiteDatabase _db) {
      }

      @Override
      protected RoomOpenHelper.ValidationResult onValidateSchema(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsTableVocabularyGroup = new HashMap<String, TableInfo.Column>(4);
        _columnsTableVocabularyGroup.put("id", new TableInfo.Column("id", "TEXT", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTableVocabularyGroup.put("title", new TableInfo.Column("title", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTableVocabularyGroup.put("created_at", new TableInfo.Column("created_at", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTableVocabularyGroup.put("modified_at", new TableInfo.Column("modified_at", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysTableVocabularyGroup = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesTableVocabularyGroup = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoTableVocabularyGroup = new TableInfo("table_vocabulary_group", _columnsTableVocabularyGroup, _foreignKeysTableVocabularyGroup, _indicesTableVocabularyGroup);
        final TableInfo _existingTableVocabularyGroup = TableInfo.read(_db, "table_vocabulary_group");
        if (! _infoTableVocabularyGroup.equals(_existingTableVocabularyGroup)) {
          return new RoomOpenHelper.ValidationResult(false, "table_vocabulary_group(dennis.hinsi.vocubipro.model.VocabularyGroup).\n"
                  + " Expected:\n" + _infoTableVocabularyGroup + "\n"
                  + " Found:\n" + _existingTableVocabularyGroup);
        }
        final HashMap<String, TableInfo.Column> _columnsTableVocabularyChapter = new HashMap<String, TableInfo.Column>(4);
        _columnsTableVocabularyChapter.put("id", new TableInfo.Column("id", "TEXT", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTableVocabularyChapter.put("group_id", new TableInfo.Column("group_id", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTableVocabularyChapter.put("title", new TableInfo.Column("title", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTableVocabularyChapter.put("position", new TableInfo.Column("position", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysTableVocabularyChapter = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesTableVocabularyChapter = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoTableVocabularyChapter = new TableInfo("table_vocabulary_chapter", _columnsTableVocabularyChapter, _foreignKeysTableVocabularyChapter, _indicesTableVocabularyChapter);
        final TableInfo _existingTableVocabularyChapter = TableInfo.read(_db, "table_vocabulary_chapter");
        if (! _infoTableVocabularyChapter.equals(_existingTableVocabularyChapter)) {
          return new RoomOpenHelper.ValidationResult(false, "table_vocabulary_chapter(dennis.hinsi.vocubipro.model.VocabularyChapter).\n"
                  + " Expected:\n" + _infoTableVocabularyChapter + "\n"
                  + " Found:\n" + _existingTableVocabularyChapter);
        }
        final HashMap<String, TableInfo.Column> _columnsTableVocabulary = new HashMap<String, TableInfo.Column>(4);
        _columnsTableVocabulary.put("id", new TableInfo.Column("id", "TEXT", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTableVocabulary.put("chapter_id", new TableInfo.Column("chapter_id", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTableVocabulary.put("entry_id", new TableInfo.Column("entry_id", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTableVocabulary.put("text", new TableInfo.Column("text", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysTableVocabulary = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesTableVocabulary = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoTableVocabulary = new TableInfo("table_vocabulary", _columnsTableVocabulary, _foreignKeysTableVocabulary, _indicesTableVocabulary);
        final TableInfo _existingTableVocabulary = TableInfo.read(_db, "table_vocabulary");
        if (! _infoTableVocabulary.equals(_existingTableVocabulary)) {
          return new RoomOpenHelper.ValidationResult(false, "table_vocabulary(dennis.hinsi.vocubipro.model.Vocabulary).\n"
                  + " Expected:\n" + _infoTableVocabulary + "\n"
                  + " Found:\n" + _existingTableVocabulary);
        }
        final HashMap<String, TableInfo.Column> _columnsTableVocabularyVocabularyEntry = new HashMap<String, TableInfo.Column>(6);
        _columnsTableVocabularyVocabularyEntry.put("id", new TableInfo.Column("id", "TEXT", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTableVocabularyVocabularyEntry.put("entry_id", new TableInfo.Column("entry_id", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTableVocabularyVocabularyEntry.put("languageId", new TableInfo.Column("languageId", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTableVocabularyVocabularyEntry.put("phoneticText", new TableInfo.Column("phoneticText", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTableVocabularyVocabularyEntry.put("transcriptionText", new TableInfo.Column("transcriptionText", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTableVocabularyVocabularyEntry.put("text", new TableInfo.Column("text", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysTableVocabularyVocabularyEntry = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesTableVocabularyVocabularyEntry = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoTableVocabularyVocabularyEntry = new TableInfo("table_vocabulary_vocabulary_entry", _columnsTableVocabularyVocabularyEntry, _foreignKeysTableVocabularyVocabularyEntry, _indicesTableVocabularyVocabularyEntry);
        final TableInfo _existingTableVocabularyVocabularyEntry = TableInfo.read(_db, "table_vocabulary_vocabulary_entry");
        if (! _infoTableVocabularyVocabularyEntry.equals(_existingTableVocabularyVocabularyEntry)) {
          return new RoomOpenHelper.ValidationResult(false, "table_vocabulary_vocabulary_entry(dennis.hinsi.vocubipro.model.VocabularyEntry).\n"
                  + " Expected:\n" + _infoTableVocabularyVocabularyEntry + "\n"
                  + " Found:\n" + _existingTableVocabularyVocabularyEntry);
        }
        final HashMap<String, TableInfo.Column> _columnsTableLanguage = new HashMap<String, TableInfo.Column>(5);
        _columnsTableLanguage.put("id", new TableInfo.Column("id", "TEXT", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTableLanguage.put("title", new TableInfo.Column("title", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTableLanguage.put("title_translated", new TableInfo.Column("title_translated", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTableLanguage.put("emoji", new TableInfo.Column("emoji", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTableLanguage.put("accent_color", new TableInfo.Column("accent_color", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysTableLanguage = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesTableLanguage = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoTableLanguage = new TableInfo("table_language", _columnsTableLanguage, _foreignKeysTableLanguage, _indicesTableLanguage);
        final TableInfo _existingTableLanguage = TableInfo.read(_db, "table_language");
        if (! _infoTableLanguage.equals(_existingTableLanguage)) {
          return new RoomOpenHelper.ValidationResult(false, "table_language(dennis.hinsi.vocubipro.model.Language).\n"
                  + " Expected:\n" + _infoTableLanguage + "\n"
                  + " Found:\n" + _existingTableLanguage);
        }
        return new RoomOpenHelper.ValidationResult(true, null);
      }
    }, "96e1736afbb35b177e6e0ef2fd3c8f8a", "83cab3c9f4913bf0691264cc2ed8f2af");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    final HashMap<String, String> _shadowTablesMap = new HashMap<String, String>(0);
    HashMap<String, Set<String>> _viewTables = new HashMap<String, Set<String>>(0);
    return new InvalidationTracker(this, _shadowTablesMap, _viewTables, "table_vocabulary_group","table_vocabulary_chapter","table_vocabulary","table_vocabulary_vocabulary_entry","table_language");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `table_vocabulary_group`");
      _db.execSQL("DELETE FROM `table_vocabulary_chapter`");
      _db.execSQL("DELETE FROM `table_vocabulary`");
      _db.execSQL("DELETE FROM `table_vocabulary_vocabulary_entry`");
      _db.execSQL("DELETE FROM `table_language`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  protected Map<Class<?>, List<Class<?>>> getRequiredTypeConverters() {
    final HashMap<Class<?>, List<Class<?>>> _typeConvertersMap = new HashMap<Class<?>, List<Class<?>>>();
    _typeConvertersMap.put(VocabularyGroupDao.class, VocabularyGroupDao_Impl.getRequiredConverters());
    _typeConvertersMap.put(VocabularyChapterDao.class, VocabularyChapterDao_Impl.getRequiredConverters());
    _typeConvertersMap.put(VocabularyDao.class, VocabularyDao_Impl.getRequiredConverters());
    _typeConvertersMap.put(VocabularyEntryDao.class, VocabularyEntryDao_Impl.getRequiredConverters());
    _typeConvertersMap.put(LanguageDao.class, LanguageDao_Impl.getRequiredConverters());
    return _typeConvertersMap;
  }

  @Override
  public Set<Class<? extends AutoMigrationSpec>> getRequiredAutoMigrationSpecs() {
    final HashSet<Class<? extends AutoMigrationSpec>> _autoMigrationSpecsSet = new HashSet<Class<? extends AutoMigrationSpec>>();
    return _autoMigrationSpecsSet;
  }

  @Override
  public List<Migration> getAutoMigrations(
      @NonNull Map<Class<? extends AutoMigrationSpec>, AutoMigrationSpec> autoMigrationSpecsMap) {
    return Arrays.asList();
  }

  @Override
  public VocabularyGroupDao provideVocabularyGroupDao() {
    if (_vocabularyGroupDao != null) {
      return _vocabularyGroupDao;
    } else {
      synchronized(this) {
        if(_vocabularyGroupDao == null) {
          _vocabularyGroupDao = new VocabularyGroupDao_Impl(this);
        }
        return _vocabularyGroupDao;
      }
    }
  }

  @Override
  public VocabularyChapterDao provideVocabularyChapterDao() {
    if (_vocabularyChapterDao != null) {
      return _vocabularyChapterDao;
    } else {
      synchronized(this) {
        if(_vocabularyChapterDao == null) {
          _vocabularyChapterDao = new VocabularyChapterDao_Impl(this);
        }
        return _vocabularyChapterDao;
      }
    }
  }

  @Override
  public VocabularyDao provideVocabularyDao() {
    if (_vocabularyDao != null) {
      return _vocabularyDao;
    } else {
      synchronized(this) {
        if(_vocabularyDao == null) {
          _vocabularyDao = new VocabularyDao_Impl(this);
        }
        return _vocabularyDao;
      }
    }
  }

  @Override
  public VocabularyEntryDao provideVocabularyEntryDao() {
    if (_vocabularyEntryDao != null) {
      return _vocabularyEntryDao;
    } else {
      synchronized(this) {
        if(_vocabularyEntryDao == null) {
          _vocabularyEntryDao = new VocabularyEntryDao_Impl(this);
        }
        return _vocabularyEntryDao;
      }
    }
  }

  @Override
  public LanguageDao provideLanguageDao() {
    if (_languageDao != null) {
      return _languageDao;
    } else {
      synchronized(this) {
        if(_languageDao == null) {
          _languageDao = new LanguageDao_Impl(this);
        }
        return _languageDao;
      }
    }
  }
}
