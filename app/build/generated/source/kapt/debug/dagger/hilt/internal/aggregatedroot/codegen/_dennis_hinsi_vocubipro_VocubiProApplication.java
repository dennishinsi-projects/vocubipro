package dagger.hilt.internal.aggregatedroot.codegen;

import dagger.hilt.android.HiltAndroidApp;
import dagger.hilt.internal.aggregatedroot.AggregatedRoot;

/**
 * This class should only be referenced by generated code! This class aggregates information across multiple compilations.
 */
@AggregatedRoot(
    root = "dennis.hinsi.vocubipro.VocubiProApplication",
    rootPackage = "dennis.hinsi.vocubipro",
    originatingRoot = "dennis.hinsi.vocubipro.VocubiProApplication",
    originatingRootPackage = "dennis.hinsi.vocubipro",
    rootAnnotation = HiltAndroidApp.class,
    rootSimpleNames = "VocubiProApplication",
    originatingRootSimpleNames = "VocubiProApplication"
)
public class _dennis_hinsi_vocubipro_VocubiProApplication {
}
