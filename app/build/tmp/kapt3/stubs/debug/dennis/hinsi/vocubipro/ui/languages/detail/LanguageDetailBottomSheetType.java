package dennis.hinsi.vocubipro.ui.languages.detail;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006\u00a8\u0006\u0007"}, d2 = {"Ldennis/hinsi/vocubipro/ui/languages/detail/LanguageDetailBottomSheetType;", "", "(Ljava/lang/String;I)V", "NONE", "FLAG_SELECTION", "COLOR_SELECTION", "CONFIRM_DELETION", "app_debug"})
public enum LanguageDetailBottomSheetType {
    /*public static final*/ NONE /* = new NONE() */,
    /*public static final*/ FLAG_SELECTION /* = new FLAG_SELECTION() */,
    /*public static final*/ COLOR_SELECTION /* = new COLOR_SELECTION() */,
    /*public static final*/ CONFIRM_DELETION /* = new CONFIRM_DELETION() */;
    
    LanguageDetailBottomSheetType() {
    }
}