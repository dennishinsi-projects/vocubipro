package dennis.hinsi.vocubipro.ui.chapter.detail;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004\u00a8\u0006\u0005"}, d2 = {"Ldennis/hinsi/vocubipro/ui/chapter/detail/ChapterDetailBottomSheetType;", "", "(Ljava/lang/String;I)V", "NONE", "DELETE_CONFIRMATION", "app_debug"})
public enum ChapterDetailBottomSheetType {
    /*public static final*/ NONE /* = new NONE() */,
    /*public static final*/ DELETE_CONFIRMATION /* = new DELETE_CONFIRMATION() */;
    
    ChapterDetailBottomSheetType() {
    }
}