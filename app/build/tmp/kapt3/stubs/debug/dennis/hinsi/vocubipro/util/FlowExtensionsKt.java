package dennis.hinsi.vocubipro.util;

import androidx.compose.runtime.Composable;
import androidx.compose.runtime.State;
import androidx.lifecycle.Lifecycle;
import kotlinx.coroutines.flow.Flow;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u001a3\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u0002H\u0002H\u0007\u00a2\u0006\u0002\u0010\u0007\u001a+\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0006\u0010\u0006\u001a\u0002H\u0002H\u0007\u00a2\u0006\u0002\u0010\t\u00a8\u0006\n"}, d2 = {"waitForState", "Landroidx/compose/runtime/State;", "T", "Lkotlinx/coroutines/flow/Flow;", "state", "Landroidx/lifecycle/Lifecycle$State;", "initialValue", "(Lkotlinx/coroutines/flow/Flow;Landroidx/lifecycle/Lifecycle$State;Ljava/lang/Object;)Landroidx/compose/runtime/State;", "waitForStateResumed", "(Lkotlinx/coroutines/flow/Flow;Ljava/lang/Object;)Landroidx/compose/runtime/State;", "app_debug"})
public final class FlowExtensionsKt {
    
    @org.jetbrains.annotations.NotNull()
    @androidx.compose.runtime.Composable()
    public static final <T extends java.lang.Object>androidx.compose.runtime.State<T> waitForState(@org.jetbrains.annotations.NotNull()
    kotlinx.coroutines.flow.Flow<? extends T> $this$waitForState, @org.jetbrains.annotations.NotNull()
    androidx.lifecycle.Lifecycle.State state, T initialValue) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @androidx.compose.runtime.Composable()
    public static final <T extends java.lang.Object>androidx.compose.runtime.State<T> waitForStateResumed(@org.jetbrains.annotations.NotNull()
    kotlinx.coroutines.flow.Flow<? extends T> $this$waitForStateResumed, T initialValue) {
        return null;
    }
}