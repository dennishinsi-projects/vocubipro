package dennis.hinsi.vocubipro.dev.helper;

import androidx.compose.runtime.Composable;
import androidx.compose.runtime.ProvidedValue;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0019\u0010\t\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\n2\u0006\u0010\u000b\u001a\u00020\u0005H\u0086\u0004R\u0016\u0010\u0003\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u00058G\u00a2\u0006\u0006\u001a\u0004\b\u0007\u0010\b\u00a8\u0006\f"}, d2 = {"Ldennis/hinsi/vocubipro/dev/helper/DevOptions;", "", "()V", "localPreferenceReader", "Landroidx/compose/runtime/ProvidableCompositionLocal;", "Ldennis/hinsi/vocubipro/dev/helper/DevOptionsRepo;", "optionsRepo", "getOptionsRepo", "()Ldennis/hinsi/vocubipro/dev/helper/DevOptionsRepo;", "provides", "Landroidx/compose/runtime/ProvidedValue;", "devOptionsRepo", "app_debug"})
public final class DevOptions {
    @org.jetbrains.annotations.NotNull()
    public static final dennis.hinsi.vocubipro.dev.helper.DevOptions INSTANCE = null;
    private static final androidx.compose.runtime.ProvidableCompositionLocal<dennis.hinsi.vocubipro.dev.helper.DevOptionsRepo> localPreferenceReader = null;
    
    private DevOptions() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.compose.runtime.Composable()
    public final dennis.hinsi.vocubipro.dev.helper.DevOptionsRepo getOptionsRepo() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.compose.runtime.ProvidedValue<dennis.hinsi.vocubipro.dev.helper.DevOptionsRepo> provides(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.dev.helper.DevOptionsRepo devOptionsRepo) {
        return null;
    }
}