package dennis.hinsi.vocubipro.ui.settings;

import androidx.lifecycle.ViewModel;
import dagger.hilt.android.lifecycle.HiltViewModel;
import dennis.hinsi.vocubipro.dev.mock.MockDataSet;
import dennis.hinsi.vocubipro.repository.*;
import dennis.hinsi.vocubipro.usecases.ChapterDataUC;
import dennis.hinsi.vocubipro.usecases.GroupDataUC;
import dennis.hinsi.vocubipro.usecases.LanguageDataUC;
import dennis.hinsi.vocubipro.usecases.VocabularyDataUC;
import javax.inject.Inject;

@dagger.hilt.android.lifecycle.HiltViewModel()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\'\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0006\u0010\u000b\u001a\u00020\fR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Ldennis/hinsi/vocubipro/ui/settings/SettingsViewModel;", "Landroidx/lifecycle/ViewModel;", "languageDataUC", "Ldennis/hinsi/vocubipro/usecases/LanguageDataUC;", "groupDataUC", "Ldennis/hinsi/vocubipro/usecases/GroupDataUC;", "chapterDataUC", "Ldennis/hinsi/vocubipro/usecases/ChapterDataUC;", "vocabularyDataUC", "Ldennis/hinsi/vocubipro/usecases/VocabularyDataUC;", "(Ldennis/hinsi/vocubipro/usecases/LanguageDataUC;Ldennis/hinsi/vocubipro/usecases/GroupDataUC;Ldennis/hinsi/vocubipro/usecases/ChapterDataUC;Ldennis/hinsi/vocubipro/usecases/VocabularyDataUC;)V", "saveMockedData", "Lkotlinx/coroutines/Job;", "app_debug"})
public final class SettingsViewModel extends androidx.lifecycle.ViewModel {
    private final dennis.hinsi.vocubipro.usecases.LanguageDataUC languageDataUC = null;
    private final dennis.hinsi.vocubipro.usecases.GroupDataUC groupDataUC = null;
    private final dennis.hinsi.vocubipro.usecases.ChapterDataUC chapterDataUC = null;
    private final dennis.hinsi.vocubipro.usecases.VocabularyDataUC vocabularyDataUC = null;
    
    @javax.inject.Inject()
    public SettingsViewModel(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.usecases.LanguageDataUC languageDataUC, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.usecases.GroupDataUC groupDataUC, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.usecases.ChapterDataUC chapterDataUC, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.usecases.VocabularyDataUC vocabularyDataUC) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job saveMockedData() {
        return null;
    }
}