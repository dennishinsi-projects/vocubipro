package dennis.hinsi.vocubipro.ui.home;

import android.annotation.SuppressLint;
import androidx.annotation.StringRes;
import androidx.compose.foundation.layout.*;
import androidx.compose.material3.*;
import androidx.compose.runtime.Composable;
import androidx.compose.ui.Alignment;
import androidx.compose.ui.Modifier;
import androidx.navigation.NavController;
import dennis.hinsi.vocubipro.R;
import dennis.hinsi.vocubipro.ui.commoncomposables.NavigationBarEntry;
import dennis.hinsi.vocubipro.ui.theme.AppStyle;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000>\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001aH\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u0010\b\u001a\u00020\u00012\u001c\u0010\t\u001a\u0018\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00050\n\u00a2\u0006\u0002\b\f\u00a2\u0006\u0002\b\r2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00050\u000fH\u0007\u001a\u001a\u0010\u0010\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\u00122\b\b\u0002\u0010\u0013\u001a\u00020\u0014H\u0007\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"BOTTOM_NAV_ID_ADD", "", "BOTTOM_NAV_ID_LANGUAGE", "BOTTOM_NAV_ID_SETTINGS", "HomeCardBase", "", "modifier", "Landroidx/compose/ui/Modifier;", "title", "content", "Lkotlin/Function1;", "Landroidx/compose/foundation/layout/ColumnScope;", "Landroidx/compose/runtime/Composable;", "Lkotlin/ExtensionFunctionType;", "onClick", "Lkotlin/Function0;", "HomeView", "navController", "Landroidx/navigation/NavController;", "homeViewModel", "Ldennis/hinsi/vocubipro/ui/home/HomeViewModel;", "app_debug"})
public final class HomeComposablesKt {
    private static final int BOTTOM_NAV_ID_SETTINGS = 0;
    private static final int BOTTOM_NAV_ID_LANGUAGE = 1;
    private static final int BOTTOM_NAV_ID_ADD = 2;
    
    @androidx.compose.runtime.Composable()
    @kotlin.OptIn(markerClass = {androidx.compose.material3.ExperimentalMaterial3Api.class})
    @android.annotation.SuppressLint(value = {"UnusedMaterial3ScaffoldPaddingParameter"})
    public static final void HomeView(@org.jetbrains.annotations.NotNull()
    androidx.navigation.NavController navController, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.ui.home.HomeViewModel homeViewModel) {
    }
    
    @androidx.compose.runtime.Composable()
    public static final void HomeCardBase(@org.jetbrains.annotations.NotNull()
    androidx.compose.ui.Modifier modifier, @androidx.annotation.StringRes()
    int title, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super androidx.compose.foundation.layout.ColumnScope, kotlin.Unit> content, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> onClick) {
    }
}