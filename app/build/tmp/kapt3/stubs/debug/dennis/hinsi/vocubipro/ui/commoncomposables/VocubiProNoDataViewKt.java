package dennis.hinsi.vocubipro.ui.commoncomposables;

import androidx.compose.runtime.Composable;
import androidx.compose.ui.Alignment;
import androidx.compose.ui.Modifier;
import dennis.hinsi.vocubipro.R;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u001a\u001a\u0010\u0000\u001a\u00020\u00012\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0007\u00a8\u0006\u0006"}, d2 = {"VocubiProNoDataView", "", "modifier", "Landroidx/compose/ui/Modifier;", "title", "", "app_debug"})
public final class VocubiProNoDataViewKt {
    
    @androidx.compose.runtime.Composable()
    public static final void VocubiProNoDataView(@org.jetbrains.annotations.NotNull()
    androidx.compose.ui.Modifier modifier, @org.jetbrains.annotations.NotNull()
    java.lang.String title) {
    }
}