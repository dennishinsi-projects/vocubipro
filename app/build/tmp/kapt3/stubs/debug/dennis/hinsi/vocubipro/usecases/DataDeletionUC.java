package dennis.hinsi.vocubipro.usecases;

import dennis.hinsi.vocubipro.repository.*;
import javax.inject.Inject;

/**
 * UseCase to clean data the right way without forgetting parts. Since we have many different
 * database tables connected by different ids and relations we always have to make sure to delete
 * everything connected to the target we want to delete.
 */
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\u0018\u00002\u00020\u0001B/\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\fJ\u0019\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0011J\u0019\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u0010H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0011J\u0019\u0010\u0014\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u0010H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0011J\u0019\u0010\u0016\u001a\u00020\u000e2\u0006\u0010\u0017\u001a\u00020\u0010H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0011J!\u0010\u0018\u001a\u00020\u000e2\u0006\u0010\u0019\u001a\u00020\u00102\u0006\u0010\u001a\u001a\u00020\u0010H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001bR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u001c"}, d2 = {"Ldennis/hinsi/vocubipro/usecases/DataDeletionUC;", "", "languageRepository", "Ldennis/hinsi/vocubipro/repository/LanguageRepository;", "groupRepository", "Ldennis/hinsi/vocubipro/repository/VocabularyGroupRepository;", "chapterRepository", "Ldennis/hinsi/vocubipro/repository/VocabularyChapterRepository;", "vocabularyRepository", "Ldennis/hinsi/vocubipro/repository/VocabularyRepository;", "vocabularyEntryRepository", "Ldennis/hinsi/vocubipro/repository/VocabularyEntryRepository;", "(Ldennis/hinsi/vocubipro/repository/LanguageRepository;Ldennis/hinsi/vocubipro/repository/VocabularyGroupRepository;Ldennis/hinsi/vocubipro/repository/VocabularyChapterRepository;Ldennis/hinsi/vocubipro/repository/VocabularyRepository;Ldennis/hinsi/vocubipro/repository/VocabularyEntryRepository;)V", "deleteChapter", "", "chapterId", "", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "deleteEntry", "id", "deleteGroup", "groupId", "deleteLanguage", "languageId", "deleteVocabulary", "vocabularyId", "entryId", "(Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class DataDeletionUC {
    private final dennis.hinsi.vocubipro.repository.LanguageRepository languageRepository = null;
    private final dennis.hinsi.vocubipro.repository.VocabularyGroupRepository groupRepository = null;
    private final dennis.hinsi.vocubipro.repository.VocabularyChapterRepository chapterRepository = null;
    private final dennis.hinsi.vocubipro.repository.VocabularyRepository vocabularyRepository = null;
    private final dennis.hinsi.vocubipro.repository.VocabularyEntryRepository vocabularyEntryRepository = null;
    
    @javax.inject.Inject()
    public DataDeletionUC(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.repository.LanguageRepository languageRepository, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.repository.VocabularyGroupRepository groupRepository, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.repository.VocabularyChapterRepository chapterRepository, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.repository.VocabularyRepository vocabularyRepository, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.repository.VocabularyEntryRepository vocabularyEntryRepository) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object deleteLanguage(@org.jetbrains.annotations.NotNull()
    java.lang.String languageId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object deleteGroup(@org.jetbrains.annotations.NotNull()
    java.lang.String groupId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object deleteChapter(@org.jetbrains.annotations.NotNull()
    java.lang.String chapterId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object deleteVocabulary(@org.jetbrains.annotations.NotNull()
    java.lang.String vocabularyId, @org.jetbrains.annotations.NotNull()
    java.lang.String entryId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object deleteEntry(@org.jetbrains.annotations.NotNull()
    java.lang.String id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
}