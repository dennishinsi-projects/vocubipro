package dennis.hinsi.vocubipro.ui.vocabulary.detail;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007\u00a8\u0006\b"}, d2 = {"Ldennis/hinsi/vocubipro/ui/vocabulary/detail/VocabularyDetailBottomSheetType;", "", "(Ljava/lang/String;I)V", "NONE", "ENTRY", "VOCABULARY_TEXT", "DELETE_ENTRY", "DELETE_VOCABULARY", "app_debug"})
public enum VocabularyDetailBottomSheetType {
    /*public static final*/ NONE /* = new NONE() */,
    /*public static final*/ ENTRY /* = new ENTRY() */,
    /*public static final*/ VOCABULARY_TEXT /* = new VOCABULARY_TEXT() */,
    /*public static final*/ DELETE_ENTRY /* = new DELETE_ENTRY() */,
    /*public static final*/ DELETE_VOCABULARY /* = new DELETE_VOCABULARY() */;
    
    VocabularyDetailBottomSheetType() {
    }
}