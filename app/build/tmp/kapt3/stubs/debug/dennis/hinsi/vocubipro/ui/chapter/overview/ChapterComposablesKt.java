package dennis.hinsi.vocubipro.ui.chapter.overview;

import androidx.compose.foundation.layout.*;
import androidx.compose.material.*;
import androidx.compose.material3.ButtonDefaults;
import androidx.compose.material3.CardDefaults;
import androidx.compose.runtime.*;
import androidx.compose.ui.Alignment;
import androidx.compose.ui.Modifier;
import androidx.compose.ui.graphics.ColorFilter;
import androidx.navigation.NavController;
import dennis.hinsi.vocubipro.R;
import dennis.hinsi.vocubipro.dev.helper.DevOptions;
import dennis.hinsi.vocubipro.util.ListIndexInt;
import dennis.hinsi.vocubipro.model.VocabularyChapter;
import dennis.hinsi.vocubipro.ui.commoncomposables.*;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000L\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a\u001c\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\u0003H\u0003\u001a\\\u0010\u0005\u001a\u00020\u00012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00010\u00032\u001c\u0010\n\u001a\u0018\u0012\b\u0012\u00060\fj\u0002`\r\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00010\u000b2\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00010\u0003H\u0003\u001aB\u0010\u000f\u001a\u00020\u00012\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\f2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00010\u00132\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00010\u00132\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00010\u0013H\u0003\u001a\"\u0010\u0014\u001a\u00020\u00012\b\b\u0002\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0011H\u0007\u001a\u0018\u0010\u001a\u001a\u00020\u00012\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0017\u001a\u00020\u0018H\u0003\u00a8\u0006\u001b"}, d2 = {"ChapterBottomBar", "", "onNavigationClick", "Lkotlin/Function1;", "Ldennis/hinsi/vocubipro/ui/commoncomposables/NavigationBarEntry;", "ChapterContentFilled", "list", "", "Ldennis/hinsi/vocubipro/model/VocabularyChapter;", "onClick", "onEditClick", "Lkotlin/Function2;", "", "Ldennis/hinsi/vocubipro/util/ListIndexInt;", "onDeleteClick", "ChapterItem", "title", "", "count", "Lkotlin/Function0;", "ChapterMainComposable", "viewModel", "Ldennis/hinsi/vocubipro/ui/chapter/overview/ChapterViewModel;", "navController", "Landroidx/navigation/NavController;", "groupId", "ChapterTopBar", "app_debug"})
public final class ChapterComposablesKt {
    
    @androidx.compose.runtime.Composable()
    @kotlin.OptIn(markerClass = {androidx.compose.material.ExperimentalMaterialApi.class})
    public static final void ChapterMainComposable(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.ui.chapter.overview.ChapterViewModel viewModel, @org.jetbrains.annotations.NotNull()
    androidx.navigation.NavController navController, @org.jetbrains.annotations.NotNull()
    java.lang.String groupId) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void ChapterContentFilled(java.util.List<dennis.hinsi.vocubipro.model.VocabularyChapter> list, kotlin.jvm.functions.Function1<? super dennis.hinsi.vocubipro.model.VocabularyChapter, kotlin.Unit> onClick, kotlin.jvm.functions.Function2<? super java.lang.Integer, ? super dennis.hinsi.vocubipro.model.VocabularyChapter, kotlin.Unit> onEditClick, kotlin.jvm.functions.Function1<? super dennis.hinsi.vocubipro.model.VocabularyChapter, kotlin.Unit> onDeleteClick) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void ChapterTopBar(java.lang.String title, androidx.navigation.NavController navController) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void ChapterBottomBar(kotlin.jvm.functions.Function1<? super dennis.hinsi.vocubipro.ui.commoncomposables.NavigationBarEntry, kotlin.Unit> onNavigationClick) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void ChapterItem(java.lang.String title, int count, kotlin.jvm.functions.Function0<kotlin.Unit> onClick, kotlin.jvm.functions.Function0<kotlin.Unit> onEditClick, kotlin.jvm.functions.Function0<kotlin.Unit> onDeleteClick) {
    }
}