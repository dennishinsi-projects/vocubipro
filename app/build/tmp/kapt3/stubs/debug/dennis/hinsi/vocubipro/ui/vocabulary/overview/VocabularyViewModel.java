package dennis.hinsi.vocubipro.ui.vocabulary.overview;

import androidx.lifecycle.ViewModel;
import dagger.hilt.android.lifecycle.HiltViewModel;
import dennis.hinsi.vocubipro.usecases.VocabularyDataUC;
import javax.inject.Inject;

@dagger.hilt.android.lifecycle.HiltViewModel()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001a\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u00062\u0006\u0010\t\u001a\u00020\nJ\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u00062\u0006\u0010\t\u001a\u00020\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Ldennis/hinsi/vocubipro/ui/vocabulary/overview/VocabularyViewModel;", "Landroidx/lifecycle/ViewModel;", "vocabularyDataUC", "Ldennis/hinsi/vocubipro/usecases/VocabularyDataUC;", "(Ldennis/hinsi/vocubipro/usecases/VocabularyDataUC;)V", "load", "Lkotlinx/coroutines/flow/Flow;", "", "Ldennis/hinsi/vocubipro/model/container/VocabularyContainer;", "chapterId", "", "loadChapter", "Ldennis/hinsi/vocubipro/model/VocabularyChapter;", "app_debug"})
public final class VocabularyViewModel extends androidx.lifecycle.ViewModel {
    private final dennis.hinsi.vocubipro.usecases.VocabularyDataUC vocabularyDataUC = null;
    
    @javax.inject.Inject()
    public VocabularyViewModel(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.usecases.VocabularyDataUC vocabularyDataUC) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<dennis.hinsi.vocubipro.model.VocabularyChapter> loadChapter(@org.jetbrains.annotations.NotNull()
    java.lang.String chapterId) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<java.util.List<dennis.hinsi.vocubipro.model.container.VocabularyContainer>> load(@org.jetbrains.annotations.NotNull()
    java.lang.String chapterId) {
        return null;
    }
}