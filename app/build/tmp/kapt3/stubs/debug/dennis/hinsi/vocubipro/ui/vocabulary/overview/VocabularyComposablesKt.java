package dennis.hinsi.vocubipro.ui.vocabulary.overview;

import android.annotation.SuppressLint;
import androidx.compose.foundation.layout.*;
import androidx.compose.material.*;
import androidx.compose.material3.CardDefaults;
import androidx.compose.runtime.*;
import androidx.compose.ui.Alignment;
import androidx.compose.ui.Modifier;
import androidx.compose.ui.text.font.FontWeight;
import androidx.navigation.NavController;
import dennis.hinsi.vocubipro.Constants;
import dennis.hinsi.vocubipro.R;
import dennis.hinsi.vocubipro.model.container.VocabularyContainer;
import dennis.hinsi.vocubipro.ui.commoncomposables.NavigationBarEntry;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000.\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u001a\u001c\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\u0003H\u0003\u001a$\u0010\u0005\u001a\u00020\u00012\u0006\u0010\u0006\u001a\u00020\u00072\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0003H\u0003\u001a\"\u0010\t\u001a\u00020\u00012\b\b\u0002\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0007\u00a8\u0006\u0010"}, d2 = {"VocabularyBottomBar", "", "onNavigationClick", "Lkotlin/Function1;", "Ldennis/hinsi/vocubipro/ui/commoncomposables/NavigationBarEntry;", "VocabularyEntryOverviewItem", "container", "Ldennis/hinsi/vocubipro/model/container/VocabularyContainer;", "onClick", "VocabularyMainComposable", "viewModel", "Ldennis/hinsi/vocubipro/ui/vocabulary/overview/VocabularyViewModel;", "navController", "Landroidx/navigation/NavController;", "chapterId", "", "app_debug"})
public final class VocabularyComposablesKt {
    
    @androidx.compose.runtime.Composable()
    @android.annotation.SuppressLint(value = {"UnusedMaterialScaffoldPaddingParameter"})
    public static final void VocabularyMainComposable(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.ui.vocabulary.overview.VocabularyViewModel viewModel, @org.jetbrains.annotations.NotNull()
    androidx.navigation.NavController navController, @org.jetbrains.annotations.NotNull()
    java.lang.String chapterId) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void VocabularyBottomBar(kotlin.jvm.functions.Function1<? super dennis.hinsi.vocubipro.ui.commoncomposables.NavigationBarEntry, kotlin.Unit> onNavigationClick) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void VocabularyEntryOverviewItem(dennis.hinsi.vocubipro.model.container.VocabularyContainer container, kotlin.jvm.functions.Function1<? super dennis.hinsi.vocubipro.model.container.VocabularyContainer, kotlin.Unit> onClick) {
    }
}