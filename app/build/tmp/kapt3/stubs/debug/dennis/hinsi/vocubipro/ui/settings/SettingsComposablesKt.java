package dennis.hinsi.vocubipro.ui.settings;

import android.annotation.SuppressLint;
import androidx.compose.foundation.layout.*;
import androidx.compose.runtime.Composable;
import androidx.compose.ui.Modifier;
import androidx.compose.ui.text.font.FontWeight;
import androidx.navigation.NavController;
import dennis.hinsi.vocubipro.R;
import dennis.hinsi.vocubipro.dev.helper.DevOptions;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000&\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a,\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u00032\u0010\b\u0002\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u0006H\u0003\u001a\u0010\u0010\u0007\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0003\u001a(\u0010\b\u001a\u00020\u00012\u0006\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\f2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00010\u0006H\u0007\u00a8\u0006\u000e"}, d2 = {"SettingsListEntry", "", "title", "", "description", "onClick", "Lkotlin/Function0;", "SettingsListHeader", "SettingsMainComposable", "navController", "Landroidx/navigation/NavController;", "viewModel", "Ldennis/hinsi/vocubipro/ui/settings/SettingsViewModel;", "onLicencesClick", "app_debug"})
public final class SettingsComposablesKt {
    
    @androidx.compose.runtime.Composable()
    @android.annotation.SuppressLint(value = {"UnusedMaterialScaffoldPaddingParameter"})
    public static final void SettingsMainComposable(@org.jetbrains.annotations.NotNull()
    androidx.navigation.NavController navController, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.ui.settings.SettingsViewModel viewModel, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> onLicencesClick) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void SettingsListHeader(java.lang.String title) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void SettingsListEntry(java.lang.String title, java.lang.String description, kotlin.jvm.functions.Function0<kotlin.Unit> onClick) {
    }
}