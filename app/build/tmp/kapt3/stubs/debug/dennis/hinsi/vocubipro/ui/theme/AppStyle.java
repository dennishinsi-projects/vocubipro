package dennis.hinsi.vocubipro.ui.theme;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0005\u001a\u00020\u0006\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002\u00a2\u0006\n\n\u0002\u0010\t\u001a\u0004\b\u0007\u0010\bR\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u0082\u0002\u000f\n\u0002\b\u0019\n\u0005\b\u00a1\u001e0\u0001\n\u0002\b!\u00a8\u0006\u000f"}, d2 = {"Ldennis/hinsi/vocubipro/ui/theme/AppStyle;", "", "()V", "BACKGROUND_COLOR", "", "BOTTOM_NAVIGATION_ELEVATION", "Landroidx/compose/ui/unit/Dp;", "getBOTTOM_NAVIGATION_ELEVATION-D9Ej5fM", "()F", "F", "DIVIDER_COLOR", "DIVIDER_COLOR_DARK", "FOREGROUND_COLOR", "NAVIGATION_BAR_COLOR_REF", "STATUS_BAR_COLOR_REF", "app_debug"})
public final class AppStyle {
    @org.jetbrains.annotations.NotNull()
    public static final dennis.hinsi.vocubipro.ui.theme.AppStyle INSTANCE = null;
    public static final int STATUS_BAR_COLOR_REF = android.R.color.system_accent1_100;
    public static final int NAVIGATION_BAR_COLOR_REF = android.R.color.system_neutral1_50;
    public static final int BACKGROUND_COLOR = android.R.color.system_neutral1_10;
    public static final int FOREGROUND_COLOR = android.R.color.system_neutral1_50;
    public static final int DIVIDER_COLOR = android.R.color.system_neutral1_50;
    public static final int DIVIDER_COLOR_DARK = android.R.color.system_neutral1_200;
    private static final float BOTTOM_NAVIGATION_ELEVATION = 0.0F;
    
    private AppStyle() {
        super();
    }
}