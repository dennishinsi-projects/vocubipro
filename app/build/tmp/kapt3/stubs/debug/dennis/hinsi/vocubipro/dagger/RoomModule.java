package dennis.hinsi.vocubipro.dagger;

import android.content.Context;
import androidx.room.Room;
import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.qualifiers.ApplicationContext;
import dagger.hilt.components.SingletonComponent;
import dennis.hinsi.vocubipro.database.*;
import dennis.hinsi.vocubipro.repository.*;
import javax.inject.Singleton;

@dagger.hilt.InstallIn(value = {dagger.hilt.components.SingletonComponent.class})
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0004H\u0007J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\t\u001a\u00020\u0004H\u0007J\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\t\u001a\u00020\u0004H\u0007J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\t\u001a\u00020\u0004H\u0007J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\t\u001a\u00020\u0004H\u0007\u00a8\u0006\u0012"}, d2 = {"Ldennis/hinsi/vocubipro/dagger/RoomModule;", "", "()V", "provideAppDatabase", "Ldennis/hinsi/vocubipro/database/AppDataBase;", "context", "Landroid/content/Context;", "provideLanguageRepository", "Ldennis/hinsi/vocubipro/repository/LanguageRepository;", "dataBase", "provideVocabularyChapterRepository", "Ldennis/hinsi/vocubipro/repository/VocabularyChapterRepository;", "provideVocabularyEntryRepository", "Ldennis/hinsi/vocubipro/repository/VocabularyEntryRepository;", "provideVocabularyGroupRepository", "Ldennis/hinsi/vocubipro/repository/VocabularyGroupRepository;", "provideVocabularyRepository", "Ldennis/hinsi/vocubipro/repository/VocabularyRepository;", "app_debug"})
@dagger.Module()
public final class RoomModule {
    
    public RoomModule() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Singleton()
    @dagger.Provides()
    public final dennis.hinsi.vocubipro.database.AppDataBase provideAppDatabase(@org.jetbrains.annotations.NotNull()
    @dagger.hilt.android.qualifiers.ApplicationContext()
    android.content.Context context) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Singleton()
    @dagger.Provides()
    public final dennis.hinsi.vocubipro.repository.VocabularyGroupRepository provideVocabularyGroupRepository(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.database.AppDataBase dataBase) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Singleton()
    @dagger.Provides()
    public final dennis.hinsi.vocubipro.repository.VocabularyChapterRepository provideVocabularyChapterRepository(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.database.AppDataBase dataBase) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Singleton()
    @dagger.Provides()
    public final dennis.hinsi.vocubipro.repository.VocabularyRepository provideVocabularyRepository(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.database.AppDataBase dataBase) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Singleton()
    @dagger.Provides()
    public final dennis.hinsi.vocubipro.repository.VocabularyEntryRepository provideVocabularyEntryRepository(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.database.AppDataBase dataBase) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Singleton()
    @dagger.Provides()
    public final dennis.hinsi.vocubipro.repository.LanguageRepository provideLanguageRepository(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.database.AppDataBase dataBase) {
        return null;
    }
}