package dennis.hinsi.vocubipro.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000,\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a\"\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0002\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0006\u001a\u001a\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0002\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0007\u001a\u0019\u0010\b\u001a\u00020\t*\u0004\u0018\u00010\t2\u0006\u0010\n\u001a\u00020\t\u00a2\u0006\u0002\u0010\u000b\u001a\f\u0010\f\u001a\u00020\t*\u0004\u0018\u00010\u0002\u001a+\u0010\r\u001a\u00020\u000e\"\u0004\b\u0000\u0010\u000f*\u0004\u0018\u0001H\u000f2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0011H\u0086\b\u00f8\u0001\u0001\u00a2\u0006\u0002\u0010\u0012\u001a\f\u0010\u0013\u001a\u00020\u0002*\u0004\u0018\u00010\u0002\u0082\u0002\u000b\n\u0002\b\u0019\n\u0005\b\u009920\u0001\u00a8\u0006\u0014"}, d2 = {"colorFromHexOrDefault", "Landroidx/compose/ui/graphics/Color;", "", "alpha", "", "defaultHex", "(Ljava/lang/String;ILjava/lang/String;)J", "(Ljava/lang/String;Ljava/lang/String;)J", "inverseOrDefault", "", "default", "(Ljava/lang/Boolean;Z)Z", "notNullOrEmpty", "onNull", "", "T", "block", "Lkotlin/Function0;", "(Ljava/lang/Object;Lkotlin/jvm/functions/Function0;)V", "orEmpty", "app_debug"})
public final class KotlinExtensionsKt {
    
    public static final <T extends java.lang.Object>void onNull(@org.jetbrains.annotations.Nullable()
    T $this$onNull, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> block) {
    }
    
    public static final boolean notNullOrEmpty(@org.jetbrains.annotations.Nullable()
    java.lang.String $this$notNullOrEmpty) {
        return false;
    }
    
    public static final long colorFromHexOrDefault(@org.jetbrains.annotations.NotNull()
    java.lang.String $this$colorFromHexOrDefault, @org.jetbrains.annotations.NotNull()
    java.lang.String defaultHex) {
        return 0L;
    }
    
    public static final long colorFromHexOrDefault(@org.jetbrains.annotations.NotNull()
    java.lang.String $this$colorFromHexOrDefault, int alpha, @org.jetbrains.annotations.NotNull()
    java.lang.String defaultHex) {
        return 0L;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String orEmpty(@org.jetbrains.annotations.Nullable()
    java.lang.String $this$orEmpty) {
        return null;
    }
    
    public static final boolean inverseOrDefault(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean $this$inverseOrDefault, boolean p1_772401952) {
        return false;
    }
}