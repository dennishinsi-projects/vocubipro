package dennis.hinsi.vocubipro.ui.vocabulary.detail;

import androidx.compose.foundation.layout.*;
import androidx.compose.material.*;
import androidx.compose.material3.ButtonDefaults;
import androidx.compose.material3.CardDefaults;
import androidx.compose.runtime.*;
import androidx.compose.ui.Alignment;
import androidx.compose.ui.Modifier;
import androidx.compose.ui.graphics.ColorFilter;
import androidx.compose.ui.text.font.FontWeight;
import androidx.compose.ui.unit.Dp;
import androidx.navigation.NavController;
import dennis.hinsi.vocubipro.Constants;
import dennis.hinsi.vocubipro.R;
import dennis.hinsi.vocubipro.model.*;
import dennis.hinsi.vocubipro.model.container.VocabularyEntryContainer;
import dennis.hinsi.vocubipro.ui.commoncomposables.*;
import java.util.*;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000R\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a\u001c\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\u0003H\u0003\u001a(\u0010\u0005\u001a\u00020\u00012\b\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u0014\u0010\b\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0007\u0012\u0004\u0012\u00020\u00010\u0003H\u0007\u001a,\u0010\t\u001a\u00020\u00012\b\b\u0002\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fH\u0007\u001a8\u0010\u0011\u001a\u00020\u00012\u0006\u0010\u0012\u001a\u00020\u00132\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00010\u00032\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00010\u0003H\u0003\u001a\u001a\u0010\u0015\u001a\u00020\u00012\b\b\u0002\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0003\u001a(\u0010\u0015\u001a\u00020\u00012\b\u0010\u0018\u001a\u0004\u0018\u00010\u00192\u0014\u0010\b\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0019\u0012\u0004\u0012\u00020\u00010\u0003H\u0003\u001a+\u0010\u001a\u001a\u00020\u00012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00010\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0003\u00f8\u0001\u0000\u00f8\u0001\u0001\u00a2\u0006\u0004\b\u001e\u0010\u001f\u0082\u0002\u000b\n\u0002\b\u0019\n\u0005\b\u00a1\u001e0\u0001\u00a8\u0006 "}, d2 = {"VocabularyDetailBottomBar", "", "onNavigationClick", "Lkotlin/Function1;", "Ldennis/hinsi/vocubipro/ui/commoncomposables/NavigationBarEntry;", "VocabularyDetailCard", "vocabulary", "Ldennis/hinsi/vocubipro/model/Vocabulary;", "onClick", "VocabularyDetailComposable", "viewModel", "Ldennis/hinsi/vocubipro/ui/vocabulary/detail/VocabularyDetailViewModel;", "navController", "Landroidx/navigation/NavController;", "chapterId", "", "vocabularyId", "VocabularyDetailEntryItemCard", "entryContainer", "Ldennis/hinsi/vocubipro/model/container/VocabularyEntryContainer;", "onDeleteClick", "VocabularyDetailLanguageCard", "modifier", "Landroidx/compose/ui/Modifier;", "language", "Ldennis/hinsi/vocubipro/model/Language;", "VocabularyDetailPlaceholder", "Lkotlin/Function0;", "height", "Landroidx/compose/ui/unit/Dp;", "VocabularyDetailPlaceholder-3ABfNKs", "(Lkotlin/jvm/functions/Function0;F)V", "app_debug"})
public final class VocabularyDetailComposablesKt {
    
    @androidx.compose.runtime.Composable()
    @kotlin.OptIn(markerClass = {androidx.compose.material.ExperimentalMaterialApi.class})
    public static final void VocabularyDetailComposable(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.ui.vocabulary.detail.VocabularyDetailViewModel viewModel, @org.jetbrains.annotations.NotNull()
    androidx.navigation.NavController navController, @org.jetbrains.annotations.NotNull()
    java.lang.String chapterId, @org.jetbrains.annotations.Nullable()
    java.lang.String vocabularyId) {
    }
    
    @androidx.compose.runtime.Composable()
    public static final void VocabularyDetailCard(@org.jetbrains.annotations.Nullable()
    dennis.hinsi.vocubipro.model.Vocabulary vocabulary, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super dennis.hinsi.vocubipro.model.Vocabulary, kotlin.Unit> onClick) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void VocabularyDetailEntryItemCard(dennis.hinsi.vocubipro.model.container.VocabularyEntryContainer entryContainer, kotlin.jvm.functions.Function1<? super dennis.hinsi.vocubipro.model.container.VocabularyEntryContainer, kotlin.Unit> onClick, kotlin.jvm.functions.Function1<? super dennis.hinsi.vocubipro.model.container.VocabularyEntryContainer, kotlin.Unit> onDeleteClick) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void VocabularyDetailLanguageCard(dennis.hinsi.vocubipro.model.Language language, kotlin.jvm.functions.Function1<? super dennis.hinsi.vocubipro.model.Language, kotlin.Unit> onClick) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void VocabularyDetailBottomBar(kotlin.jvm.functions.Function1<? super dennis.hinsi.vocubipro.ui.commoncomposables.NavigationBarEntry, kotlin.Unit> onNavigationClick) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void VocabularyDetailLanguageCard(androidx.compose.ui.Modifier modifier, dennis.hinsi.vocubipro.model.Language language) {
    }
}