package dennis.hinsi.vocubipro.ui.group;

import androidx.compose.foundation.layout.*;
import androidx.compose.material.*;
import androidx.compose.runtime.*;
import androidx.compose.ui.Modifier;
import androidx.compose.ui.text.font.FontWeight;
import androidx.compose.ui.text.input.ImeAction;
import androidx.navigation.NavController;
import dennis.hinsi.vocubipro.R;
import dennis.hinsi.vocubipro.ui.commoncomposables.*;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000*\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u001c\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\u0003H\u0003\u001a,\u0010\u0005\u001a\u00020\u00012\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u00072\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0003H\u0007\u001a$\u0010\n\u001a\u00020\u00012\b\b\u0002\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0007H\u0007\u00a8\u0006\u0010"}, d2 = {"GroupDetailBottomBar", "", "onNavigationClick", "Lkotlin/Function1;", "Ldennis/hinsi/vocubipro/ui/commoncomposables/NavigationBarEntry;", "GroupDetailTitleInputCard", "cardTitle", "", "title", "onTitleChange", "GroupMainComposable", "viewModel", "Ldennis/hinsi/vocubipro/ui/group/GroupDetailViewModel;", "navController", "Landroidx/navigation/NavController;", "groupId", "app_debug"})
public final class GroupDetailComposablesKt {
    
    @androidx.compose.runtime.Composable()
    @kotlin.OptIn(markerClass = {androidx.compose.material.ExperimentalMaterialApi.class})
    public static final void GroupMainComposable(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.ui.group.GroupDetailViewModel viewModel, @org.jetbrains.annotations.NotNull()
    androidx.navigation.NavController navController, @org.jetbrains.annotations.Nullable()
    java.lang.String groupId) {
    }
    
    @androidx.compose.runtime.Composable()
    public static final void GroupDetailTitleInputCard(@org.jetbrains.annotations.NotNull()
    java.lang.String cardTitle, @org.jetbrains.annotations.NotNull()
    java.lang.String title, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onTitleChange) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void GroupDetailBottomBar(kotlin.jvm.functions.Function1<? super dennis.hinsi.vocubipro.ui.commoncomposables.NavigationBarEntry, kotlin.Unit> onNavigationClick) {
    }
}