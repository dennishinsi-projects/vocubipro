package dennis.hinsi.vocubipro.ui.vocabulary.detail;

import androidx.lifecycle.ViewModel;
import dagger.hilt.android.lifecycle.HiltViewModel;
import dennis.hinsi.vocubipro.model.*;
import dennis.hinsi.vocubipro.model.container.VocabularyEntryContainer;
import dennis.hinsi.vocubipro.usecases.DataDeletionUC;
import dennis.hinsi.vocubipro.usecases.LanguageDataUC;
import dennis.hinsi.vocubipro.usecases.VocabularyDataUC;
import kotlinx.coroutines.flow.*;
import java.util.*;
import javax.inject.Inject;

@dagger.hilt.android.lifecycle.HiltViewModel()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0007\u0018\u00002\u00020\u0001B\u001f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\u000e\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015J\u0016\u0010\u0016\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u0015J\u0016\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00190\n2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0015J\u0016\u0010\u001b\u001a\u00020\u00132\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u0019J:\u0010\u001f\u001a\u00020\u00132\u0006\u0010\u0017\u001a\u00020\u00152\b\u0010 \u001a\u0004\u0018\u00010!2\b\u0010\"\u001a\u0004\u0018\u00010\u00102\u0006\u0010#\u001a\u00020\u00152\u0006\u0010$\u001a\u00020\u00152\u0006\u0010%\u001a\u00020\u0015R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\n8F\u00a2\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000b0\n8F\u00a2\u0006\u0006\u001a\u0004\b\u0011\u0010\u000eR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"}, d2 = {"Ldennis/hinsi/vocubipro/ui/vocabulary/detail/VocabularyDetailViewModel;", "Landroidx/lifecycle/ViewModel;", "languageDataUC", "Ldennis/hinsi/vocubipro/usecases/LanguageDataUC;", "vocabularyDataUC", "Ldennis/hinsi/vocubipro/usecases/VocabularyDataUC;", "deletionUC", "Ldennis/hinsi/vocubipro/usecases/DataDeletionUC;", "(Ldennis/hinsi/vocubipro/usecases/LanguageDataUC;Ldennis/hinsi/vocubipro/usecases/VocabularyDataUC;Ldennis/hinsi/vocubipro/usecases/DataDeletionUC;)V", "infinityEntryFlow", "Lkotlinx/coroutines/flow/Flow;", "", "Ldennis/hinsi/vocubipro/model/VocabularyEntry;", "getInfinityEntryFlow", "()Lkotlinx/coroutines/flow/Flow;", "languages", "Ldennis/hinsi/vocubipro/model/Language;", "getLanguages", "deleteEntry", "Lkotlinx/coroutines/Job;", "id", "", "deleteVocabulary", "entryId", "loadVocabulary", "Ldennis/hinsi/vocubipro/model/Vocabulary;", "vocabularyId", "submit", "alreadyStored", "", "vocabulary", "submitEntry", "vocabularyEntryContainer", "Ldennis/hinsi/vocubipro/model/container/VocabularyEntryContainer;", "language", "newText", "newPhoneticText", "newTranscriptionText", "app_debug"})
public final class VocabularyDetailViewModel extends androidx.lifecycle.ViewModel {
    private final dennis.hinsi.vocubipro.usecases.LanguageDataUC languageDataUC = null;
    private final dennis.hinsi.vocubipro.usecases.VocabularyDataUC vocabularyDataUC = null;
    private final dennis.hinsi.vocubipro.usecases.DataDeletionUC deletionUC = null;
    
    @javax.inject.Inject()
    public VocabularyDetailViewModel(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.usecases.LanguageDataUC languageDataUC, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.usecases.VocabularyDataUC vocabularyDataUC, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.usecases.DataDeletionUC deletionUC) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<java.util.List<dennis.hinsi.vocubipro.model.Language>> getLanguages() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<java.util.List<dennis.hinsi.vocubipro.model.VocabularyEntry>> getInfinityEntryFlow() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<dennis.hinsi.vocubipro.model.Vocabulary> loadVocabulary(@org.jetbrains.annotations.Nullable()
    java.lang.String vocabularyId) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job submit(boolean alreadyStored, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.model.Vocabulary vocabulary) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job submitEntry(@org.jetbrains.annotations.NotNull()
    java.lang.String entryId, @org.jetbrains.annotations.Nullable()
    dennis.hinsi.vocubipro.model.container.VocabularyEntryContainer vocabularyEntryContainer, @org.jetbrains.annotations.Nullable()
    dennis.hinsi.vocubipro.model.Language language, @org.jetbrains.annotations.NotNull()
    java.lang.String newText, @org.jetbrains.annotations.NotNull()
    java.lang.String newPhoneticText, @org.jetbrains.annotations.NotNull()
    java.lang.String newTranscriptionText) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job deleteEntry(@org.jetbrains.annotations.NotNull()
    java.lang.String id) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job deleteVocabulary(@org.jetbrains.annotations.NotNull()
    java.lang.String id, @org.jetbrains.annotations.NotNull()
    java.lang.String entryId) {
        return null;
    }
}