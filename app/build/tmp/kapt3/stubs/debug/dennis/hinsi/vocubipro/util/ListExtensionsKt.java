package dennis.hinsi.vocubipro.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\u001e\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a*\u0010\u0000\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u00010\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004\u001a8\u0010\u0005\u001a\u00020\u0006\"\u0004\b\u0000\u0010\u0002*\n\u0012\u0004\u0012\u0002H\u0002\u0018\u00010\u00012\u0018\u0010\u0007\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u0001\u0012\u0004\u0012\u00020\u00060\bH\u0086\b\u00f8\u0001\u0000\u001a\u0018\u0010\t\u001a\u00020\u0004\"\u0004\b\u0000\u0010\u0002*\n\u0012\u0004\u0012\u0002H\u0002\u0018\u00010\u0001\u0082\u0002\u0007\n\u0005\b\u009920\u0001\u00a8\u0006\n"}, d2 = {"groupByIndexed", "", "T", "targetIndex", "", "notNullOrEmpty", "", "block", "Lkotlin/Function1;", "sizeOrZero", "app_debug"})
public final class ListExtensionsKt {
    
    public static final <T extends java.lang.Object>void notNullOrEmpty(@org.jetbrains.annotations.Nullable()
    java.util.List<? extends T> $this$notNullOrEmpty, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.util.List<? extends T>, kotlin.Unit> block) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final <T extends java.lang.Object>java.util.List<java.util.List<T>> groupByIndexed(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends T> $this$groupByIndexed, int targetIndex) {
        return null;
    }
    
    public static final <T extends java.lang.Object>int sizeOrZero(@org.jetbrains.annotations.Nullable()
    java.util.List<? extends T> $this$sizeOrZero) {
        return 0;
    }
}