package dennis.hinsi.vocubipro.ui.languages.detail;

import android.annotation.SuppressLint;
import androidx.annotation.StringRes;
import androidx.compose.foundation.*;
import androidx.compose.foundation.layout.*;
import androidx.compose.material.*;
import androidx.compose.material3.*;
import androidx.compose.material3.ButtonDefaults;
import androidx.compose.runtime.*;
import androidx.compose.ui.Alignment;
import androidx.compose.ui.Modifier;
import androidx.compose.ui.text.font.FontWeight;
import androidx.navigation.NavController;
import dennis.hinsi.vocubipro.Constants;
import dennis.hinsi.vocubipro.dev.mock.MockDataProvider;
import dennis.hinsi.vocubipro.R;
import dennis.hinsi.vocubipro.ui.commoncomposables.*;
import dennis.hinsi.vocubipro.ui.languages.LanguageDetailViewModel;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000Z\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u001a$\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00010\u0005H\u0007\u001a\u001c\u0010\u0006\u001a\u00020\u00012\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00010\u0005H\u0003\u001a%\u0010\t\u001a\u00020\u00012\b\b\u0001\u0010\n\u001a\u00020\u000b2\u0011\u0010\f\u001a\r\u0012\u0004\u0012\u00020\u00010\r\u00a2\u0006\u0002\b\u000eH\u0007\u001a^\u0010\u000f\u001a\u00020\u00012\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00030\u00112\u0006\u0010\u0012\u001a\u00020\u00132\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\u00112\u0006\u0010\u0016\u001a\u00020\u00172\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00030\u00112\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00030\u00112\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00030\u0011H\u0003\u001a&\u0010\u001b\u001a\u00020\u00012\u0006\u0010\u001c\u001a\u00020\u001d2\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u001f\u001a\u00020 H\u0007\u001a$\u0010!\u001a\u00020\u00012\u0006\u0010\"\u001a\u00020\u00032\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00010\u0005H\u0007\u001a@\u0010#\u001a\u00020\u00012\u0006\u0010$\u001a\u00020\u00032\u0006\u0010%\u001a\u00020\u00032\u0012\u0010&\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00010\u00052\u0012\u0010\'\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00010\u0005H\u0007\u00a8\u0006("}, d2 = {"LanguageAccentColorSelectorCard", "", "selectedColor", "", "onChange", "Lkotlin/Function1;", "LanguageDetailBottomBar", "onNavigationClick", "Ldennis/hinsi/vocubipro/ui/commoncomposables/NavigationBarEntry;", "LanguageDetailCard", "title", "", "content", "Lkotlin/Function0;", "Landroidx/compose/runtime/Composable;", "LanguageDetailContent", "rememberSelectedFlag", "Landroidx/compose/runtime/MutableState;", "coroutineScope", "Lkotlinx/coroutines/CoroutineScope;", "modalBottomSheetType", "Ldennis/hinsi/vocubipro/ui/languages/detail/LanguageDetailBottomSheetType;", "modalBottomSheetState", "Landroidx/compose/material/ModalBottomSheetState;", "rememberSelectedColor", "rememberInputTitle", "rememberInputTitleTranslated", "LanguageDetailView", "navController", "Landroidx/navigation/NavController;", "languageId", "viewModel", "Ldennis/hinsi/vocubipro/ui/languages/LanguageDetailViewModel;", "LanguageFlagSelectorCard", "selectedFlag", "LanguageNameSelectorCard", "storedText", "storedTextTranslated", "onChangeText", "onChangeTranslatedText", "app_debug"})
public final class LanguageDetailComposablesKt {
    
    @androidx.compose.runtime.Composable()
    @kotlin.OptIn(markerClass = {androidx.compose.material3.ExperimentalMaterial3Api.class, androidx.compose.material.ExperimentalMaterialApi.class})
    @android.annotation.SuppressLint(value = {"UnusedMaterial3ScaffoldPaddingParameter"})
    public static final void LanguageDetailView(@org.jetbrains.annotations.NotNull()
    androidx.navigation.NavController navController, @org.jetbrains.annotations.Nullable()
    java.lang.String languageId, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.ui.languages.LanguageDetailViewModel viewModel) {
    }
    
    @androidx.compose.runtime.Composable()
    @kotlin.OptIn(markerClass = {androidx.compose.material.ExperimentalMaterialApi.class})
    private static final void LanguageDetailContent(androidx.compose.runtime.MutableState<java.lang.String> rememberSelectedFlag, kotlinx.coroutines.CoroutineScope coroutineScope, androidx.compose.runtime.MutableState<dennis.hinsi.vocubipro.ui.languages.detail.LanguageDetailBottomSheetType> modalBottomSheetType, androidx.compose.material.ModalBottomSheetState modalBottomSheetState, androidx.compose.runtime.MutableState<java.lang.String> rememberSelectedColor, androidx.compose.runtime.MutableState<java.lang.String> rememberInputTitle, androidx.compose.runtime.MutableState<java.lang.String> rememberInputTitleTranslated) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void LanguageDetailBottomBar(kotlin.jvm.functions.Function1<? super dennis.hinsi.vocubipro.ui.commoncomposables.NavigationBarEntry, kotlin.Unit> onNavigationClick) {
    }
    
    @androidx.compose.runtime.Composable()
    public static final void LanguageDetailCard(@androidx.annotation.StringRes()
    int title, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> content) {
    }
    
    @androidx.compose.runtime.Composable()
    public static final void LanguageFlagSelectorCard(@org.jetbrains.annotations.NotNull()
    java.lang.String selectedFlag, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onChange) {
    }
    
    @androidx.compose.runtime.Composable()
    public static final void LanguageAccentColorSelectorCard(@org.jetbrains.annotations.NotNull()
    java.lang.String selectedColor, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onChange) {
    }
    
    @androidx.compose.runtime.Composable()
    @kotlin.OptIn(markerClass = {androidx.compose.material3.ExperimentalMaterial3Api.class})
    public static final void LanguageNameSelectorCard(@org.jetbrains.annotations.NotNull()
    java.lang.String storedText, @org.jetbrains.annotations.NotNull()
    java.lang.String storedTextTranslated, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onChangeText, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onChangeTranslatedText) {
    }
}