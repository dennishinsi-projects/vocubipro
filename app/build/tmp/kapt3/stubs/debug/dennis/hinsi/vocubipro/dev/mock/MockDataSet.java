package dennis.hinsi.vocubipro.dev.mock;

import dennis.hinsi.vocubipro.Constants;
import dennis.hinsi.vocubipro.model.*;
import java.util.*;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u001e\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u00042\u0006\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\rJ\u0016\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00042\b\b\u0002\u0010\f\u001a\u00020\rJ\u001e\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\u00042\u0006\u0010\u0012\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\rJ\u001e\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u000b2\u0006\u0010\u0016\u001a\u00020\u000b2\u0006\u0010\u0017\u001a\u00020\rR\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\u0018"}, d2 = {"Ldennis/hinsi/vocubipro/dev/mock/MockDataSet;", "", "()V", "languages", "", "Ldennis/hinsi/vocubipro/model/Language;", "getLanguages", "()Ljava/util/List;", "provideChapters", "Ldennis/hinsi/vocubipro/model/VocabularyChapter;", "groupId", "", "size", "", "provideGroups", "Ldennis/hinsi/vocubipro/model/VocabularyGroup;", "provideVocabularies", "Ldennis/hinsi/vocubipro/model/Vocabulary;", "chapterId", "provideVocabularyEntry", "Ldennis/hinsi/vocubipro/model/VocabularyEntry;", "languageId", "entryId", "index", "app_debug"})
public final class MockDataSet {
    @org.jetbrains.annotations.NotNull()
    public static final dennis.hinsi.vocubipro.dev.mock.MockDataSet INSTANCE = null;
    @org.jetbrains.annotations.NotNull()
    private static final java.util.List<dennis.hinsi.vocubipro.model.Language> languages = null;
    
    private MockDataSet() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<dennis.hinsi.vocubipro.model.Language> getLanguages() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<dennis.hinsi.vocubipro.model.VocabularyGroup> provideGroups(int size) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<dennis.hinsi.vocubipro.model.VocabularyChapter> provideChapters(@org.jetbrains.annotations.NotNull()
    java.lang.String groupId, int size) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<dennis.hinsi.vocubipro.model.Vocabulary> provideVocabularies(@org.jetbrains.annotations.NotNull()
    java.lang.String chapterId, int size) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final dennis.hinsi.vocubipro.model.VocabularyEntry provideVocabularyEntry(@org.jetbrains.annotations.NotNull()
    java.lang.String languageId, @org.jetbrains.annotations.NotNull()
    java.lang.String entryId, int index) {
        return null;
    }
}