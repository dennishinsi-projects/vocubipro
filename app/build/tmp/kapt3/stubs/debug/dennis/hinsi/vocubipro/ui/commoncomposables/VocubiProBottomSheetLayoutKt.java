package dennis.hinsi.vocubipro.ui.commoncomposables;

import androidx.compose.material.ExperimentalMaterialApi;
import androidx.compose.material.ModalBottomSheetState;
import androidx.compose.runtime.Composable;
import androidx.compose.ui.Modifier;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\"\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a<\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0017\u0010\u0004\u001a\u0013\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00010\u0005\u00a2\u0006\u0002\b\u00072\u0011\u0010\b\u001a\r\u0012\u0004\u0012\u00020\u00010\t\u00a2\u0006\u0002\b\u0007H\u0007\u00a8\u0006\n"}, d2 = {"VocubiProBottomSheetLayout", "", "sheetState", "Landroidx/compose/material/ModalBottomSheetState;", "sheetLayout", "Lkotlin/Function1;", "", "Landroidx/compose/runtime/Composable;", "content", "Lkotlin/Function0;", "app_debug"})
public final class VocubiProBottomSheetLayoutKt {
    
    @androidx.compose.runtime.Composable()
    @kotlin.OptIn(markerClass = {androidx.compose.material.ExperimentalMaterialApi.class})
    public static final void VocubiProBottomSheetLayout(@org.jetbrains.annotations.NotNull()
    androidx.compose.material.ModalBottomSheetState sheetState, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> sheetLayout, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> content) {
    }
}