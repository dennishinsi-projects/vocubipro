package dennis.hinsi.vocubipro.usecases;

import dennis.hinsi.vocubipro.model.VocabularyGroup;
import dennis.hinsi.vocubipro.repository.VocabularyChapterRepository;
import dennis.hinsi.vocubipro.repository.VocabularyGroupRepository;
import javax.inject.Inject;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0019\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000bJ\u0019\u0010\f\u001a\u00020\b2\u0006\u0010\r\u001a\u00020\u000eH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000fJ\u0012\u0010\u0010\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\u00120\u0011J\u0019\u0010\u0013\u001a\u00020\u000e2\u0006\u0010\u0014\u001a\u00020\nH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000bJ\u0019\u0010\u0015\u001a\u00020\b2\u0006\u0010\r\u001a\u00020\u000eH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0016"}, d2 = {"Ldennis/hinsi/vocubipro/usecases/GroupDataUC;", "", "groupRepository", "Ldennis/hinsi/vocubipro/repository/VocabularyGroupRepository;", "chapterRepository", "Ldennis/hinsi/vocubipro/repository/VocabularyChapterRepository;", "(Ldennis/hinsi/vocubipro/repository/VocabularyGroupRepository;Ldennis/hinsi/vocubipro/repository/VocabularyChapterRepository;)V", "delete", "", "id", "", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "insert", "data", "Ldennis/hinsi/vocubipro/model/VocabularyGroup;", "(Ldennis/hinsi/vocubipro/model/VocabularyGroup;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "loadAllGroups", "Lkotlinx/coroutines/flow/Flow;", "", "loadGroup", "groupId", "update", "app_debug"})
public final class GroupDataUC {
    private final dennis.hinsi.vocubipro.repository.VocabularyGroupRepository groupRepository = null;
    private final dennis.hinsi.vocubipro.repository.VocabularyChapterRepository chapterRepository = null;
    
    @javax.inject.Inject()
    public GroupDataUC(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.repository.VocabularyGroupRepository groupRepository, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.repository.VocabularyChapterRepository chapterRepository) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<java.util.List<dennis.hinsi.vocubipro.model.VocabularyGroup>> loadAllGroups() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object loadGroup(@org.jetbrains.annotations.NotNull()
    java.lang.String groupId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super dennis.hinsi.vocubipro.model.VocabularyGroup> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object insert(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.model.VocabularyGroup data, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object update(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.model.VocabularyGroup data, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object delete(@org.jetbrains.annotations.NotNull()
    java.lang.String id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
}