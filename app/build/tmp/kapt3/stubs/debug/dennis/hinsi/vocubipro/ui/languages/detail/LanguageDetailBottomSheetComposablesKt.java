package dennis.hinsi.vocubipro.ui.languages.detail;

import androidx.compose.foundation.*;
import androidx.compose.foundation.layout.*;
import androidx.compose.runtime.Composable;
import androidx.compose.ui.Alignment;
import androidx.compose.ui.Modifier;
import androidx.compose.ui.text.font.FontWeight;
import dennis.hinsi.vocubipro.Constants;
import dennis.hinsi.vocubipro.dev.mock.MockDataProvider;
import dennis.hinsi.vocubipro.R;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u001a$\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00010\u0005H\u0007\u001a$\u0010\u0006\u001a\u00020\u00012\u0006\u0010\u0007\u001a\u00020\u00032\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00010\u0005H\u0007\u00a8\u0006\t"}, d2 = {"LanguageDetailColorSelectorBottomSheet", "", "selectedColor", "", "onColorChange", "Lkotlin/Function1;", "LanguageDetailFlagSelectorBottomSheet", "selectedFlag", "onFlagChange", "app_debug"})
public final class LanguageDetailBottomSheetComposablesKt {
    
    @androidx.compose.runtime.Composable()
    public static final void LanguageDetailFlagSelectorBottomSheet(@org.jetbrains.annotations.NotNull()
    java.lang.String selectedFlag, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onFlagChange) {
    }
    
    @androidx.compose.runtime.Composable()
    public static final void LanguageDetailColorSelectorBottomSheet(@org.jetbrains.annotations.NotNull()
    java.lang.String selectedColor, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onColorChange) {
    }
}