package dennis.hinsi.vocubipro.ui.chapter.detail;

import androidx.compose.foundation.layout.*;
import androidx.compose.material.*;
import androidx.compose.runtime.*;
import androidx.compose.ui.Modifier;
import androidx.compose.ui.text.font.FontWeight;
import androidx.compose.ui.text.input.ImeAction;
import androidx.navigation.NavController;
import dennis.hinsi.vocubipro.R;
import dennis.hinsi.vocubipro.ui.commoncomposables.*;
import dennis.hinsi.vocubipro.util.ListIndexInt;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u00004\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0006\u001a\u001c\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\u0003H\u0003\u001aA\u0010\u0005\u001a\u00020\u00012\b\b\u0002\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u000b2\u000e\u0010\r\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\u000fH\u0007\u00a2\u0006\u0002\u0010\u0010\u001a,\u0010\u0011\u001a\u00020\u00012\u0006\u0010\u0012\u001a\u00020\u000b2\u0006\u0010\u0013\u001a\u00020\u000b2\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00010\u0003H\u0007\u00a8\u0006\u0015"}, d2 = {"ChapterDetailBottomBar", "", "onNavigationClick", "Lkotlin/Function1;", "Ldennis/hinsi/vocubipro/ui/commoncomposables/NavigationBarEntry;", "ChapterDetailComposable", "viewModel", "Ldennis/hinsi/vocubipro/ui/chapter/detail/ChapterDetailViewModel;", "navController", "Landroidx/navigation/NavController;", "groupId", "", "chapterId", "chapterPosition", "", "Ldennis/hinsi/vocubipro/util/ListIndexInt;", "(Ldennis/hinsi/vocubipro/ui/chapter/detail/ChapterDetailViewModel;Landroidx/navigation/NavController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V", "ChapterDetailTitleInputCard", "cardTitle", "title", "onTitleChange", "app_debug"})
public final class ChapterDetailComposablesKt {
    
    @androidx.compose.runtime.Composable()
    @kotlin.OptIn(markerClass = {androidx.compose.material.ExperimentalMaterialApi.class})
    public static final void ChapterDetailComposable(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.ui.chapter.detail.ChapterDetailViewModel viewModel, @org.jetbrains.annotations.NotNull()
    androidx.navigation.NavController navController, @org.jetbrains.annotations.NotNull()
    java.lang.String groupId, @org.jetbrains.annotations.Nullable()
    java.lang.String chapterId, @org.jetbrains.annotations.Nullable()
    java.lang.Integer chapterPosition) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void ChapterDetailBottomBar(kotlin.jvm.functions.Function1<? super dennis.hinsi.vocubipro.ui.commoncomposables.NavigationBarEntry, kotlin.Unit> onNavigationClick) {
    }
    
    @androidx.compose.runtime.Composable()
    public static final void ChapterDetailTitleInputCard(@org.jetbrains.annotations.NotNull()
    java.lang.String cardTitle, @org.jetbrains.annotations.NotNull()
    java.lang.String title, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onTitleChange) {
    }
}