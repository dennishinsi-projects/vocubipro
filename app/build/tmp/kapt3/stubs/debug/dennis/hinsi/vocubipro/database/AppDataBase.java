package dennis.hinsi.vocubipro.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import dennis.hinsi.vocubipro.model.*;

@androidx.room.Database(entities = {dennis.hinsi.vocubipro.model.VocabularyGroup.class, dennis.hinsi.vocubipro.model.VocabularyChapter.class, dennis.hinsi.vocubipro.model.Vocabulary.class, dennis.hinsi.vocubipro.model.VocabularyEntry.class, dennis.hinsi.vocubipro.model.Language.class}, version = 1)
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\b\u0010\u0005\u001a\u00020\u0006H&J\b\u0010\u0007\u001a\u00020\bH&J\b\u0010\t\u001a\u00020\nH&J\b\u0010\u000b\u001a\u00020\fH&\u00a8\u0006\r"}, d2 = {"Ldennis/hinsi/vocubipro/database/AppDataBase;", "Landroidx/room/RoomDatabase;", "()V", "provideLanguageDao", "Ldennis/hinsi/vocubipro/database/LanguageDao;", "provideVocabularyChapterDao", "Ldennis/hinsi/vocubipro/database/VocabularyChapterDao;", "provideVocabularyDao", "Ldennis/hinsi/vocubipro/database/VocabularyDao;", "provideVocabularyEntryDao", "Ldennis/hinsi/vocubipro/database/VocabularyEntryDao;", "provideVocabularyGroupDao", "Ldennis/hinsi/vocubipro/database/VocabularyGroupDao;", "app_debug"})
public abstract class AppDataBase extends androidx.room.RoomDatabase {
    
    public AppDataBase() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public abstract dennis.hinsi.vocubipro.database.VocabularyGroupDao provideVocabularyGroupDao();
    
    @org.jetbrains.annotations.NotNull()
    public abstract dennis.hinsi.vocubipro.database.VocabularyChapterDao provideVocabularyChapterDao();
    
    @org.jetbrains.annotations.NotNull()
    public abstract dennis.hinsi.vocubipro.database.VocabularyDao provideVocabularyDao();
    
    @org.jetbrains.annotations.NotNull()
    public abstract dennis.hinsi.vocubipro.database.VocabularyEntryDao provideVocabularyEntryDao();
    
    @org.jetbrains.annotations.NotNull()
    public abstract dennis.hinsi.vocubipro.database.LanguageDao provideLanguageDao();
}