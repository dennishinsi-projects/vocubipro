package dennis.hinsi.vocubipro.dagger;

import android.content.Context;
import android.content.SharedPreferences;
import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.qualifiers.ApplicationContext;
import dagger.hilt.components.SingletonComponent;
import dennis.hinsi.vocubipro.dev.helper.PreferenceReader;
import javax.inject.Named;
import javax.inject.Singleton;

@dagger.hilt.InstallIn(value = {dagger.hilt.components.SingletonComponent.class})
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\u0007J\u0012\u0010\u0007\u001a\u00020\u00062\b\b\u0001\u0010\b\u001a\u00020\tH\u0007\u00a8\u0006\n"}, d2 = {"Ldennis/hinsi/vocubipro/dagger/DevOptionsModule;", "", "()V", "providePreferenceReader", "Ldennis/hinsi/vocubipro/dev/helper/PreferenceReader;", "sharedPreferences", "Landroid/content/SharedPreferences;", "provideSharedPreferences", "context", "Landroid/content/Context;", "app_debug"})
@dagger.Module()
public final class DevOptionsModule {
    
    public DevOptionsModule() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Named(value = "pref_dev_options")
    @javax.inject.Singleton()
    @dagger.Provides()
    public final android.content.SharedPreferences provideSharedPreferences(@org.jetbrains.annotations.NotNull()
    @dagger.hilt.android.qualifiers.ApplicationContext()
    android.content.Context context) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Singleton()
    @dagger.Provides()
    public final dennis.hinsi.vocubipro.dev.helper.PreferenceReader providePreferenceReader(@org.jetbrains.annotations.NotNull()
    @javax.inject.Named(value = "pref_dev_options")
    android.content.SharedPreferences sharedPreferences) {
        return null;
    }
}