package dennis.hinsi.vocubipro.ui.vocabulary.detail;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0003"}, d2 = {"VocabularyDetailBottomBarIdDelete", "", "VocabularyDetailBottomBarIdSave", "app_debug"})
public final class VocabularyDetailVariablesKt {
    public static final int VocabularyDetailBottomBarIdSave = 1;
    public static final int VocabularyDetailBottomBarIdDelete = 2;
}