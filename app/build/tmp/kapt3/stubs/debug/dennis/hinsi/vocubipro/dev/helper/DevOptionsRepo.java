package dennis.hinsi.vocubipro.dev.helper;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\u0018\u0000 \f2\u00020\u0001:\u0001\fB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R&\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00068F@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000b\u00a8\u0006\r"}, d2 = {"Ldennis/hinsi/vocubipro/dev/helper/DevOptionsRepo;", "", "reader", "Ldennis/hinsi/vocubipro/dev/helper/PreferenceReader;", "(Ldennis/hinsi/vocubipro/dev/helper/PreferenceReader;)V", "value", "", "showAdditionalInformationEnabled", "getShowAdditionalInformationEnabled", "()Z", "setShowAdditionalInformationEnabled", "(Z)V", "Companion", "app_debug"})
public final class DevOptionsRepo {
    private final dennis.hinsi.vocubipro.dev.helper.PreferenceReader reader = null;
    private boolean showAdditionalInformationEnabled = false;
    @org.jetbrains.annotations.NotNull()
    public static final dennis.hinsi.vocubipro.dev.helper.DevOptionsRepo.Companion Companion = null;
    private static final java.lang.String ADD_INFO_ENABLED = "add_info_enabled";
    
    public DevOptionsRepo(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.dev.helper.PreferenceReader reader) {
        super();
    }
    
    public final void setShowAdditionalInformationEnabled(boolean value) {
    }
    
    public final boolean getShowAdditionalInformationEnabled() {
        return false;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Ldennis/hinsi/vocubipro/dev/helper/DevOptionsRepo$Companion;", "", "()V", "ADD_INFO_ENABLED", "", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}