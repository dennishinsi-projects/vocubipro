package dennis.hinsi.vocubipro.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\f\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u001a\u0017\u0010\u0000\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u0001*\u0004\u0018\u00010\u0002\u00a2\u0006\u0002\u0010\u0003\u001a\u0019\u0010\u0004\u001a\u0004\u0018\u0001H\u0001\"\u0004\b\u0000\u0010\u0001*\u0004\u0018\u00010\u0002\u00a2\u0006\u0002\u0010\u0003\u00a8\u0006\u0005"}, d2 = {"castAs", "T", "", "(Ljava/lang/Object;)Ljava/lang/Object;", "safeCastAs", "app_debug"})
public final class ExtensionsKt {
    
    @org.jetbrains.annotations.Nullable()
    public static final <T extends java.lang.Object>T safeCastAs(@org.jetbrains.annotations.Nullable()
    java.lang.Object $this$safeCastAs) {
        return null;
    }
    
    public static final <T extends java.lang.Object>T castAs(@org.jetbrains.annotations.Nullable()
    java.lang.Object $this$castAs) {
        return null;
    }
}