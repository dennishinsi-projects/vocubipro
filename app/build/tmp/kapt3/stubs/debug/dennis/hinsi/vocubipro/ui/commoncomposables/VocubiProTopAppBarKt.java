package dennis.hinsi.vocubipro.ui.commoncomposables;

import androidx.compose.runtime.Composable;
import androidx.compose.ui.Modifier;
import androidx.compose.ui.graphics.ColorFilter;
import dennis.hinsi.vocubipro.R;
import dennis.hinsi.vocubipro.ui.theme.AppStyle;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\u001a3\u0010\u0000\u001a\u00020\u00012\u0011\u0010\u0002\u001a\r\u0012\u0004\u0012\u00020\u00010\u0003\u00a2\u0006\u0002\b\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u00062\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00010\u0003H\u0007\u00a8\u0006\b"}, d2 = {"VocubiTopAppBar", "", "title", "Lkotlin/Function0;", "Landroidx/compose/runtime/Composable;", "navBackClickEnabled", "", "onNavBackClick", "app_debug"})
public final class VocubiProTopAppBarKt {
    
    @androidx.compose.runtime.Composable()
    public static final void VocubiTopAppBar(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> title, boolean navBackClickEnabled, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> onNavBackClick) {
    }
}