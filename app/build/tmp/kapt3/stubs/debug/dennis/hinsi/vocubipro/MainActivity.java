package dennis.hinsi.vocubipro;

import android.content.Intent;
import android.os.Bundle;
import androidx.activity.ComponentActivity;
import androidx.navigation.NavController;
import androidx.navigation.NavGraphBuilder;
import com.google.android.gms.oss.licenses.OssLicensesMenuActivity;
import dagger.hilt.android.AndroidEntryPoint;
import dennis.hinsi.vocubipro.dev.helper.DevOptions;
import dennis.hinsi.vocubipro.dev.helper.DevOptionsRepo;
import dennis.hinsi.vocubipro.dev.helper.PreferenceReader;
import javax.inject.Inject;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0014J\u0014\u0010\r\u001a\u00020\n*\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002R\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\u0011"}, d2 = {"Ldennis/hinsi/vocubipro/MainActivity;", "Landroidx/activity/ComponentActivity;", "()V", "preferenceReader", "Ldennis/hinsi/vocubipro/dev/helper/PreferenceReader;", "getPreferenceReader", "()Ldennis/hinsi/vocubipro/dev/helper/PreferenceReader;", "setPreferenceReader", "(Ldennis/hinsi/vocubipro/dev/helper/PreferenceReader;)V", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "createNavGraph", "Landroidx/navigation/NavGraphBuilder;", "navController", "Landroidx/navigation/NavController;", "app_debug"})
@dagger.hilt.android.AndroidEntryPoint()
public final class MainActivity extends androidx.activity.ComponentActivity {
    @javax.inject.Inject()
    public dennis.hinsi.vocubipro.dev.helper.PreferenceReader preferenceReader;
    
    public MainActivity() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final dennis.hinsi.vocubipro.dev.helper.PreferenceReader getPreferenceReader() {
        return null;
    }
    
    public final void setPreferenceReader(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.dev.helper.PreferenceReader p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void createNavGraph(androidx.navigation.NavGraphBuilder $this$createNavGraph, androidx.navigation.NavController navController) {
    }
}