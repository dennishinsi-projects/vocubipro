package dennis.hinsi.vocubipro.usecases;

import dennis.hinsi.vocubipro.model.Vocabulary;
import dennis.hinsi.vocubipro.model.container.VocabularyContainer;
import dennis.hinsi.vocubipro.model.VocabularyEntry;
import dennis.hinsi.vocubipro.model.container.VocabularyEntryContainer;
import dennis.hinsi.vocubipro.repository.LanguageRepository;
import dennis.hinsi.vocubipro.repository.VocabularyChapterRepository;
import dennis.hinsi.vocubipro.repository.VocabularyEntryRepository;
import dennis.hinsi.vocubipro.repository.VocabularyRepository;
import kotlinx.coroutines.flow.Flow;
import javax.inject.Inject;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\'\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0019\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0015J\u0019\u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\u0018H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0019J\u0019\u0010\u001a\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\u000eH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001bJ\u0019\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u0014H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0015J\u001f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020 0\r2\u0006\u0010\u001e\u001a\u00020\u0014H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0015J\u0019\u0010!\u001a\u00020\u00182\u0006\u0010\"\u001a\u00020\u0014H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0015J\u0019\u0010#\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\u0018H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0019J\u0019\u0010$\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\u000eH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001bR\u001d\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\f8F\u00a2\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006%"}, d2 = {"Ldennis/hinsi/vocubipro/usecases/VocabularyDataUC;", "", "languageRepository", "Ldennis/hinsi/vocubipro/repository/LanguageRepository;", "chapterRepository", "Ldennis/hinsi/vocubipro/repository/VocabularyChapterRepository;", "vocabularyRepository", "Ldennis/hinsi/vocubipro/repository/VocabularyRepository;", "vocabularyEntryRepository", "Ldennis/hinsi/vocubipro/repository/VocabularyEntryRepository;", "(Ldennis/hinsi/vocubipro/repository/LanguageRepository;Ldennis/hinsi/vocubipro/repository/VocabularyChapterRepository;Ldennis/hinsi/vocubipro/repository/VocabularyRepository;Ldennis/hinsi/vocubipro/repository/VocabularyEntryRepository;)V", "allEntries", "Lkotlinx/coroutines/flow/Flow;", "", "Ldennis/hinsi/vocubipro/model/VocabularyEntry;", "getAllEntries", "()Lkotlinx/coroutines/flow/Flow;", "deleteEntry", "", "id", "", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "insert", "data", "Ldennis/hinsi/vocubipro/model/Vocabulary;", "(Ldennis/hinsi/vocubipro/model/Vocabulary;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "insertEntry", "(Ldennis/hinsi/vocubipro/model/VocabularyEntry;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "loadChapter", "Ldennis/hinsi/vocubipro/model/VocabularyChapter;", "chapterId", "loadVocabulariesFromChapter", "Ldennis/hinsi/vocubipro/model/container/VocabularyContainer;", "loadVocabulary", "vocabularyId", "update", "updateEntry", "app_debug"})
public final class VocabularyDataUC {
    private final dennis.hinsi.vocubipro.repository.LanguageRepository languageRepository = null;
    private final dennis.hinsi.vocubipro.repository.VocabularyChapterRepository chapterRepository = null;
    private final dennis.hinsi.vocubipro.repository.VocabularyRepository vocabularyRepository = null;
    private final dennis.hinsi.vocubipro.repository.VocabularyEntryRepository vocabularyEntryRepository = null;
    
    @javax.inject.Inject()
    public VocabularyDataUC(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.repository.LanguageRepository languageRepository, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.repository.VocabularyChapterRepository chapterRepository, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.repository.VocabularyRepository vocabularyRepository, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.repository.VocabularyEntryRepository vocabularyEntryRepository) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<java.util.List<dennis.hinsi.vocubipro.model.VocabularyEntry>> getAllEntries() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object loadChapter(@org.jetbrains.annotations.NotNull()
    java.lang.String chapterId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super dennis.hinsi.vocubipro.model.VocabularyChapter> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object loadVocabulariesFromChapter(@org.jetbrains.annotations.NotNull()
    java.lang.String chapterId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.List<dennis.hinsi.vocubipro.model.container.VocabularyContainer>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object loadVocabulary(@org.jetbrains.annotations.NotNull()
    java.lang.String vocabularyId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super dennis.hinsi.vocubipro.model.Vocabulary> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object insert(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.model.Vocabulary data, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object update(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.model.Vocabulary data, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object insertEntry(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.model.VocabularyEntry data, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object updateEntry(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.model.VocabularyEntry data, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object deleteEntry(@org.jetbrains.annotations.NotNull()
    java.lang.String id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
}