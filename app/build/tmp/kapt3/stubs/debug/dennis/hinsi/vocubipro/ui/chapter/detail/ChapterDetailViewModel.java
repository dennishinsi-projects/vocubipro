package dennis.hinsi.vocubipro.ui.chapter.detail;

import androidx.lifecycle.ViewModel;
import dagger.hilt.android.lifecycle.HiltViewModel;
import dennis.hinsi.vocubipro.model.VocabularyChapter;
import dennis.hinsi.vocubipro.usecases.ChapterDataUC;
import dennis.hinsi.vocubipro.usecases.DataDeletionUC;
import dennis.hinsi.vocubipro.util.ListIndexInt;
import java.util.*;
import javax.inject.Inject;

@dagger.hilt.android.lifecycle.HiltViewModel()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u0016\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f2\b\u0010\t\u001a\u0004\u0018\u00010\nJ5\u0010\u000e\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\n2\b\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u0010\u001a\u00020\n2\u000e\u0010\u0011\u001a\n\u0018\u00010\u0012j\u0004\u0018\u0001`\u0013\u00a2\u0006\u0002\u0010\u0014R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Ldennis/hinsi/vocubipro/ui/chapter/detail/ChapterDetailViewModel;", "Landroidx/lifecycle/ViewModel;", "chapterDataUC", "Ldennis/hinsi/vocubipro/usecases/ChapterDataUC;", "deletionUseCase", "Ldennis/hinsi/vocubipro/usecases/DataDeletionUC;", "(Ldennis/hinsi/vocubipro/usecases/ChapterDataUC;Ldennis/hinsi/vocubipro/usecases/DataDeletionUC;)V", "delete", "Lkotlinx/coroutines/Job;", "chapterId", "", "loadChapter", "Lkotlinx/coroutines/flow/Flow;", "Ldennis/hinsi/vocubipro/model/VocabularyChapter;", "submit", "groupId", "title", "index", "", "Ldennis/hinsi/vocubipro/util/ListIndexInt;", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)Lkotlinx/coroutines/Job;", "app_debug"})
public final class ChapterDetailViewModel extends androidx.lifecycle.ViewModel {
    private final dennis.hinsi.vocubipro.usecases.ChapterDataUC chapterDataUC = null;
    private final dennis.hinsi.vocubipro.usecases.DataDeletionUC deletionUseCase = null;
    
    @javax.inject.Inject()
    public ChapterDetailViewModel(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.usecases.ChapterDataUC chapterDataUC, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.usecases.DataDeletionUC deletionUseCase) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<dennis.hinsi.vocubipro.model.VocabularyChapter> loadChapter(@org.jetbrains.annotations.Nullable()
    java.lang.String chapterId) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job submit(@org.jetbrains.annotations.NotNull()
    java.lang.String groupId, @org.jetbrains.annotations.Nullable()
    java.lang.String chapterId, @org.jetbrains.annotations.NotNull()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.Integer index) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job delete(@org.jetbrains.annotations.NotNull()
    java.lang.String chapterId) {
        return null;
    }
}