package dennis.hinsi.vocubipro.ui.home;

import androidx.compose.foundation.layout.*;
import androidx.compose.material3.CardDefaults;
import androidx.compose.runtime.Composable;
import androidx.compose.ui.Alignment;
import androidx.compose.ui.Modifier;
import dennis.hinsi.vocubipro.R;
import dennis.hinsi.vocubipro.model.VocabularyChapter;
import dennis.hinsi.vocubipro.ui.theme.AppStyle;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000 \n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\u001a0\u0010\u0000\u001a\u00020\u00012\b\b\u0002\u0010\u0002\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH\u0007\u00a8\u0006\n"}, d2 = {"VocabularyGroupHomeCard", "", "modifier", "Landroidx/compose/ui/Modifier;", "onClick", "Lkotlin/Function0;", "title", "", "chapterCount", "", "app_debug"})
public final class HomeVocabularyGroupComposablesKt {
    
    @androidx.compose.runtime.Composable()
    public static final void VocabularyGroupHomeCard(@org.jetbrains.annotations.NotNull()
    androidx.compose.ui.Modifier modifier, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> onClick, @org.jetbrains.annotations.NotNull()
    java.lang.String title, int chapterCount) {
    }
}