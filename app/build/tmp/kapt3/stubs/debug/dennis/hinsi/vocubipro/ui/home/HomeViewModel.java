package dennis.hinsi.vocubipro.ui.home;

import androidx.lifecycle.ViewModel;
import dagger.hilt.android.lifecycle.HiltViewModel;
import dennis.hinsi.vocubipro.model.Language;
import dennis.hinsi.vocubipro.model.VocabularyGroup;
import dennis.hinsi.vocubipro.repository.LanguageRepository;
import dennis.hinsi.vocubipro.repository.VocabularyGroupRepository;
import dennis.hinsi.vocubipro.usecases.GroupDataUC;
import dennis.hinsi.vocubipro.usecases.LanguageDataUC;
import kotlinx.coroutines.flow.Flow;
import javax.inject.Inject;

@dagger.hilt.android.lifecycle.HiltViewModel()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\b8F\u00a2\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\t0\b8F\u00a2\u0006\u0006\u001a\u0004\b\u000f\u0010\f\u00a8\u0006\u0010"}, d2 = {"Ldennis/hinsi/vocubipro/ui/home/HomeViewModel;", "Landroidx/lifecycle/ViewModel;", "languageDataUC", "Ldennis/hinsi/vocubipro/usecases/LanguageDataUC;", "groupDataUC", "Ldennis/hinsi/vocubipro/usecases/GroupDataUC;", "(Ldennis/hinsi/vocubipro/usecases/LanguageDataUC;Ldennis/hinsi/vocubipro/usecases/GroupDataUC;)V", "groupList", "Lkotlinx/coroutines/flow/Flow;", "", "Ldennis/hinsi/vocubipro/model/VocabularyGroup;", "getGroupList", "()Lkotlinx/coroutines/flow/Flow;", "languageList", "Ldennis/hinsi/vocubipro/model/Language;", "getLanguageList", "app_debug"})
public final class HomeViewModel extends androidx.lifecycle.ViewModel {
    private final dennis.hinsi.vocubipro.usecases.LanguageDataUC languageDataUC = null;
    private final dennis.hinsi.vocubipro.usecases.GroupDataUC groupDataUC = null;
    
    @javax.inject.Inject()
    public HomeViewModel(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.usecases.LanguageDataUC languageDataUC, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.usecases.GroupDataUC groupDataUC) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<java.util.List<dennis.hinsi.vocubipro.model.Language>> getLanguageList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<java.util.List<dennis.hinsi.vocubipro.model.VocabularyGroup>> getGroupList() {
        return null;
    }
}