package dennis.hinsi.vocubipro.ui.languages;

import android.annotation.SuppressLint;
import androidx.compose.foundation.*;
import androidx.compose.foundation.layout.*;
import androidx.compose.material3.*;
import androidx.compose.runtime.Composable;
import androidx.compose.ui.Alignment;
import androidx.compose.ui.Modifier;
import androidx.navigation.NavController;
import dennis.hinsi.vocubipro.Constants;
import dennis.hinsi.vocubipro.R;
import dennis.hinsi.vocubipro.model.Language;
import dennis.hinsi.vocubipro.ui.commoncomposables.NavigationBarEntry;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\"\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0018\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0007\u001a\u001a\u0010\u0006\u001a\u00020\u00012\u0006\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\nH\u0007\u00a8\u0006\u000b"}, d2 = {"LanguageEntryView", "", "modifier", "Landroidx/compose/ui/Modifier;", "language", "Ldennis/hinsi/vocubipro/model/Language;", "LanguageOverviewView", "navController", "Landroidx/navigation/NavController;", "languageDetailViewModel", "Ldennis/hinsi/vocubipro/ui/languages/LanguageDetailViewModel;", "app_debug"})
public final class LanguageOverviewComposableKt {
    
    @androidx.compose.runtime.Composable()
    @kotlin.OptIn(markerClass = {androidx.compose.material3.ExperimentalMaterial3Api.class})
    @android.annotation.SuppressLint(value = {"UnusedMaterial3ScaffoldPaddingParameter"})
    public static final void LanguageOverviewView(@org.jetbrains.annotations.NotNull()
    androidx.navigation.NavController navController, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.ui.languages.LanguageDetailViewModel languageDetailViewModel) {
    }
    
    @androidx.compose.runtime.Composable()
    public static final void LanguageEntryView(@org.jetbrains.annotations.NotNull()
    androidx.compose.ui.Modifier modifier, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.model.Language language) {
    }
}