package dennis.hinsi.vocubipro.ui.commoncomposables;

import androidx.compose.foundation.layout.*;
import androidx.compose.material3.ButtonDefaults;
import androidx.compose.runtime.Composable;
import androidx.compose.ui.Alignment;
import androidx.compose.ui.Modifier;
import androidx.compose.ui.text.font.FontWeight;
import dennis.hinsi.vocubipro.R;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\u001a&\u0010\u0000\u001a\u00020\u00012\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00010\u0005H\u0007\u00a8\u0006\u0007"}, d2 = {"VocubiProDeleteNothingConfirmation", "", "modifier", "Landroidx/compose/ui/Modifier;", "canDelete", "Lkotlin/Function1;", "", "app_debug"})
public final class VocubiProDeleteNothingConfirmationKt {
    
    @androidx.compose.runtime.Composable()
    public static final void VocubiProDeleteNothingConfirmation(@org.jetbrains.annotations.NotNull()
    androidx.compose.ui.Modifier modifier, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Boolean, kotlin.Unit> canDelete) {
    }
}