package dennis.hinsi.vocubipro.ui.languages;

import androidx.lifecycle.ViewModel;
import dagger.hilt.android.lifecycle.HiltViewModel;
import dennis.hinsi.vocubipro.model.Language;
import dennis.hinsi.vocubipro.usecases.DataDeletionUC;
import dennis.hinsi.vocubipro.usecases.LanguageDataUC;
import java.util.*;
import javax.inject.Inject;

@dagger.hilt.android.lifecycle.HiltViewModel()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\b\u0007\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010J\u0016\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\n0\b2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0010J0\u0010\u0013\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0014\u001a\u00020\u00102\u0006\u0010\u0015\u001a\u00020\u00102\u0006\u0010\u0016\u001a\u00020\u00102\u0006\u0010\u0017\u001a\u00020\u0010R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\b8F\u00a2\u0006\u0006\u001a\u0004\b\u000b\u0010\f\u00a8\u0006\u0018"}, d2 = {"Ldennis/hinsi/vocubipro/ui/languages/LanguageDetailViewModel;", "Landroidx/lifecycle/ViewModel;", "languageDataUC", "Ldennis/hinsi/vocubipro/usecases/LanguageDataUC;", "dataDeletionUC", "Ldennis/hinsi/vocubipro/usecases/DataDeletionUC;", "(Ldennis/hinsi/vocubipro/usecases/LanguageDataUC;Ldennis/hinsi/vocubipro/usecases/DataDeletionUC;)V", "languageList", "Lkotlinx/coroutines/flow/Flow;", "", "Ldennis/hinsi/vocubipro/model/Language;", "getLanguageList", "()Lkotlinx/coroutines/flow/Flow;", "delete", "Lkotlinx/coroutines/Job;", "id", "", "loadLanguage", "languageId", "submitData", "title", "titleTranslated", "flag", "accentColor", "app_debug"})
public final class LanguageDetailViewModel extends androidx.lifecycle.ViewModel {
    private final dennis.hinsi.vocubipro.usecases.LanguageDataUC languageDataUC = null;
    private final dennis.hinsi.vocubipro.usecases.DataDeletionUC dataDeletionUC = null;
    
    @javax.inject.Inject()
    public LanguageDetailViewModel(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.usecases.LanguageDataUC languageDataUC, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.usecases.DataDeletionUC dataDeletionUC) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<java.util.List<dennis.hinsi.vocubipro.model.Language>> getLanguageList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<dennis.hinsi.vocubipro.model.Language> loadLanguage(@org.jetbrains.annotations.Nullable()
    java.lang.String languageId) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job delete(@org.jetbrains.annotations.NotNull()
    java.lang.String id) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job submitData(@org.jetbrains.annotations.Nullable()
    java.lang.String id, @org.jetbrains.annotations.NotNull()
    java.lang.String title, @org.jetbrains.annotations.NotNull()
    java.lang.String titleTranslated, @org.jetbrains.annotations.NotNull()
    java.lang.String flag, @org.jetbrains.annotations.NotNull()
    java.lang.String accentColor) {
        return null;
    }
}