package dennis.hinsi.vocubipro.ui.chapter.overview;

import androidx.lifecycle.ViewModel;
import dagger.hilt.android.lifecycle.HiltViewModel;
import dennis.hinsi.vocubipro.usecases.ChapterDataUC;
import dennis.hinsi.vocubipro.usecases.DataDeletionUC;
import dennis.hinsi.vocubipro.usecases.GroupDataUC;
import javax.inject.Inject;

@dagger.hilt.android.lifecycle.HiltViewModel()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u001f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u000e\u0010\r\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\fJ\u001a\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u00110\u00102\u0006\u0010\u000e\u001a\u00020\fJ\u0014\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00140\u00102\u0006\u0010\u000e\u001a\u00020\fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Ldennis/hinsi/vocubipro/ui/chapter/overview/ChapterViewModel;", "Landroidx/lifecycle/ViewModel;", "groupDataUC", "Ldennis/hinsi/vocubipro/usecases/GroupDataUC;", "chapterDataUC", "Ldennis/hinsi/vocubipro/usecases/ChapterDataUC;", "deletionUC", "Ldennis/hinsi/vocubipro/usecases/DataDeletionUC;", "(Ldennis/hinsi/vocubipro/usecases/GroupDataUC;Ldennis/hinsi/vocubipro/usecases/ChapterDataUC;Ldennis/hinsi/vocubipro/usecases/DataDeletionUC;)V", "deleteChapter", "Lkotlinx/coroutines/Job;", "chapterId", "", "deleteGroup", "groupId", "loadChapters", "Lkotlinx/coroutines/flow/Flow;", "", "Ldennis/hinsi/vocubipro/model/VocabularyChapter;", "loadGroup", "Ldennis/hinsi/vocubipro/model/VocabularyGroup;", "app_debug"})
public final class ChapterViewModel extends androidx.lifecycle.ViewModel {
    private final dennis.hinsi.vocubipro.usecases.GroupDataUC groupDataUC = null;
    private final dennis.hinsi.vocubipro.usecases.ChapterDataUC chapterDataUC = null;
    private final dennis.hinsi.vocubipro.usecases.DataDeletionUC deletionUC = null;
    
    @javax.inject.Inject()
    public ChapterViewModel(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.usecases.GroupDataUC groupDataUC, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.usecases.ChapterDataUC chapterDataUC, @org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.usecases.DataDeletionUC deletionUC) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<dennis.hinsi.vocubipro.model.VocabularyGroup> loadGroup(@org.jetbrains.annotations.NotNull()
    java.lang.String groupId) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<java.util.List<dennis.hinsi.vocubipro.model.VocabularyChapter>> loadChapters(@org.jetbrains.annotations.NotNull()
    java.lang.String groupId) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job deleteGroup(@org.jetbrains.annotations.NotNull()
    java.lang.String groupId) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job deleteChapter(@org.jetbrains.annotations.NotNull()
    java.lang.String chapterId) {
        return null;
    }
}