package dennis.hinsi.vocubipro.ui.home;

import androidx.compose.runtime.Composable;
import androidx.compose.ui.Alignment;
import androidx.compose.ui.Modifier;
import androidx.compose.ui.tooling.preview.Preview;
import dennis.hinsi.vocubipro.dev.mock.MockDataProvider;
import dennis.hinsi.vocubipro.R;
import dennis.hinsi.vocubipro.model.Language;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u001a$\u0010\u0000\u001a\u00020\u00012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00010\u0006H\u0007\u001a\u0010\u0010\u0007\u001a\u00020\u00012\u0006\u0010\b\u001a\u00020\u0004H\u0007\u001a\b\u0010\t\u001a\u00020\u0001H\u0007\u00a8\u0006\n"}, d2 = {"LanguageHomeCard", "", "list", "", "Ldennis/hinsi/vocubipro/model/Language;", "onClick", "Lkotlin/Function0;", "LanguageHomeCardEntry", "language", "LanguageHomeCardEntryPreview", "app_debug"})
public final class HomeLanguageComposablesKt {
    
    @androidx.compose.runtime.Composable()
    public static final void LanguageHomeCard(@org.jetbrains.annotations.NotNull()
    java.util.List<dennis.hinsi.vocubipro.model.Language> list, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> onClick) {
    }
    
    @androidx.compose.runtime.Composable()
    public static final void LanguageHomeCardEntry(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.model.Language language) {
    }
    
    @androidx.compose.ui.tooling.preview.Preview(showBackground = true)
    @androidx.compose.runtime.Composable()
    public static final void LanguageHomeCardEntryPreview() {
    }
}