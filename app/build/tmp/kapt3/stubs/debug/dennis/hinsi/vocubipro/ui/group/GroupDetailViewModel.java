package dennis.hinsi.vocubipro.ui.group;

import androidx.lifecycle.ViewModel;
import dagger.hilt.android.lifecycle.HiltViewModel;
import dennis.hinsi.vocubipro.model.VocabularyGroup;
import dennis.hinsi.vocubipro.usecases.GroupDataUC;
import java.util.*;
import javax.inject.Inject;

@dagger.hilt.android.lifecycle.HiltViewModel()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u000b\u001a\u00020\fJ\u0016\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010J\u000e\u0010\u0011\u001a\u00020\f2\u0006\u0010\u0012\u001a\u00020\u0010R\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"}, d2 = {"Ldennis/hinsi/vocubipro/ui/group/GroupDetailViewModel;", "Landroidx/lifecycle/ViewModel;", "groupDataUC", "Ldennis/hinsi/vocubipro/usecases/GroupDataUC;", "(Ldennis/hinsi/vocubipro/usecases/GroupDataUC;)V", "existingVocabularyGroup", "Ldennis/hinsi/vocubipro/model/VocabularyGroup;", "getExistingVocabularyGroup", "()Ldennis/hinsi/vocubipro/model/VocabularyGroup;", "setExistingVocabularyGroup", "(Ldennis/hinsi/vocubipro/model/VocabularyGroup;)V", "delete", "Lkotlinx/coroutines/Job;", "loadGroup", "Lkotlinx/coroutines/flow/Flow;", "groupId", "", "submit", "title", "app_debug"})
public final class GroupDetailViewModel extends androidx.lifecycle.ViewModel {
    private final dennis.hinsi.vocubipro.usecases.GroupDataUC groupDataUC = null;
    @org.jetbrains.annotations.Nullable()
    private dennis.hinsi.vocubipro.model.VocabularyGroup existingVocabularyGroup;
    
    @javax.inject.Inject()
    public GroupDetailViewModel(@org.jetbrains.annotations.NotNull()
    dennis.hinsi.vocubipro.usecases.GroupDataUC groupDataUC) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<dennis.hinsi.vocubipro.model.VocabularyGroup> loadGroup(@org.jetbrains.annotations.Nullable()
    java.lang.String groupId) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final dennis.hinsi.vocubipro.model.VocabularyGroup getExistingVocabularyGroup() {
        return null;
    }
    
    public final void setExistingVocabularyGroup(@org.jetbrains.annotations.Nullable()
    dennis.hinsi.vocubipro.model.VocabularyGroup p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job submit(@org.jetbrains.annotations.NotNull()
    java.lang.String title) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job delete() {
        return null;
    }
}