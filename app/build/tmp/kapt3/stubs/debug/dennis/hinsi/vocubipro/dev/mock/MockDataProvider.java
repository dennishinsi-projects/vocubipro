package dennis.hinsi.vocubipro.dev.mock;

import dennis.hinsi.vocubipro.model.*;
import java.util.*;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004J\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004J\u0012\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u0004J\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u0004J\u0016\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\u00042\b\b\u0002\u0010\f\u001a\u00020\rJ\u001e\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00042\b\b\u0002\u0010\f\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u0005J\u0016\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00120\u00042\b\b\u0002\u0010\f\u001a\u00020\rJ\u0016\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00140\u00042\b\b\u0002\u0010\f\u001a\u00020\r\u00a8\u0006\u0015"}, d2 = {"Ldennis/hinsi/vocubipro/dev/mock/MockDataProvider;", "", "()V", "accentColors", "", "", "flags", "flagsGrouped", "provideLanguageList", "Ldennis/hinsi/vocubipro/model/Language;", "provideVocabularyChapterList", "Ldennis/hinsi/vocubipro/model/VocabularyChapter;", "size", "", "provideVocabularyEntryList", "Ldennis/hinsi/vocubipro/model/VocabularyEntry;", "entryId", "provideVocabularyGroupList", "Ldennis/hinsi/vocubipro/model/VocabularyGroup;", "provideVocabularyList", "Ldennis/hinsi/vocubipro/model/Vocabulary;", "app_debug"})
public final class MockDataProvider {
    @org.jetbrains.annotations.NotNull()
    public static final dennis.hinsi.vocubipro.dev.mock.MockDataProvider INSTANCE = null;
    
    private MockDataProvider() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.String> flags() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.util.List<java.lang.String>> flagsGrouped() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.String> accentColors() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<dennis.hinsi.vocubipro.model.Language> provideLanguageList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<dennis.hinsi.vocubipro.model.VocabularyGroup> provideVocabularyGroupList(int size) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<dennis.hinsi.vocubipro.model.VocabularyChapter> provideVocabularyChapterList(int size) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<dennis.hinsi.vocubipro.model.Vocabulary> provideVocabularyList(int size) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<dennis.hinsi.vocubipro.model.VocabularyEntry> provideVocabularyEntryList(int size, @org.jetbrains.annotations.NotNull()
    java.lang.String entryId) {
        return null;
    }
}