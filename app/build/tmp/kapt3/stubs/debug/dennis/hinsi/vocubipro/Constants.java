package dennis.hinsi.vocubipro;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\n0\nR\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0007X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Ldennis/hinsi/vocubipro/Constants;", "", "()V", "DEFAULT_ACCENT_COLOR_ALPHA", "", "DEFAULT_DECENT_BACKGROUND_ACCENT_COLOR_ALPHA", "DEFAULT_UNSELECTED_COLOR", "", "DEFAULT_UNSELECTED_FLAG", "accentColors", "", "app_debug"})
public final class Constants {
    @org.jetbrains.annotations.NotNull()
    public static final dennis.hinsi.vocubipro.Constants INSTANCE = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DEFAULT_UNSELECTED_FLAG = "\ud83c\udff3";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DEFAULT_UNSELECTED_COLOR = "#3F51B5";
    public static final int DEFAULT_ACCENT_COLOR_ALPHA = 20;
    public static final int DEFAULT_DECENT_BACKGROUND_ACCENT_COLOR_ALPHA = 10;
    
    private Constants() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.util.List<java.lang.String>> accentColors() {
        return null;
    }
}