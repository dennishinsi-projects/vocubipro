package dennis.hinsi.vocubipro.ui.commoncomposables;

import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;
import androidx.compose.foundation.layout.*;
import androidx.compose.runtime.Composable;
import androidx.compose.ui.Alignment;
import androidx.compose.ui.Modifier;
import androidx.compose.ui.tooling.preview.Preview;
import dennis.hinsi.vocubipro.R;
import dennis.hinsi.vocubipro.ui.theme.AppStyle;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\b\u0010\u0000\u001a\u00020\u0001H\u0007\u001a*\u0010\u0002\u001a\u00020\u00012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0007H\u0007\u00a8\u0006\b"}, d2 = {"BottomNavigationBarPreview", "", "VocubiProBottomNavigationBar", "entries", "", "Ldennis/hinsi/vocubipro/ui/commoncomposables/NavigationBarEntry;", "onClick", "Lkotlin/Function1;", "app_debug"})
public final class VocubiProBottomNavigationBarComposablesKt {
    
    @androidx.compose.runtime.Composable()
    public static final void VocubiProBottomNavigationBar(@org.jetbrains.annotations.NotNull()
    java.util.List<dennis.hinsi.vocubipro.ui.commoncomposables.NavigationBarEntry> entries, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super dennis.hinsi.vocubipro.ui.commoncomposables.NavigationBarEntry, kotlin.Unit> onClick) {
    }
    
    @androidx.compose.ui.tooling.preview.Preview(showBackground = true)
    @androidx.compose.runtime.Composable()
    public static final void BottomNavigationBarPreview() {
    }
}