<h1>Vocubi Pro</h1>

Project to take learning vocabularies to the next level. 

Every vocabulary is linked to many different target vocabularies in different languages.
For instance: Vocabulary "House" is linked to a list of VocabularyEntries (German: "Haus", Italian: "casa", ...)
So when you learn a new language you can also refresh another language you already learned. In the example above your native language
is English and you learn Italian and refresh German while doing so. 

<h2>Milestones</h2>

- [x] Create setup of project

- [x] Add possibility to create/modify/delete languages

- [x] Add possibility to create/modify/delete groups/chapters/vocabularies and linked vocabulary entries

- [ ] Add training methods

- [ ] Add training methods result evaluations

- [ ] Add unit tests 

Technically it is completely using Jetpack Compose. 

<h2>App info</h2>

 * minSdkVersion: 31 (Android 12)
 
 * targetSdkVersion: 33 (Android 13)

<h2>License</h2>
	
    Copyright 2022 dennis hinsi

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 	
